package data;

import static code.AddressInfo.VariableSize.BYTE;
import static code.AddressInfo.VariableSize.BYTE_STR;
import static code.AddressInfo.VariableSize.WORD;
import static code.AddressInfo.VariableSize.WORD_STR;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.Tuple2;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import assembly.Address16bit;
import assembly.ByteSource;
import assembly.Instruction;
import code.AddressInfo;
import code.AddressInfo.MemoryRef;
import code.AddressInfo.Variable;
import code.AddressInfo.VariableSize;
import code.Image;
import code.Project;
import code.Segment;
import code.imageref.DataFileRef;
import code.imageref.ImageRef;
import code.imageref.ImageRef.ImageRefType;
import code.imageref.OverlayRef;
import code.imageref.ProgramFileRef;

public class ProjectFile {
	private Project project;

	private File projectFile;

	public ProjectFile(@Nonnull File projectFile) {
		this.projectFile = projectFile;
		load();
	}

	public ProjectFile(@Nonnull Project project) {
		this.project = project;
		this.projectFile = null;
	}

	public Project getProject() {
		return project;
	}

	public boolean canReset() {
		return this.projectFile != null;
	}

	public boolean canSave() {
		return this.projectFile != null;
	}

	public void reset() {
		if (canReset()) {
			load();
		}
	}

	private void load() {
		this.project = (Project) createSerialize().fromXML(this.projectFile);
		this.project.setDirty(false);
	}

	public void save() throws IOException {
		if (canSave()) {
			try (FileChannel c = FileChannel.open(projectFile.toPath(), StandardOpenOption.WRITE,
				StandardOpenOption.CREATE, TRUNCATE_EXISTING)) {
				c.write(ByteBuffer.wrap(createSerialize().toXML(this.project).getBytes()));
				this.project.setDirty(false);
			}
		}
	}

	public void saveAs(@Nonnull File projectFile) throws IOException {
		this.projectFile = projectFile;
		save();
	}

	private XStream createSerialize() {
		final XStream xStream = new XStream();
		xStream.allowTypesByWildcard(new String[] { "**" });
		xStream.alias("project", Project.class);
		xStream.registerConverter(new ConverterImpl());
		return xStream;
	}

	private static final class ConverterImpl implements Converter {
		@Override
		@SuppressWarnings("rawtypes")
		public boolean canConvert(Class type) {
			return Project.class.isAssignableFrom(type);
		}

		@Override
		public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
			final Project prj = (Project) source;

			writer.startNode("config");
			if (prj.getDosboxConfig() != null) {
				writer.startNode("dosbox");
				writer.setValue(prj.getDosboxConfig());
				writer.endNode();
			}
			if (prj.getDosboxKeyMap() != null) {
				writer.startNode("mapper");
				writer.setValue(prj.getDosboxKeyMap());
				writer.endNode();
			}
			writer.endNode(); // config

			prj.updateImagesSeq();

			final Seq<Image> images = prj.getImages();
			final Map<Image, Integer> imageMap = images.zipWithIndex().toMap(Tuple2::_1, Tuple2::_2);

			writer.startNode("images");
			images.forEach(image -> {
				writer.startNode("image");
				ImageRef ref = image.getRef();
				writer.addAttribute("type", ref.getType().name());
				writer.addAttribute("start", image.getStart().toString());
				if (ref.getFilename() != null)
					writer.addAttribute("filename", ref.getFilename());
				if (ref instanceof ProgramFileRef bRef) {
					writer.addAttribute("index", String.format("%d", bRef.getIndex()));
				} else if (ref instanceof DataFileRef dRef) {
					writer.addAttribute("offset", String.format("%d", dRef.getStart()));
					writer.addAttribute("length", String.format("%d", dRef.getLength()));
				}
				writer.endNode();
			});
			writer.endNode(); // images

			writer.startNode("content");
			images.forEachWithIndex((image, index) -> {
				final Seq<Instruction> instructions = image.getSegments().flatMap(Segment::getInstructions);
				final Seq<AddressInfo> infos = image.getSegments()
					.flatMap(Segment::getInfos)
					.filter(this::needsPersisting);
				if (instructions.isEmpty() && infos.isEmpty())
					return;

				writer.startNode("image");
				writer.addAttribute("index", String.format("%d", index));

				if (!instructions.isEmpty()) {
					writer.startNode("instructions");
					instructions.forEach(inst -> {
						writer.startNode("instruction");
						writer.addAttribute("at", inst.getAddress().toString());
						writer.addAttribute("bytes", inst.getBytesAsString());
						writer.endNode();
					});
					writer.endNode();
				}

				if (!infos.isEmpty()) {
					writer.startNode("infos");
					infos.forEach(info -> {
						writer.startNode("info");
						writer.addAttribute("at", info.getAddress().toString());
						if (info.isMethodStart()) {
							writer.addAttribute("methodName", info.getMethodInfo().getName());
						}

						if (!info.getJumpTargets().isEmpty()) {
							writer.startNode("jumps");
							info.getJumpTargets().forEach(jt -> {
								writer.startNode("jump");
								writer.addAttribute("ref", String.format("%d", imageMap.apply(jt.image())));
								writer.addAttribute("at", jt.address().toString());
								writer.endNode();
							});
							writer.endNode();
						}

						if (!info.getMemoryAccesses().isEmpty()) {
							writer.startNode("memoryAccesses");
							info.getMemoryAccesses().forEach(ma -> {
								writer.startNode("memoryAccess");
								writer.addAttribute("ref", String.format("%d", imageMap.apply(ma.ref().image())));
								writer.addAttribute("at", ma.ref().address().toString());
								writer.addAttribute("base", String.format("%d", ma.base()));
								writer.addAttribute("index", String.format("%d", ma.index()));
								writer.endNode();
							});
							writer.endNode();
						}

						if (!info.getStringParameter().isEmpty()) {
							writer.startNode("stringParameters");
							info.getStringParameter().forEach(param -> {
								writer.startNode("stringParameter");
								writer.addAttribute("count", String.format("%d", param.count()));
								MemoryRef src = param.src();
								if (src != null) {
									writer.addAttribute("src", String.format("%d", imageMap.apply(src.image())));
									writer.addAttribute("srcAddress", src.address().toString());
								}
								MemoryRef dst = param.dst();
								if (dst != null) {
									writer.addAttribute("dst", String.format("%d", imageMap.apply(dst.image())));
									writer.addAttribute("dstAddress", dst.address().toString());
								}
								writer.endNode();
							});
							writer.endNode();
						}

						if (!info.getVariableNames().isEmpty()) {
							writer.startNode("variables");
							info.getVariableNames().forEach(varAndName -> {
								writer.startNode("variable");
								writer.addAttribute("count", String.format("%d", varAndName._1().count()));
								writer.addAttribute("size", varAndName._1().size().name());
								writer.addAttribute("name", varAndName._2());
								writer.endNode();
							});
							writer.endNode();
						}

						writer.endNode(); // info
					});
					writer.endNode(); // infos
				}
				writer.endNode(); // image
			});
			writer.endNode(); // content
		}

		public boolean needsPersisting(AddressInfo info) {
			return info.isMethodStart() || !info.getJumpTargets().isEmpty() || !info.getMemoryAccesses().isEmpty()
				|| !info.getStringParameter().isEmpty() || !info.getVariableNames().isEmpty();
		}

		@Override
		public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
			final Project result = new Project();

			validateNodename(reader, "project");

			reader.moveDown();
			if ("config".equals(reader.getNodeName())) {
				if (reader.hasMoreChildren())
					reader.moveDown();
				if ("dosbox".equals(reader.getNodeName())) {
					result.setDosboxConfing(reader.getValue());
					reader.moveUp();
				}
				if (reader.hasMoreChildren())
					reader.moveDown();
				if ("mapper".equals(reader.getNodeName())) {
					result.setDosboxKeyMap(reader.getValue());
					reader.moveUp();
				}
				reader.moveUp();
				reader.moveDown();
			}
			validateNodename(reader, "images");
			while (reader.hasMoreChildren()) {
				reader.moveDown();

				validateNodename(reader, "image");
				ImageRefType type = ImageRefType.valueOf(reader.getAttribute("type"));
				Address16bit start = new Address16bit(reader.getAttribute("start"));
				switch (type) {
					case ANONYMOUS -> result.getAnonymous();
					case COM, EXE -> fromBinary(reader, result, type, start);
					case DATA -> fromData(reader, result, start);
					case OVERLAY -> fromOverlay(reader, result, start);
				}

				reader.moveUp();
			}
			reader.moveUp();

			result.updateImagesSeq();

			reader.moveDown();
			validateNodename(reader, "content");

			while (reader.hasMoreChildren()) {
				reader.moveDown();
				validateNodename(reader, "image");

				final int index = Integer.parseUnsignedInt(reader.getAttribute("index"));
				final Image image = result.getImage(index);

				while (reader.hasMoreChildren()) {
					reader.moveDown();
					if ("instructions".equals(reader.getNodeName())) {
						handleInstructions(reader, image);
					} else if ("infos".equals(reader.getNodeName())) {
						handleInfos(reader, result, image);
					}
					reader.moveUp();
				}
				reader.moveUp(); // image
			}
			reader.moveUp(); // content

			return result;
		}

		private void handleInstructions(HierarchicalStreamReader reader, Image image) {
			while (reader.hasMoreChildren()) {
				reader.moveDown();
				validateNodename(reader, "instruction");

				Address16bit address = new Address16bit(reader.getAttribute("at"));
				ByteSource src = new StringByteSource(reader.getAttribute("bytes"));
				image.addInstruction(Instruction.parseNext(src, address));

				reader.moveUp();
			}
		}

		private void handleInfos(HierarchicalStreamReader reader, Project result, Image image) {
			while (reader.hasMoreChildren()) {
				reader.moveDown();
				validateNodename(reader, "info");

				final Address16bit at = new Address16bit(reader.getAttribute("at"));
				final AddressInfo info = image.getInfo(at);

				final String methodName = reader.getAttribute("methodName");
				if (methodName != null) {
					info.setMethodName(methodName);
				}

				while (reader.hasMoreChildren()) {
					reader.moveDown();
					handleInfoChild(reader, result, info);
					reader.moveUp();
				}
				reader.moveUp();
			}
		}

		private void handleInfoChild(HierarchicalStreamReader reader, Project result, AddressInfo info) {
			switch (reader.getNodeName()) {
				case "jumps":
					handleJumpTargets(reader, result, info);
					break;
				case "memoryAccesses":
					handleMemoryAccesses(reader, result, info);
					break;
				case "stringParameters":
					handleStringParameter(reader, result, info);
					break;
				case "variables":
					handleVariables(reader, info);
					break;
			}
		}

		private void handleJumpTargets(HierarchicalStreamReader reader, Project result, AddressInfo info) {
			while (reader.hasMoreChildren()) {
				reader.moveDown();
				validateNodename(reader, "jump");
				int ref = Integer.parseUnsignedInt(reader.getAttribute("ref"));
				Image jumpImage = result.getImage(ref);
				Address16bit jumpAt = new Address16bit(reader.getAttribute("at"));
				info.addJumpTarget(jumpImage, jumpAt);
				reader.moveUp();
			}
		}

		private void handleMemoryAccesses(HierarchicalStreamReader reader, Project result, AddressInfo info) {
			while (reader.hasMoreChildren()) {
				reader.moveDown();
				validateNodename(reader, "memoryAccess");
				int ref = Integer.parseUnsignedInt(reader.getAttribute("ref"));
				Image targetImage = result.getImage(ref);
				Address16bit targetAt = new Address16bit(reader.getAttribute("at"));
				int accessBase = Integer.parseUnsignedInt(reader.getAttribute("base"));
				int accessIndex = Integer.parseUnsignedInt(reader.getAttribute("index"));
				info.addMemoryAccess(targetImage, targetAt, accessBase, accessIndex);
				reader.moveUp();
			}
		}

		private void handleStringParameter(HierarchicalStreamReader reader, Project result, AddressInfo info) {
			while (reader.hasMoreChildren()) {
				reader.moveDown();
				validateNodename(reader, "stringParameter");
				int count = Integer.parseInt(reader.getAttribute("count"));
				Image srcImage = null;
				Address16bit srcAddress = null;
				if (reader.getAttribute("src") != null) {
					int src = Integer.parseUnsignedInt(reader.getAttribute("src"));
					srcImage = result.getImage(src);
					srcAddress = new Address16bit(reader.getAttribute("srcAddress"));
				}
				Image dstImage = null;
				Address16bit dstAddress = null;
				if (reader.getAttribute("dst") != null) {
					int dst = Integer.parseUnsignedInt(reader.getAttribute("dst"));
					dstImage = result.getImage(dst);
					dstAddress = new Address16bit(reader.getAttribute("dstAddress"));
				}
				info.addStringParams(srcImage, srcAddress, dstImage, dstAddress, count);
				reader.moveUp();
			}
		}

		private void handleVariables(HierarchicalStreamReader reader, AddressInfo info) {
			while (reader.hasMoreChildren()) {
				reader.moveDown();
				validateNodename(reader, "variable");
				String name = reader.getAttribute("name");
				VariableSize size = VariableSize.valueOf(reader.getAttribute("size"));
				int count = Integer.parseInt(reader.getAttribute("count"));
				Variable v = info.appendVar(size, count);
				boolean isDefaultName = v.getDefaultName().equals(name);
				if (size == BYTE || size == WORD) {
					VariableSize strSize = size == BYTE ? BYTE_STR : WORD_STR;
					Variable strV = info.appendVar(strSize, count);
					isDefaultName |= strV.getDefaultName().equals(name);
				}
				if (!isDefaultName) {
					info.setVariableName(name, v);
				}
				reader.moveUp();
			}
		}

		private void validateNodename(HierarchicalStreamReader reader, String expectedNodename) {
			if (!expectedNodename.equals(reader.getNodeName())) {
				throw new IllegalArgumentException("File is not a project file");
			}
		}

		private Image fromBinary(HierarchicalStreamReader reader, Project project, ImageRefType type,
			Address16bit start) {

			File binary = new File(reader.getAttribute("filename"));
			try {
				DOSProgramFile pf = switch (type) {
					case COM -> new COMFile(binary);
					case EXE -> new EXEFile(binary);
					default -> throw new IllegalArgumentException();
				};
				int index = Integer.parseUnsignedInt(reader.getAttribute("index"));
				return project.addImage(new ProgramFileRef(pf, start, index), start);
			} catch (IOException e) {
				throw new IllegalArgumentException(e);
			}
		}

		private Image fromData(HierarchicalStreamReader reader, final Project project, Address16bit start) {
			try {
				int offset = Integer.parseUnsignedInt(reader.getAttribute("offset"));
				int length = Integer.parseUnsignedInt(reader.getAttribute("length"));
				return project.addData(reader.getAttribute("filename"), start, offset, length).ensureNotEmpty();
			} catch (IOException e) {
				throw new IllegalArgumentException(e);
			}
		}

		private Image fromOverlay(HierarchicalStreamReader reader, final Project project, Address16bit start) {
			File data = new File(reader.getAttribute("filename"));
			try {
				OverlayFile overlay = new OverlayFile(data);
				return project.addImage(new OverlayRef(overlay), start);
			} catch (IOException e) {
				throw new IllegalArgumentException(e);
			}
		}
	}

	private static final class StringByteSource implements ByteSource {
		private final byte[] bytes;
		private int index;

		public StringByteSource(String bytes) {
			Seq<Byte> bytesSeq = API.Seq(bytes.split(" ")).map(v -> (byte) Integer.parseInt(v, 16));
			this.bytes = new byte[bytesSeq.size()];
			bytesSeq.forEachWithIndex((b, i) -> this.bytes[i] = b);
			index = 0;
		}

		@Override
		public byte signedByte() {
			return bytes[index++];
		}

		@Override
		public short signedWord() {
			return (short) unsignedWord();
		}

		@Override
		public int unsignedByte() {
			return bytes[index++] & 0xFF;
		}

		@Override
		public int unsignedWord() {
			int b1 = bytes[index++] & 0xFF;
			int b2 = (bytes[index++] & 0xFF) << 8;
			return b2 | b1;
		}

		@Override
		public void markStart() {
		}

		@Override
		public byte[] fromMark() {
			return bytes;
		}
	}
}
