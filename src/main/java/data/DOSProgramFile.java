package data;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.annotation.Nonnull;

import assembly.Address16bit;

public abstract class DOSProgramFile extends DOSFile {

	protected DOSProgramFile(File localFile) {
		super(localFile);
	}

	public abstract int getImageCount();

	public abstract ByteBuffer getBuffer(int index, int segment);

	public abstract Address16bit getCodeEntry(int index, int segment);

	public abstract Address16bit getInitialStack(int index, int segment);

	@Nonnull
	public static DOSProgramFile create(@Nonnull File f) throws IOException {
		if (f.getName().toLowerCase().endsWith(".exe")) {
			return new EXEFile(f);
		} else if (f.getName().toLowerCase().endsWith(".com")) {
			return new COMFile(f);
		} else {
			throw new IllegalArgumentException(f.getName() + " is not a valid binary file.");
		}
	}
}
