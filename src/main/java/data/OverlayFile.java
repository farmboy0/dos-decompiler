package data;

import static java.nio.ByteOrder.LITTLE_ENDIAN;
import static java.nio.file.StandardOpenOption.READ;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import javax.annotation.Nonnull;

public class OverlayFile extends DOSFile {
	private final transient ByteBuffer buffer;

	public OverlayFile(@Nonnull File f) throws IOException {
		super(f);
		try (FileChannel c = FileChannel.open(f.toPath(), READ)) {
			buffer = ByteBuffer.allocate((int) c.size()).order(LITTLE_ENDIAN);
			c.read(buffer);
		}
	}

	@Override
	public int getContentLength() {
		return buffer.limit();
	}

	@Override
	public ByteBuffer getContent(int start, int length) {
		return buffer.slice(start, length);
	}

}
