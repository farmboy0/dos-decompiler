package data;

import static java.nio.ByteOrder.LITTLE_ENDIAN;
import static java.nio.file.StandardOpenOption.READ;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import javax.annotation.Nonnull;

import assembly.Address16bit;

public class COMFile extends DOSProgramFile {
	private final transient ByteBuffer buffer;

	COMFile(@Nonnull File f) throws IOException {
		super(f);
		try (FileChannel c = FileChannel.open(f.toPath(), READ)) {
			buffer = ByteBuffer.allocate((int) c.size()).order(LITTLE_ENDIAN);
			c.read(buffer);
		}
	}

	@Override
	public int getContentLength() {
		return buffer.limit();
	}

	@Override
	public ByteBuffer getContent(int start, int length) {
		return buffer.slice(start, length);
	}

	@Override
	public int getImageCount() {
		return 1;
	}

	@Override
	public ByteBuffer getBuffer(int index, int segment) {
		return buffer;
	}

	@Override
	public Address16bit getCodeEntry(int index, int segment) {
		return new Address16bit(segment - 16, 0x100);
	}

	@Override
	public Address16bit getInitialStack(int index, int segment) {
		return new Address16bit(segment - 16, 0xFFFE);
	}
}
