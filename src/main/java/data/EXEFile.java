package data;

import static java.nio.ByteOrder.LITTLE_ENDIAN;
import static java.nio.file.StandardOpenOption.READ;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

import javax.annotation.Nonnull;

import io.vavr.collection.Array;
import io.vavr.collection.Seq;

import assembly.Address16bit;

public class EXEFile extends DOSProgramFile {
	private final transient EXEHeader header;
	private final transient Seq<Address16bit> relocations;
	private final transient ByteBuffer buffer;

	EXEFile(@Nonnull File f) throws IOException {
		super(f);

		try (FileChannel c = FileChannel.open(f.toPath(), READ)) {
			ByteBuffer file = ByteBuffer.allocate((int) c.size()).order(LITTLE_ENDIAN);
			c.read(file);

			header = EXEHeader.readFrom(file.rewind());

			file.position(header.getRelocationsOffset());
			relocations = Array.fill(header.getRelocationsCount(), () -> Address16bit.readFrom(file));

			file.position(header.getImageOffset());
			byte[] imageBytes = new byte[header.getImageSize()];
			file.get(imageBytes);

			buffer = ByteBuffer.wrap(imageBytes).order(LITTLE_ENDIAN);
		}
	}

	@Override
	public int getContentLength() {
		return buffer.limit();
	}

	@Override
	public ByteBuffer getContent(int start, int length) {
		return buffer.slice(start, length);
	}

	@Override
	public int getImageCount() {
		return 1;
	}

	@Override
	public ByteBuffer getBuffer(int index, int segment) {
		return createRelocatedImage(buffer, relocations, segment);
	}

	@Override
	public Address16bit getCodeEntry(int index, int segment) {
		return new Address16bit(header.getInitialCodeSegment() + segment, header.getInitialInstructionPointer());
	}

	@Override
	public Address16bit getInitialStack(int index, int segment) {
		return new Address16bit(header.getInitialStackSegment() + segment, header.getInitialStackPointer());
	}

	public static ByteBuffer createRelocatedImage(ByteBuffer buffer, Seq<Address16bit> relocations, int segment) {
		final ByteBuffer result = ByteBuffer.allocate(buffer.limit())
			.put(buffer.rewind())
			.rewind()
			.order(ByteOrder.LITTLE_ENDIAN);
		relocations.map(Address16bit::linearAddress).forEach(offset -> {
			int origValue = result.getShort(offset) & 0xFFFF;
			int newValue = origValue + segment;
			result.putShort(offset, (short) newValue);
		});
		return result;
	}

	public static class EXEHeader {
		private final int lastPageSize;
		private final int pagesCount;
		private final int relocationsCount;
		private final int headerSizeInParagraphs;
		private final int minAllocsInParagraphs;
		private final int maxAllocsInParagraphs;
		private final int initialStackSegment;
		private final int initialStackPointer;
		private final int checksum;
		private final int initialInstructionPointer;
		private final int initialCodeSegment;
		private final int relocationsOffset;
		private final int overlayIndex;

		private EXEHeader(int lastPageSize, int pagesCount, int relocationsCount, int headerSizeInParagraphs,
			int minAllocsInParagraphs, int maxAllocsInParagraphs, int initialStackSegment, int initialStackPointer,
			int checksum, int initialInstructionPointer, int initialCodeSegment, int relocationsOffset,
			int overlayIndex) {

			this.lastPageSize = lastPageSize;
			this.pagesCount = pagesCount;
			this.relocationsCount = relocationsCount;
			this.headerSizeInParagraphs = headerSizeInParagraphs;
			this.minAllocsInParagraphs = minAllocsInParagraphs;
			this.maxAllocsInParagraphs = maxAllocsInParagraphs;
			this.initialStackSegment = initialStackSegment;
			this.initialStackPointer = initialStackPointer;
			this.checksum = checksum;
			this.initialInstructionPointer = initialInstructionPointer;
			this.initialCodeSegment = initialCodeSegment;
			this.relocationsOffset = relocationsOffset;
			this.overlayIndex = overlayIndex;
		}

		static EXEHeader readFrom(ByteBuffer data) throws IOException {
			byte[] signature = new byte[2];

			data.get(signature);
			if (!new String(signature).equals("MZ")) {
				throw new IOException("not an exe file");
			}

			int lastPageSize = data.getShort() & 0xFFFF;
			int pagesCount = data.getShort() & 0xFFFF;
			int relocationsCount = data.getShort() & 0xFFFF;
			int headerSizeInParagraphs = data.getShort() & 0xFFFF;
			int minAllocsInParagraphs = data.getShort() & 0xFFFF;
			int maxAllocsInParagraphs = data.getShort() & 0xFFFF;
			int initialStackSegment = data.getShort() & 0xFFFF;
			int initialStackPointer = data.getShort() & 0xFFFF;
			int checksum = data.getShort() & 0xFFFF;
			int initialInstructionPointer = data.getShort() & 0xFFFF;
			int initialCodeSegment = data.getShort() & 0xFFFF;
			int relocationsOffset = data.getShort() & 0xFFFF;
			int overlayIndex = data.getShort() & 0xFFFF;

			return new EXEHeader(lastPageSize, pagesCount, relocationsCount, headerSizeInParagraphs,
				minAllocsInParagraphs, maxAllocsInParagraphs, initialStackSegment, initialStackPointer, checksum,
				initialInstructionPointer, initialCodeSegment, relocationsOffset, overlayIndex);
		}

		public int getImageOffset() {
			return getHeaderSizeInParagraphs() * 16;
		}

		public int getImageSize() {
			return getImageEnd() - getImageOffset();
		}

		public int getImageEnd() {
			return getLastPageSize() == 0 ? getPagesCount() * 512 : (getPagesCount() - 1) * 512 + getLastPageSize();
		}

		public int getLastPageSize() {
			return lastPageSize;
		}

		public int getPagesCount() {
			return pagesCount;
		}

		public int getRelocationsCount() {
			return relocationsCount;
		}

		public int getHeaderSizeInParagraphs() {
			return headerSizeInParagraphs;
		}

		public int getMinAllocsInParagraphs() {
			return minAllocsInParagraphs;
		}

		public int getMaxAllocsInParagraphs() {
			return maxAllocsInParagraphs;
		}

		public int getInitialStackSegment() {
			return initialStackSegment;
		}

		public int getInitialStackPointer() {
			return initialStackPointer;
		}

		public int getChecksum() {
			return checksum;
		}

		public int getInitialInstructionPointer() {
			return initialInstructionPointer;
		}

		public int getInitialCodeSegment() {
			return initialCodeSegment;
		}

		public int getRelocationsOffset() {
			return relocationsOffset;
		}

		public int getOverlayIndex() {
			return overlayIndex;
		}
	}
}
