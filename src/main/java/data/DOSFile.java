package data;

import java.io.File;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Objects;

public abstract class DOSFile implements Serializable {
	private final File localFile;

	protected DOSFile(File localFile) {
		this.localFile = localFile;
	}

	public String getName() {
		return localFile.getName();
	}

	public String getFilename() {
		return localFile.getAbsolutePath();
	}

	public abstract int getContentLength();

	public ByteBuffer getContent() {
		return getContent(0, getContentLength());
	}

	public abstract ByteBuffer getContent(int start, int length);

	@Override
	public int hashCode() {
		return Objects.hash(localFile);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DOSFile other = (DOSFile) obj;
		return Objects.equals(localFile, other.localFile);
	}

	@Override
	public String toString() {
		return getName();
	}
}
