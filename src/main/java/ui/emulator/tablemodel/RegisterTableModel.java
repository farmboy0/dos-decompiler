package ui.emulator.tablemodel;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import io.vavr.collection.Seq;

import emulation.processor.RegisterValue;

public class RegisterTableModel implements TableModel {
	private final Seq<RegisterValue> registers;

	public RegisterTableModel(Seq<RegisterValue> registers) {
		this.registers = registers;
	}

	@Override
	public int getRowCount() {
		return 1;
	}

	@Override
	public int getColumnCount() {
		return registers.size();
	}

	@Override
	public String getColumnName(int columnIndex) {
		return registers.get(columnIndex).getName();
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (rowIndex == 0) {
			return String.format("%04X", registers.get(columnIndex).get());
		}
		return null;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

	}

	@Override
	public void addTableModelListener(TableModelListener l) {

	}

	@Override
	public void removeTableModelListener(TableModelListener l) {

	}
}
