package ui.emulator.tablemodel;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import emulation.processor.Processor;
import emulation.processor.ProcessorFlag;

public class FlagsTableModel implements TableModel {
	private final Processor cpu;

	public FlagsTableModel(Processor cpu) {
		this.cpu = cpu;
	}

	@Override
	public int getRowCount() {
		return 1;
	}

	@Override
	public int getColumnCount() {
		return ProcessorFlag.values().length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return ProcessorFlag.values()[columnIndex].getShortName();
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (rowIndex == 0 && cpu != null) {
			return cpu.read(ProcessorFlag.values()[columnIndex]) ? "1" : "0";
		}
		return null;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
	}
}
