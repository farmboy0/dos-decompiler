package ui.emulator;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.function.Supplier;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableModel;

import assembly.Instruction;
import code.Project;
import emulation.memory.MemoryController;
import emulation.memory.impl.MemoryControllerImpl;
import emulation.processor.ProcessorListener;
import emulation.processor.impl.ProcessorImpl;
import ui.code.DisassemblyPanel;
import ui.emulator.tablemodel.FlagsTableModel;
import ui.emulator.tablemodel.RegisterTableModel;

public class EmulatorWindow extends JInternalFrame {
	private JToolBar toolBar;
	private JPanel monitorPanel;
	private DisassemblyPanel disassemblyPanel;
	private JPanel statusBar;
	private JButton buttonStart;
	private JButton buttonStop;
	private JButton buttonStepInto;
	private JButton buttonStepOver;
	private JButton buttonStepOut;
	private JScrollPane regScrollPane;
	private JTable regTable;
	private JScrollPane flagsScrollPane;
	private JTable flagTable;
	private TableModel regTableModel;
	private TableModel flagTableModel;

	private final MemoryController memory;
	private final ProcessorImpl cpu;
	private JScrollPane disScrollPane;

	public EmulatorWindow(Supplier<Project> projectSupplier) {
		setMaximizable(true);
		setResizable(true);
		this.memory = new MemoryControllerImpl(projectSupplier, 1024 * 1024);
		this.cpu = new ProcessorImpl(memory);
		initialize();
		this.cpu.addProcessorListener(new ProcessorListener() {

			@Override
			public void found(Instruction inst) {
				projectSupplier.get().addInstruction(inst);
				//			getDisassemblyPanel().update(image.getSegment(inst.getAddress()));
			}

			@Override
			public void executed(Instruction inst) {
				getRegTable().tableChanged(new TableModelEvent(getRegTableModel()));
				getFlagTable().tableChanged(new TableModelEvent(getFlagTableModel()));
			}
		});
		this.cpu.setUp(projectSupplier.get().getImages().get());
	}

	private void initialize() {
		final Dimension size = new Dimension(1230, 600);
		setMinimumSize(size);
		setPreferredSize(size);
		setBounds(100, 100, 1230, 600);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 550, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 24, 24, 0, 0, 16 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);
		GridBagConstraints gbcToolBar = new GridBagConstraints();
		gbcToolBar.weightx = 1.0;
		gbcToolBar.anchor = GridBagConstraints.NORTH;
		gbcToolBar.fill = GridBagConstraints.HORIZONTAL;
		gbcToolBar.insets = new Insets(0, 0, 5, 0);
		gbcToolBar.gridwidth = 2;
		gbcToolBar.gridx = 0;
		gbcToolBar.gridy = 0;
		getContentPane().add(getToolBar(), gbcToolBar);
		GridBagConstraints gbcDisScrollPane = new GridBagConstraints();
		gbcDisScrollPane.weightx = 0.5;
		gbcDisScrollPane.weighty = 1.0;
		gbcDisScrollPane.fill = GridBagConstraints.BOTH;
		gbcDisScrollPane.gridheight = 3;
		gbcDisScrollPane.insets = new Insets(0, 0, 5, 5);
		gbcDisScrollPane.gridx = 0;
		gbcDisScrollPane.gridy = 1;
		getContentPane().add(getDisScrollPane(), gbcDisScrollPane);
		GridBagConstraints gbcRegScrollPane = new GridBagConstraints();
		gbcRegScrollPane.weighty = 0.1;
		gbcRegScrollPane.fill = GridBagConstraints.BOTH;
		gbcRegScrollPane.anchor = GridBagConstraints.NORTH;
		gbcRegScrollPane.insets = new Insets(0, 0, 5, 0);
		gbcRegScrollPane.gridx = 1;
		gbcRegScrollPane.gridy = 1;
		getContentPane().add(getRegScrollPane(), gbcRegScrollPane);
		GridBagConstraints gbcFlagsScrollPane = new GridBagConstraints();
		gbcFlagsScrollPane.weighty = 0.1;
		gbcFlagsScrollPane.fill = GridBagConstraints.BOTH;
		gbcFlagsScrollPane.anchor = GridBagConstraints.NORTH;
		gbcFlagsScrollPane.insets = new Insets(0, 0, 5, 0);
		gbcFlagsScrollPane.gridx = 1;
		gbcFlagsScrollPane.gridy = 2;
		getContentPane().add(getFlagsScrollPane(), gbcFlagsScrollPane);
		GridBagConstraints gbcMonitorPanel = new GridBagConstraints();
		gbcMonitorPanel.weighty = 0.8;
		gbcMonitorPanel.weightx = 0.5;
		gbcMonitorPanel.fill = GridBagConstraints.BOTH;
		gbcMonitorPanel.insets = new Insets(0, 0, 5, 0);
		gbcMonitorPanel.gridx = 1;
		gbcMonitorPanel.gridy = 3;
		getContentPane().add(getMonitorPanel(), gbcMonitorPanel);
		GridBagConstraints gbcStatusBar = new GridBagConstraints();
		gbcStatusBar.anchor = GridBagConstraints.SOUTH;
		gbcStatusBar.fill = GridBagConstraints.HORIZONTAL;
		gbcStatusBar.gridwidth = 2;
		gbcStatusBar.gridx = 0;
		gbcStatusBar.gridy = 4;
		getContentPane().add(getStatusBar(), gbcStatusBar);
	}

	private JToolBar getToolBar() {
		if (toolBar == null) {
			toolBar = new JToolBar();
			toolBar.setFloatable(false);
			toolBar.add(getButtonStart());
			toolBar.add(getButtonStop());
			toolBar.add(getButtonStepInto());
			toolBar.add(getButtonStepOver());
			toolBar.add(getButtonStepOut());
		}
		return toolBar;
	}

	private JPanel getMonitorPanel() {
		if (monitorPanel == null) {
			monitorPanel = new JPanel();
			monitorPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			monitorPanel.setPreferredSize(new Dimension(320, 200));
			monitorPanel.setMinimumSize(new Dimension(320, 200));
			monitorPanel.setMaximumSize(new Dimension(640, 480));
		}
		return monitorPanel;
	}

	private JScrollPane getDisScrollPane() {
		if (disScrollPane == null) {
			disScrollPane = new JScrollPane();
			disScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			disScrollPane.getVerticalScrollBar().setUnitIncrement(50);
			disScrollPane.setViewportView(getDisassemblyPanel());
		}
		return disScrollPane;
	}

	private DisassemblyPanel getDisassemblyPanel() {
		if (disassemblyPanel == null) {
			disassemblyPanel = new DisassemblyPanel();
		}
		return disassemblyPanel;
	}

	private JPanel getStatusBar() {
		if (statusBar == null) {
			statusBar = new JPanel();
		}
		return statusBar;
	}

	private JButton getButtonStart() {
		if (buttonStart == null) {
			buttonStart = new JButton("Start");
			buttonStart.addActionListener(e -> cpu.start());
		}
		return buttonStart;
	}

	private JButton getButtonStop() {
		if (buttonStop == null) {
			buttonStop = new JButton("Stop");
			buttonStop.addActionListener(e -> cpu.stop());
		}
		return buttonStop;
	}

	private JButton getButtonStepInto() {
		if (buttonStepInto == null) {
			buttonStepInto = new JButton("Step Into");
			buttonStepInto.addActionListener(e -> cpu.stepInto());
		}
		return buttonStepInto;
	}

	private JButton getButtonStepOver() {
		if (buttonStepOver == null) {
			buttonStepOver = new JButton("Step Over");
			buttonStepOver.addActionListener(e -> cpu.stepOver());
		}
		return buttonStepOver;
	}

	private JButton getButtonStepOut() {
		if (buttonStepOut == null) {
			buttonStepOut = new JButton("Step Out");
			buttonStepOut.addActionListener(e -> cpu.stepReturn());
		}
		return buttonStepOut;
	}

	private JScrollPane getRegScrollPane() {
		if (regScrollPane == null) {
			regScrollPane = new JScrollPane();
			regScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
			regScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			regScrollPane.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			regScrollPane.setViewportView(getRegTable());
		}
		return regScrollPane;
	}

	private JTable getRegTable() {
		if (regTable == null) {
			regTable = new JTable(getRegTableModel());
			regTable.setAutoscrolls(false);
			regTable.setRowSelectionAllowed(false);
			regTable.setFillsViewportHeight(true);
			regTable.setColumnSelectionAllowed(true);
		}
		return regTable;
	}

	protected TableModel getRegTableModel() {
		if (regTableModel == null) {
			regTableModel = new RegisterTableModel(cpu.getRegisters());
		}
		return regTableModel;
	}

	private JScrollPane getFlagsScrollPane() {
		if (flagsScrollPane == null) {
			flagsScrollPane = new JScrollPane();
			flagsScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			flagsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
			flagsScrollPane.setViewportView(getFlagTable());
		}
		return flagsScrollPane;
	}

	private JTable getFlagTable() {
		if (flagTable == null) {
			flagTable = new JTable(getFlagTableModel());
		}
		return flagTable;
	}

	protected TableModel getFlagTableModel() {
		if (flagTableModel == null) {
			flagTableModel = new FlagsTableModel(cpu);
		}
		return flagTableModel;
	}
}
