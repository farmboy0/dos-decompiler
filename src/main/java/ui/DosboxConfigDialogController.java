package ui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.function.Supplier;

import javax.annotation.Nonnull;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;

import code.Project;

public class DosboxConfigDialogController {
	private JDialog dialog;
	private JPanel panel;
	private JTabbedPane tabbedPane;
	private JScrollPane configScrollPane;
	private JScrollPane mapperScrollPane;
	private JTextArea configTextArea;
	private JTextArea mapperTextArea;

	private final Supplier<Project> projectSupplier;
	private JToolBar toolBar;
	private JButton resetButton;
	private JButton okButton;
	private JButton cancelButton;

	public DosboxConfigDialogController(@Nonnull Supplier<Project> projectSupplier) {
		this.projectSupplier = projectSupplier;
	}

	public void show() {
		update();
		getDialog().setVisible(true);
	}

	public void update() {
		final Project project = getProject();
		if (project == null) {
			getConfigTextArea().setText(null);
			getMapperTextArea().setText(null);
			getOkButton().setEnabled(false);
			getResetButton().setEnabled(false);
		} else {
			getConfigTextArea().setText(project.getDosboxConfig());
			getMapperTextArea().setText(project.getDosboxKeyMap());
			getOkButton().setEnabled(true);
			getResetButton().setEnabled(true);
		}
	}

	public Project getProject() {
		return projectSupplier.get();
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	private JDialog getDialog() {
		if (dialog == null) {
			dialog = new JDialog();
			dialog.setBounds(100, 100, 800, 600);
			dialog.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
			dialog.getContentPane().add(getPanel(), BorderLayout.SOUTH);
			dialog.getContentPane().add(getTabbedPane(), BorderLayout.CENTER);
			dialog.getContentPane().add(getToolBar(), BorderLayout.NORTH);
		}
		return dialog;
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			panel.add(getOkButton());
			panel.add(getCancelButton());
		}
		return panel;
	}

	private JTabbedPane getTabbedPane() {
		if (tabbedPane == null) {
			tabbedPane = new JTabbedPane(JTabbedPane.TOP);
			tabbedPane.addTab("Config", null, getConfigScrollPane(), null);
			tabbedPane.addTab("Mapper", null, getMapperScrollPane(), null);
		}
		return tabbedPane;
	}

	private JScrollPane getConfigScrollPane() {
		if (configScrollPane == null) {
			configScrollPane = new JScrollPane(getConfigTextArea());
		}
		return configScrollPane;
	}

	private JScrollPane getMapperScrollPane() {
		if (mapperScrollPane == null) {
			mapperScrollPane = new JScrollPane(getMapperTextArea());
		}
		return mapperScrollPane;
	}

	private JTextArea getConfigTextArea() {
		if (configTextArea == null) {
			configTextArea = new JTextArea();
			configTextArea.setFont(new Font("Monospaced", Font.BOLD, 12));
		}
		return configTextArea;
	}

	private JTextArea getMapperTextArea() {
		if (mapperTextArea == null) {
			mapperTextArea = new JTextArea();
			mapperTextArea.setFont(new Font("Monospaced", Font.BOLD, 12));
		}
		return mapperTextArea;
	}

	private JToolBar getToolBar() {
		if (toolBar == null) {
			toolBar = new JToolBar();
			toolBar.add(getResetButton());
		}
		return toolBar;
	}

	private JButton getResetButton() {
		if (resetButton == null) {
			resetButton = new JButton("Reset");
			resetButton.addActionListener(e -> reset());
		}
		return resetButton;
	}

	private JButton getOkButton() {
		if (okButton == null) {
			okButton = new JButton("OK");
			okButton.addActionListener(e -> ok());
		}
		return okButton;
	}

	private JButton getCancelButton() {
		if (cancelButton == null) {
			cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(e -> cancel());
		}
		return cancelButton;
	}

	private void reset() {
		final Project project = getProject();
		if (project != null)
			project.resetConfig();
	}

	private void ok() {
		final Project project = getProject();
		if (project != null) {
			project.setDosboxConfing(getConfigTextArea().getText());
			project.setDosboxKeyMap(getMapperTextArea().getText());
		}
		getDialog().setVisible(false);
	}

	private void cancel() {
		getDialog().setVisible(false);
	}
}
