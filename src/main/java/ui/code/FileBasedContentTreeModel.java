package ui.code;

import java.util.function.Supplier;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreePath;

import io.vavr.API;
import io.vavr.collection.Set;

import code.AddressInfo;
import code.AddressInfo.Variable;
import code.Image;
import code.Image.InitialStateMap;
import code.ParentWithChildren;
import code.Project;
import code.Segment;
import code.Segment.CodeModifications;
import code.Segment.CodeModificator;
import code.Segment.OffsetRange;
import code.imageref.AnonymousImageRef;
import data.DataFile;

public class FileBasedContentTreeModel implements ContentTreeModel {
	private final Supplier<Project> projectSupplier;

	public FileBasedContentTreeModel(Supplier<Project> project) {
		this.projectSupplier = project;
	}

	@Override
	public void update() {
		TreeModelEvent e = new TreeModelEvent(this, new Object[] { getRoot() });
		listeners.forEach(l -> l.treeStructureChanged(e));
	}

	@Override
	public TreePath pathTo(AddressInfo info) {
		final Image image = info.getImage();
		final Object root = getRoot();

		if (image.getRef() instanceof AnonymousImageRef) {
			return new TreePath(new Object[] { root, image, info.getSegment(), info });
		}
		return new TreePath(new Object[] { root, image, info });
	}

	@Override
	public TreePath pathTo(Variable v) {
		final Image image = v.info().getImage();
		final Object root = getRoot();

		if (v.getDataBlock().getParent() != null) {
			return new TreePath(new Object[] { root, image, v.getDataBlock().getParent().getInfo(), v.getDataBlock() });
		}
		return new TreePath(new Object[] { root, image, v.info() });
	}

	public Project getProject() {
		return projectSupplier.get();
	}

	@Override
	public Object getRoot() {
		return projectSupplier;
	}

	@Override
	public Object getChild(Object parent, int index) {
		if (getProject() == null) {
			return null;
		} else if (parent == getRoot()) {
			int count = getProject().getImagesCount();
			if (index < count) {
				return getProject().getImagesWithoutData().get(index);
			}
			return getProject().getDataFile(index - count);
		} else if (parent instanceof DataFile dataFile) {
			return getProject().getDataFileImage(dataFile, index);
		} else if (parent instanceof Image image) {
			if (image.getRef() instanceof AnonymousImageRef) {
				return image.getSegment(index);
			}
			if (!image.hasLoadModule()) {
				return image.getContent().get(index);
			}
			if (index == 0) {
				return image.getInitialState();
			}
			return image.getContent().get(index - 1);
		} else if (parent instanceof Segment segment) {
			if (!segment.isModified()) {
				return segment.getDataAndMethodInfos().get(index);
			}
			if (index == 0) {
				return segment.getModifications();
			}
			return segment.getDataAndMethodInfos().get(index - 1);
		} else if (parent instanceof AddressInfo info) {
			if (info.isMethodStart()) {
				return info.getMethodInfo().getCaller(index);
			}
			if (info.isDataBlockStart()) {
				return info.getDataBlock().getChild(index);
			}
		} else if (parent instanceof CodeModifications mods) {
			return mods.get(index);
		} else if (parent instanceof CodeModificator mod) {
			return mod.segment().getModifications().get(mod, index);
		} else if (parent instanceof ParentWithChildren<?> p) {
			return p.getChild(index);
		}
		return null;
	}

	@Override
	public int getChildCount(Object parent) {
		if (getProject() == null) {
			return 0;
		} else if (parent == getRoot()) {
			return getProject().getImagesCount() + getProject().getDataFileCount();
		} else if (parent instanceof DataFile dataFile) {
			return getProject().getDataFileImagesCount(dataFile);
		} else if (parent instanceof Image image) {
			if (image.getRef() instanceof AnonymousImageRef) {
				return image.getSegments().size();
			}
			if (!image.hasLoadModule()) {
				return image.getContent().size();
			}
			return image.getContent().size() + 1;
		} else if (parent instanceof Segment segment) {
			if (!segment.isModified()) {
				return segment.getDataAndMethodInfos().size();
			}
			return segment.getDataAndMethodInfos().size() + 1;
		} else if (parent instanceof AddressInfo info) {
			if (info.isMethodStart()) {
				return info.getMethodInfo().getCallersCount();
			}
			if (info.isDataBlockStart()) {
				return info.getDataBlock().childrenCount();
			}
		} else if (parent instanceof CodeModifications mods) {
			return mods.size();
		} else if (parent instanceof CodeModificator mod) {
			return mod.segment().getModifications().size(mod);
		} else if (parent instanceof ParentWithChildren<?> p) {
			return p.childrenCount();
		}
		return 0;
	}

	@Override
	public boolean isLeaf(Object node) {
		return getChildCount(node) == 0;
	}

	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		if (getProject() == null) {
			return 0;
		} else if (parent == getRoot()) {
			int count = getProject().getImagesCount();
			if (child instanceof Image image) {
				return getProject().getImagesWithoutData().indexOf(image);
			}
			return count + getProject().getDataFiles().indexOf((DataFile) child);
		} else if (parent instanceof DataFile dataFile) {
			return getProject().getDataFileImages(dataFile).indexOf((Image) child);
		} else if (parent instanceof Image image) {
			if (image.getRef() instanceof AnonymousImageRef) {
				return image.getSegments().indexOf((Segment) child);
			}
			if (!image.hasLoadModule()) {
				return image.getContent().indexOf((AddressInfo) child);
			}
			if (child instanceof InitialStateMap) {
				return 0;
			}
			return image.getContent().indexOf((AddressInfo) child) + 1;
		} else if (parent instanceof Segment segment) {
			if (child instanceof CodeModifications) {
				return 0;
			}
			if (!segment.isModified()) {
				return segment.getDataAndMethodInfos().indexOf((AddressInfo) child);
			}
			return segment.getDataAndMethodInfos().indexOf((AddressInfo) child) + 1;
		} else if (parent instanceof AddressInfo info) {
			if (info.isMethodStart()) {
				return info.getMethodInfo().getIndexOfCaller((AddressInfo) child);
			}
			if (info.isDataBlockStart()) {
				return info.getDataBlock().indexOfChild(child);
			}
		} else if (parent instanceof CodeModifications mods) {
			return mods.indexOf((CodeModificator) child);
		} else if (parent instanceof CodeModificator mod) {
			return mod.segment().getModifications().indexOf(mod, (OffsetRange) child);
		} else if (parent instanceof ParentWithChildren<?> p) {
			return p.indexOfChild(child);
		}
		return 0;
	}

	private Set<TreeModelListener> listeners = API.Set();

	@Override
	public void addTreeModelListener(TreeModelListener l) {
		listeners = listeners.add(l);
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
		listeners = listeners.remove(l);
	}
}
