package ui.code;

import java.util.function.Supplier;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreePath;

import io.vavr.API;
import io.vavr.collection.Set;

import code.AddressInfo;
import code.AddressInfo.Variable;
import code.Image;
import code.Image.InitialStateMap;
import code.ParentWithChildren;
import code.Project;
import code.Segment;
import code.Segment.CodeModifications;
import code.Segment.CodeModificator;
import code.Segment.OffsetRange;

public class ImageBasedContentTreeModel implements ContentTreeModel {
	private final Supplier<Project> projectSupplier;

	public ImageBasedContentTreeModel(Supplier<Project> project) {
		this.projectSupplier = project;
	}

	@Override
	public void update() {
		TreeModelEvent e = new TreeModelEvent(this, new Object[] { getRoot() });
		listeners.forEach(l -> l.treeStructureChanged(e));
	}

	@Override
	public TreePath pathTo(AddressInfo info) {
		final Image image = info.getImage();
		final Object root = getRoot();

		return new TreePath(new Object[] { root, image, info.getSegment(), info });
	}

	@Override
	public TreePath pathTo(Variable v) {
		final Image image = v.info().getImage();
		final Object root = getRoot();

		if (v.getDataBlock().getParent() != null) {
			return new TreePath(new Object[] { root, image, v.info().getSegment(),
				v.getDataBlock().getParent().getInfo(), v.getDataBlock() });
		}
		return new TreePath(new Object[] { root, image, v.info().getSegment(), v.info() });
	}

	public Project getProject() {
		return projectSupplier.get();
	}

	@Override
	public Object getRoot() {
		return projectSupplier;
	}

	@Override
	public Object getChild(Object parent, int index) {
		if (getProject() == null) {
			return null;
		} else if (parent == getRoot()) {
			return getProject().getImages().get(index);
		} else if (parent instanceof Image image) {
			if (!image.hasLoadModule()) {
				return image.getSegment(index);
			}
			if (index == 0) {
				return image.getInitialState();
			}
			return image.getSegment(index - 1);
		} else if (parent instanceof Segment segment) {
			if (!segment.isModified()) {
				return segment.getDataAndMethodInfos().get(index);
			}
			if (index == 0) {
				return segment.getModifications();
			}
			return segment.getDataAndMethodInfos().get(index - 1);
		} else if (parent instanceof AddressInfo info) {
			if (info.isMethodStart()) {
				return info.getMethodInfo().getCaller(index);
			}
			if (info.isDataBlockStart()) {
				return info.getDataBlock().getChild(index);
			}
		} else if (parent instanceof CodeModifications mods) {
			return mods.get(index);
		} else if (parent instanceof CodeModificator mod) {
			return mod.segment().getModifications().get(mod, index);
		} else if (parent instanceof ParentWithChildren<?> p) {
			return p.getChild(index);
		}
		return null;
	}

	@Override
	public int getChildCount(Object parent) {
		if (getProject() == null) {
			return 0;
		} else if (parent == getRoot()) {
			return getProject().getImages().size();
		} else if (parent instanceof Image image) {
			if (!image.hasLoadModule()) {
				return image.getSegments().size();
			}
			return image.getSegments().size() + 1;
		} else if (parent instanceof Segment segment) {
			if (!segment.isModified()) {
				return segment.getDataAndMethodInfos().size();
			}
			return segment.getDataAndMethodInfos().size() + 1;
		} else if (parent instanceof AddressInfo info) {
			if (info.isMethodStart()) {
				return info.getMethodInfo().getCallersCount();
			}
			if (info.isDataBlockStart()) {
				return info.getDataBlock().childrenCount();
			}
		} else if (parent instanceof CodeModifications mods) {
			return mods.size();
		} else if (parent instanceof CodeModificator mod) {
			return mod.segment().getModifications().size(mod);
		} else if (parent instanceof ParentWithChildren<?> p) {
			return p.childrenCount();
		}
		return 0;
	}

	@Override
	public boolean isLeaf(Object node) {
		return getChildCount(node) == 0;
	}

	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		if (getProject() == null) {
			return 0;
		} else if (parent == getRoot()) {
			return getProject().getImages().indexOf((Image) child);
		} else if (parent instanceof Image image) {
			if (child instanceof InitialStateMap) {
				return 0;
			}
			if (!image.hasLoadModule()) {
				return image.getSegments().indexOf((Segment) child);
			}
			return image.getSegments().indexOf((Segment) child) + 1;
		} else if (parent instanceof Segment segment) {
			if (child instanceof CodeModifications) {
				return 0;
			}
			if (!segment.isModified()) {
				return segment.getDataAndMethodInfos().indexOf((AddressInfo) child);
			}
			return segment.getDataAndMethodInfos().indexOf((AddressInfo) child) + 1;
		} else if (parent instanceof AddressInfo info) {
			if (info.isMethodStart()) {
				return info.getMethodInfo().getIndexOfCaller((AddressInfo) child);
			}
			if (info.isDataBlockStart()) {
				return info.getDataBlock().indexOfChild(child);
			}
		} else if (parent instanceof CodeModifications mods) {
			return mods.indexOf((CodeModificator) child);
		} else if (parent instanceof CodeModificator mod) {
			return mod.segment().getModifications().indexOf(mod, (OffsetRange) child);
		} else if (parent instanceof ParentWithChildren<?> p) {
			return p.indexOfChild(child);
		}
		return 0;
	}

	private Set<TreeModelListener> listeners = API.Set();

	@Override
	public void addTreeModelListener(TreeModelListener l) {
		listeners = listeners.add(l);
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
		listeners = listeners.remove(l);
	}

}
