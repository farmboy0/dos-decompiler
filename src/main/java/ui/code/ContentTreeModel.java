package ui.code;

import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import code.AddressInfo;
import code.AddressInfo.Variable;

public interface ContentTreeModel extends TreeModel {
	void update();

	TreePath pathTo(AddressInfo info);

	TreePath pathTo(Variable v);
}
