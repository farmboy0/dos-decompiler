package ui.code.cellrenderer;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class HexValueTableCellRenderer extends DefaultTableCellRenderer {

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
		int row, int column) {

		final Component result = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		result.setBackground(Color.black);
		result.setForeground(Color.white);
		return result;
	}
}
