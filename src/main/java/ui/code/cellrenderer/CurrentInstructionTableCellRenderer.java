package ui.code.cellrenderer;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import assembly.Instruction;

public class CurrentInstructionTableCellRenderer extends DefaultTableCellRenderer {
	private Instruction current;

	public Instruction getCurrent() {
		return current;
	}

	public void setCurrent(Instruction current) {
		this.current = current;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
		int row, int column) {

		final Component result = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		if (value == current)
			result.setBackground(Color.DARK_GRAY);
		return result;
	}
}
