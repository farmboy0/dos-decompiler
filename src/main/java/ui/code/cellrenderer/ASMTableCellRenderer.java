package ui.code.cellrenderer;

import static java.lang.String.format;

import java.awt.Color;
import java.awt.Component;

import javax.annotation.Nonnull;
import javax.swing.JTable;
import javax.swing.ToolTipManager;
import javax.swing.table.DefaultTableCellRenderer;

import io.vavr.collection.Set;

import assembly.Address16bit;
import assembly.Instruction;
import code.AddressInfo;
import code.AddressInfo.MemoryRef;
import code.AddressInfo.VariableAccess;
import code.AddressInfo.VariableAccessType;
import code.MethodInfo;
import code.Segment;
import ui.code.tablemodel.ASMTableModel;

public class ASMTableCellRenderer extends DefaultTableCellRenderer {
	private static final String TOOLTIP_START = "<html><font face=\"Liberation Mono, monospace\" size=\"5\">";
	private static final String TOOLTIP_END = "</font></html>";

	private final Segment segment;
	private final Set<Address16bit> marked;

	public ASMTableCellRenderer(@Nonnull Segment segment, @Nonnull Set<Address16bit> marked) {
		this.segment = segment;
		this.marked = marked;
		ToolTipManager.sharedInstance().setDismissDelay(60000);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
		int row, int column) {

		final ASMTableModel model = (ASMTableModel) table.getModel();
		final Instruction inst = model.getInst(row);
		final AddressInfo instInfo = segment.getInfo(inst.getAddress());
		final boolean isMarker = value != null && marked.contains(inst.getAddress());
		setBackground(isMarker ? Color.GRAY : null);
		setToolTipText(null);
		if (instInfo != null) {
			if (instInfo.isJumpTarget() && column == 0) {
				final StringBuilder sb = new StringBuilder(TOOLTIP_START);
				instInfo.getJumpSources()
					.forEach(js -> sb.append(format("[%s] %s<br/>", js.address(),
						js.image()
							.getMethodsFor(js.address())
							.map(AddressInfo::getMethodInfo)
							.map(MethodInfo::getName)
							.mkString(", "))));
				sb.append(TOOLTIP_END);
				setToolTipText(sb.toString());
			} else if (inst.isMethodCall()) {
				final StringBuilder sb = new StringBuilder(TOOLTIP_START);
				instInfo.getJumpTargets()
					.flatMap(MemoryRef::asMethods)
					.forEach(method -> sb
						.append(format("[%s] %s<br/>", method.getRef().address(), method.getMethodInfo().getName())));
				sb.append(TOOLTIP_END);
				setToolTipText(sb.toString());
			} else if (!instInfo.getVariableRefs().isEmpty()) {
				final boolean showCount = instInfo.getVariableRefs().size() > 11;
				final StringBuilder sb = new StringBuilder(TOOLTIP_START);
				instInfo.getVariableRefs()
					.take(showCount ? 10 : 11)
					.forEach(v -> sb.append(format("[%s](%s)%s<br/>", v.info().getRef().address(),
						v.getAccess(instInfo.getRef())
							.map(VariableAccess::accessType)
							.map(VariableAccessType::shortName)
							.orElse("unknown access"),
						v)));
				if (showCount)
					sb.append(format("and %d more...", instInfo.getVariableRefs().size() - 10));
				sb.append(TOOLTIP_END);
				setToolTipText(sb.toString());
			}
		}
		return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	}
}
