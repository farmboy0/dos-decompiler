package ui.code.tablemodel;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import javax.annotation.Nonnull;
import javax.swing.table.AbstractTableModel;

import assembly.Address16bit;

public class MemoryTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 767320373557181608L;

	private final transient Address16bit firstAddress;
	private final int indent;
	private final transient ByteBuffer image;
	private final int rowCount;

	public MemoryTableModel(@Nonnull Address16bit firstAddress, int indent, @Nonnull ByteBuffer image) {
		this.firstAddress = firstAddress;
		this.indent = indent;
		this.image = image;
		this.rowCount = (this.image.limit() + indent) / 16 + ((this.image.limit() + indent) % 16 != 0 ? 1 : 0);
	}

	@Override
	public int getColumnCount() {
		return 35;
	}

	@Override
	public int getRowCount() {
		return rowCount;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if ((columnIndex >= 34) || (columnIndex == 0)) {
			return null;
		}
		// Memory address
		if (columnIndex == 1) {
			if (rowIndex == 0) {
				return firstAddress.relativeAddress(indent);
			}
			return firstAddress.relativeAddress(16 * rowIndex);
		}
		if (columnIndex >= 2 && columnIndex < 18) {
			int pos = (rowIndex << 4) + columnIndex - 2 - indent;
			if (pos < 0 || pos >= image.limit())
				return null;
			return hex2(image.get(pos) & 0xFF);
		} else {
			int pos = (rowIndex << 4) + columnIndex - 18 - indent;
			if (pos < 0 || pos >= image.limit() || !isPrintable(image.get(pos)))
				return null;
			return new String(new byte[] { image.get(pos) }, StandardCharsets.US_ASCII);
		}
	}

	private static String hex2(int value) {
		return String.format("%02X", value);
	}

	private static boolean isPrintable(byte c) {
		return c >= 32 && c <= 126;
	}
}
