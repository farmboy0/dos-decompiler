package ui.code.tablemodel;

import javax.annotation.Nonnull;
import javax.swing.table.AbstractTableModel;

import io.vavr.collection.Seq;
import io.vavr.collection.Set;

import assembly.Address16bit;
import assembly.Instruction;
import code.Image;

public class ASMTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 6730901626650684935L;

	private final transient Image image;
	private final transient Seq<Instruction> assembly;
	private final transient Set<Address16bit> jumpTargets;

	public ASMTableModel(@Nonnull Image image, @Nonnull Set<Instruction> assembly,
		@Nonnull Set<Address16bit> jumpTargets) {

		this.image = image;
		this.assembly = assembly.toArray();
		this.jumpTargets = jumpTargets;
	}

	public Image getImage() {
		return image;
	}

	@Override
	public int getColumnCount() {
		return 5;
	}

	@Override
	public int getRowCount() {
		return this.assembly.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		final Instruction inst = getInst(rowIndex);
		switch (columnIndex) {
			case 0:
				return jumpTargets.contains(inst.getAddress()) ? "o" : "";
			case 1:
				return inst.getAddress();
			case 2:
				return inst.getBytesAsString();
			case 3:
				return inst.getOpcodeAsString();
			case 4:
				return inst.getArgumentsAsString();
			default:
				return null;
		}
	}

	public Instruction getInst(int row) {
		return this.assembly.get(row);
	}
}
