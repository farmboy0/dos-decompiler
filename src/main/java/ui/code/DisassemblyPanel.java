package ui.code;

import java.awt.Component;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.FontRenderContext;
import java.nio.ByteBuffer;

import javax.annotation.Nonnull;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.TreePath;

import io.vavr.API;
import io.vavr.collection.Array;
import io.vavr.collection.Set;
import io.vavr.collection.SortedSet;

import assembly.Address16bit;
import assembly.Instruction;
import code.AddressInfo;
import code.AddressInfo.MemoryRef;
import code.AddressInfo.Variable;
import code.AddressInfo.VariableAccess;
import code.AddressInfo.VariableAccessType;
import code.DataBlock;
import code.MethodInfo;
import code.Segment;
import ui.code.cellrenderer.ASMTableCellRenderer;
import ui.code.cellrenderer.HexValueTableCellRenderer;
import ui.code.tablemodel.ASMTableModel;
import ui.code.tablemodel.MemoryTableModel;

public class DisassemblyPanel extends JPanel {
	private static final Font DEFAULT_TABLE_FONT = new Font(Font.MONOSPACED, Font.BOLD, 16);
	private static final int DEFAULT_TABLE_FONT_WIDTH = 3
		+ DEFAULT_TABLE_FONT.getMaxCharBounds(new FontRenderContext(null, true, true)).getBounds().width;

	private JTree tree;

	public DisassemblyPanel() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}

	public DisassemblyPanel(@Nonnull JTree tree) {
		this();
		this.tree = tree;
	}

	public Object getTreeRoot() {
		return tree.getModel().getRoot();
	}

	public void update(Segment segment) {
		removeAll();

		Set<Address16bit> jt = segment.getJumpTargets().toSet();
		segment.getContentOrdered().map(content -> {
			if (content.isCode()) {
				return createAssemblerTable(segment, content.toCode(), API.Set(), jt);
			}
			if (content.isData()) {
				int startOffset = content.getOffset();
				int alignedOffset = startOffset / 16 * 16;
				int indent = startOffset - alignedOffset;
				return createMemoryTable(content.toData(), new Address16bit(segment.getSegment(), alignedOffset),
					indent);
			}
			throw new IllegalStateException("Unknown content " + content);
		}).forEach(this::add);

		revalidate();
		repaint();
	}

	public void update(Segment segment, SortedSet<Instruction> blocks) {
		update(segment, blocks, API.Set());
	}

	public void update(Segment segment, SortedSet<Instruction> blocks, Set<Address16bit> markers) {
		removeAll();

		add(createAssemblerTable(segment, blocks, markers));

		revalidate();
		repaint();
	}

	public void update(DataBlock block) {
		removeAll();

		final ByteBuffer buf = block.getContent();
		final Address16bit start = block.getInfo().getAddress();

		final int alignedOffset = start.getOffset() / 16 * 16;
		final int indent = start.getOffset() - alignedOffset;

		add(createMemoryTable(buf, new Address16bit(start.getSegment(), alignedOffset), indent));

		revalidate();
		repaint();
	}

	private JTable createMemoryTable(ByteBuffer image, Address16bit startAddress, int indent) {
		final JTable memoryTable = new JTable();
		memoryTable.setCellSelectionEnabled(false);
		memoryTable.setDoubleBuffered(true);
		memoryTable.setEnabled(false);
		memoryTable.setFont(DEFAULT_TABLE_FONT);
		memoryTable.setModel(new MemoryTableModel(startAddress, indent, image));
		memoryTable.setShowVerticalLines(false);
		memoryTable.setTableHeader(null);

		final TableCellRenderer renderer = new HexValueTableCellRenderer();
		final TableColumnModel columnModel = memoryTable.getColumnModel();
		setCellWidth(columnModel.getColumn(0), 1);
		setCellWidth(columnModel.getColumn(1), 8);
		setCellWidth(columnModel.getColumn(34), 2);

		for (int i = 2; i < 18; i++) {
			setCellWidth(columnModel.getColumn(i), 2);
			columnModel.getColumn(i).setCellRenderer(renderer);
		}
		for (int i = 18; i < 34; i++) {
			setCellWidth(columnModel.getColumn(i), 1);
		}
		return memoryTable;
	}

	private JTable createAssemblerTable(Segment segment, SortedSet<Instruction> instructions,
		Set<Address16bit> marked) {
		return createAssemblerTable(segment, instructions, marked, segment.getJumpTargets().toSet());
	}

	private JTable createAssemblerTable(Segment segment, SortedSet<Instruction> instructions, Set<Address16bit> marked,
		Set<Address16bit> jumpTargets) {

		final JTable assemblerTable = new JTable();
		final ASMTableModel dataModel = new ASMTableModel(segment.getImage(), instructions, jumpTargets);
		assemblerTable.setCellSelectionEnabled(false);
		assemblerTable.setDoubleBuffered(true);
		assemblerTable.setEnabled(true);
		assemblerTable.setFont(DEFAULT_TABLE_FONT);
		assemblerTable.setModel(dataModel);
		assemblerTable.setRowSelectionAllowed(true);
		assemblerTable.setShowVerticalLines(false);
		assemblerTable.setTableHeader(null);

		final KeyStroke ksCopy = KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_DOWN_MASK);
		assemblerTable.unregisterKeyboardAction(ksCopy);
		assemblerTable.registerKeyboardAction(e -> {
			if (assemblerTable.getSelectedRowCount() == 0)
				return;

			final StringBuilder sb = new StringBuilder();

			int[] rows = assemblerTable.getSelectedRows();
			for (int i = 0; i < rows.length; i++) {
				sb.append(dataModel.getInst(rows[i]));
				sb.append(System.lineSeparator());
			}

			final StringSelection selection = new StringSelection(sb.toString());
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, selection);
		}, ksCopy, JComponent.WHEN_FOCUSED);
		assemblerTable.addMouseListener(new ASMMouseListener(assemblerTable));

		final TableColumnModel columnModel = assemblerTable.getColumnModel();
		final ASMTableCellRenderer cellRenderer = new ASMTableCellRenderer(segment, marked);
		columnModel.getColumn(0).setCellRenderer(cellRenderer);
		columnModel.getColumn(1).setCellRenderer(cellRenderer);
		columnModel.getColumn(2).setCellRenderer(cellRenderer);
		columnModel.getColumn(3).setCellRenderer(cellRenderer);
		columnModel.getColumn(4).setCellRenderer(cellRenderer);

		setCellWidth(columnModel.getColumn(0), 1);
		setCellWidth(columnModel.getColumn(1), 8);
		setCellWidth(columnModel.getColumn(2), 17);
		setCellWidth(columnModel.getColumn(3), 10);
		setCellWidth(columnModel.getColumn(4), 23);
		return assemblerTable;
	}

	private void setCellWidth(TableColumn tc, int widthFactor) {
		tc.setMinWidth(widthFactor * DEFAULT_TABLE_FONT_WIDTH);
		tc.setPreferredWidth(widthFactor * DEFAULT_TABLE_FONT_WIDTH);
		tc.setMaxWidth(widthFactor * DEFAULT_TABLE_FONT_WIDTH);
	}

	private final class ASMMouseListener extends MouseAdapter {
		private final JTable assemblerTable;

		public ASMMouseListener(JTable assemblerTable) {
			this.assemblerTable = assemblerTable;
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			if (getTreeRoot() == null || e.getClickCount() < 2 || e.getButton() != MouseEvent.BUTTON1) {
				return;
			}

			final ASMTableModel model = (ASMTableModel) assemblerTable.getModel();
			final Instruction inst = model.getInst(assemblerTable.rowAtPoint(e.getPoint()));
			if (inst == null || (!inst.isBranch() && !inst.isMemoryAccess())) {
				return;
			}
			final AddressInfo info = model.getImage().getInfo(inst.getAddress());

			if (!info.getJumpTargets().isEmpty()) {
				if (info.getJumpTargets().size() == 1) {
					gotoJumpTarget(info.getJumpTargets().get(), inst);
					return;
				}
				final JPopupMenu menu = new JPopupMenu("Select target");
				info.getJumpTargets().toSortedSet().forEach(jt -> {
					jt.image()
						.getMethodsFor(jt.address())
						.map(AddressInfo::getMethodInfo)
						.map(MethodInfo::getName)
						.forEach(name -> {
							JMenuItem item = new JMenuItem(String.format("[%s] %s", jt.address(), name));
							item.addActionListener(evt -> gotoJumpTarget(jt, inst));
							menu.add(item);
						});
				});
				menu.show((Component) e.getSource(), e.getX(), e.getY());
			} else if (!info.getVariableRefs().isEmpty() && info.getVariableRefs().size() <= 20) {
				if (info.getVariableRefs().size() == 1) {
					gotoVariable(info.getVariableRefs().get());
					return;
				}
				final JPopupMenu menu = new JPopupMenu("Select target");
				info.getVariableRefs().forEach(v -> {
					JMenuItem item = new JMenuItem(String.format("[%s](%s)%s", v.info().getRef().address(),
						v.getAccess(info.getRef())
							.map(VariableAccess::accessType)
							.map(VariableAccessType::shortName)
							.orElse("unknown access"),
						v));
					item.addActionListener(evt -> gotoVariable(v));
					menu.add(item);
				});
				menu.show((Component) e.getSource(), e.getX(), e.getY());
			}
		}

		private void gotoJumpTarget(MemoryRef ref, Instruction src) {
			final AddressInfo method = ref.image().getMethodsFor(ref.address()).getOrNull();
			if (method != null) {
				final TreePath path = ((ContentTreeModel) tree.getModel()).pathTo(method);
				tree.setSelectionPath(path);
				tree.scrollPathToVisible(path);
				if (!src.isMethodCall() && !src.isMethodFinisher()) {
					final ASMTableModel model = (ASMTableModel) assemblerTable.getModel();
					Array.range(0, model.getRowCount()).find(i -> isTargetRow(model, ref.address(), i)).peek(i -> {
						assemblerTable.getSelectionModel().clearSelection();
						assemblerTable.getSelectionModel().addSelectionInterval(i, i);
						assemblerTable.scrollRectToVisible(assemblerTable.getCellRect(i, 0, true));
					});
				}
			}
		}

		private void gotoVariable(Variable v) {
			final TreePath path = ((ContentTreeModel) tree.getModel()).pathTo(v);
			tree.setSelectionPath(path);
			tree.scrollPathToVisible(path);
		}
	}

	private static boolean isTargetRow(ASMTableModel model, Address16bit target, int i) {
		final Instruction rowInst = model.getInst(i);
		return rowInst != null && rowInst.getAddress().equals(target);
	}
}
