package ui.code;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.function.Supplier;

import javax.annotation.Nonnull;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.tree.TreePath;

import io.vavr.API;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;
import io.vavr.collection.SortedSet;

import assembly.Address16bit;
import assembly.Instruction;
import assembly.Mnemonics;
import code.AddressInfo;
import code.Image;
import code.Project;

public class MethodListDialogController {
	private JDialog dialog;
	private JPanel panel;
	private JLabel labelSearch;
	private JCheckBox comboShortOnly;
	private JCheckBox comboHasInOutInt;
	private JTextField textSearch;
	private JButton buttonClear;
	private JScrollPane scrollPane;
	private JTable table;
	private MethodListTableModel tableModel;

	private final JTree segmentTree;
	private final Supplier<Project> projectSupplier;

	private SortedSet<AddressInfo> methods = API.SortedSet();
	private Seq<AddressInfo> content = API.Seq();

	public MethodListDialogController(@Nonnull JTree segmentTree, @Nonnull Supplier<Project> projectSupplier) {
		this.segmentTree = segmentTree;
		this.projectSupplier = projectSupplier;
	}

	public void show() {
		update();
		getDialog().setVisible(true);
	}

	public void filterAndSort() {
		final int row = getTable().getSelectedRow();
		final AddressInfo selectedMethod = row != -1 ? content.get(row) : null;

		SortedSet<AddressInfo> infos = methods;

		final String text = getTextSearch().getText();
		if (text != null && !text.trim().isEmpty())
			infos = infos.filter(info -> info.getMethodInfo().getName().contains(text));

		if (getComboShortOnly().isSelected())
			infos = infos.filter(info -> info.getInstructions().size() < 20);

		if (getComboHasInOutInt().isSelected())
			infos = infos.filter(info -> hasIN_OUT_INT(info.getInstructions()));

		content = infos.toArray();

		getTable().tableChanged(new TableModelEvent(getTable().getModel()));

		if (selectedMethod != null) {
			int newRow = content.indexOf(selectedMethod);
			if (newRow == -1 && row < content.size())
				newRow = row;
			if (newRow != -1)
				getTable().setRowSelectionInterval(newRow, newRow);
		}
	}

	private boolean hasIN_OUT_INT(SortedSet<Instruction> instructions) {
		return instructions.exists(inst -> Mnemonics.IN.equals(inst.getMnemonic())
			|| Mnemonics.OUT.equals(inst.getMnemonic()) || Mnemonics.INT.equals(inst.getMnemonic()));
	}

	public void update() {
		final Project project = getProject();
		if (project == null) {
			methods = API.SortedSet();
		} else {
			methods = project.getImages().flatMap(Image::getMethodinfos).toSortedSet();
		}
		filterAndSort();
	}

	public Project getProject() {
		return projectSupplier.get();
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	private JDialog getDialog() {
		if (dialog == null) {
			dialog = new JDialog();
			dialog.setBounds(100, 100, 800, 600);
			dialog.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
			dialog.getContentPane().add(getPanel(), BorderLayout.NORTH);
			dialog.getContentPane().add(getScrollPane(), BorderLayout.CENTER);
		}
		return dialog;
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			panel.add(getLabelSearch());
			panel.add(getComboShortOnly());
			panel.add(getComboHasInOutInt());
			panel.add(getTextSearch());
			panel.add(getButtonClear());
		}
		return panel;
	}

	private JLabel getLabelSearch() {
		if (labelSearch == null) {
			labelSearch = new JLabel("Search:");
		}
		return labelSearch;
	}

	private JCheckBox getComboShortOnly() {
		if (comboShortOnly == null) {
			comboShortOnly = new JCheckBox("Short(<20 inst)");
			comboShortOnly.addChangeListener(e -> filterAndSort());
		}
		return comboShortOnly;
	}

	private JCheckBox getComboHasInOutInt() {
		if (comboHasInOutInt == null) {
			comboHasInOutInt = new JCheckBox("Has IN, OUT, INT inst");
			comboHasInOutInt.addChangeListener(e -> filterAndSort());
		}
		return comboHasInOutInt;
	}

	private JTextField getTextSearch() {
		if (textSearch == null) {
			textSearch = new JTextField();
			textSearch.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					final String text = getTextSearch().getText();
					getButtonClear().setEnabled(text != null && !text.trim().isEmpty());
				}
			});
			textSearch.addActionListener(e -> {
				filterAndSort();
			});
			textSearch.setColumns(10);
		}
		return textSearch;
	}

	private JButton getButtonClear() {
		if (buttonClear == null) {
			buttonClear = new JButton("");
			buttonClear.setEnabled(false);
			buttonClear.addActionListener(e -> {
				getTextSearch().setText(null);
				getButtonClear().setEnabled(false);
				filterAndSort();
			});
			buttonClear.setIcon(new ImageIcon(
				MethodListDialogController.class.getResource("/com/jtattoo/plaf/icons/large/closer_12x12.png")));
		}
		return buttonClear;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}

	private JTable getTable() {
		if (table == null) {
			table = new JTable();
			table.getSelectionModel().addListSelectionListener(e -> {
				if (e.getValueIsAdjusting())
					return;
				final int row = getTable().getSelectedRow();
				if (row != -1)
					gotoMethod(content.get(row));
			});
			table.setDoubleBuffered(true);
			table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			table.setModel(getTableModel());
		}
		return table;
	}

	private MethodListTableModel getTableModel() {
		if (tableModel == null) {
			tableModel = new MethodListTableModel();
		}
		return tableModel;
	}

	private void gotoMethod(AddressInfo info) {
		final TreePath path = ((ContentTreeModel) segmentTree.getModel()).pathTo(info);

		segmentTree.setSelectionPath(path);
		segmentTree.scrollPathToVisible(path);
	}

	private final class MethodListTableModel extends AbstractTableModel {
		private static final String[] COLUMNS = new String[] { "Image", "Address", "Name", "Instruction count" };
		private static final Class<?>[] CLASSES = new Class[] { Image.class, Address16bit.class, String.class,
			Integer.class };

		@Override
		public int getRowCount() {
			return content.size();
		}

		@Override
		public int getColumnCount() {
			return COLUMNS.length;
		}

		@Override
		public String getColumnName(int columnIndex) {
			return COLUMNS[columnIndex];
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			return CLASSES[columnIndex];
		}

		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return columnIndex == 2;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			final AddressInfo info = content.get(rowIndex);
			return switch (columnIndex) {
				case 0 -> info.getImage();
				case 1 -> info.getAddress();
				case 2 -> info.getMethodInfo().getName();
				case 3 -> info.getInstructions().size();
				default -> null;
			};
		}

		@Override
		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			content.get(rowIndex).setMethodName(aValue.toString());
		}

		private Set<TableModelListener> listeners = API.Set();

		@Override
		public void addTableModelListener(TableModelListener l) {
			listeners = listeners.add(l);
		}

		@Override
		public void removeTableModelListener(TableModelListener l) {
			listeners = listeners.remove(l);
		}
	}
}
