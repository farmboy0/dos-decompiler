package ui.code;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.function.Supplier;

import javax.annotation.Nonnull;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.tree.TreePath;

import io.vavr.API;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;
import io.vavr.collection.SortedSet;

import assembly.Address16bit;
import code.AddressInfo;
import code.AddressInfo.Variable;
import code.DataBlock;
import code.Image;
import code.Project;
import code.Segment;

public class VariableListDialogController {
	private JDialog dialog;
	private JPanel panel;
	private JLabel labelSearch;
	private JCheckBox comboStringsOnly;
	private JCheckBox comboConstOnly;
	private JTextField textSearch;
	private JButton buttonClear;
	private JScrollPane scrollPane;
	private JTable table;
	private VariableListTableModel tableModel;

	private final JTree segmentTree;
	private final Supplier<Project> projectSupplier;

	private SortedSet<DataBlock> variables = API.SortedSet();
	private Seq<DataBlock> content = API.Seq();

	public VariableListDialogController(@Nonnull JTree segmentTree, @Nonnull Supplier<Project> projectSupplier) {
		this.segmentTree = segmentTree;
		this.projectSupplier = projectSupplier;
	}

	public void show() {
		update();
		getDialog().setVisible(true);
	}

	public void filterAndSort() {
		final int row = getTable().getSelectedRow();
		final DataBlock selectedBlock = row != -1 ? content.get(row) : null;

		SortedSet<DataBlock> bloecke = variables;

		final String text = getTextSearch().getText();
		if (text != null && !text.trim().isEmpty())
			bloecke = bloecke.filter(block -> block.getName().contains(text));

		if (getComboStringsOnly().isSelected())
			bloecke = bloecke
				.filter(block -> block.byteLength() >= 3 && block.getContentAsString().matches("^[0-9A-Za-z]+$"));

		if (getComboConstOnly().isSelected())
			bloecke = bloecke.filter(DataBlock::isConst);

		content = bloecke.toArray();

		getTable().tableChanged(new TableModelEvent(getTable().getModel()));

		if (selectedBlock != null) {
			int newRow = content.indexOf(selectedBlock);
			if (newRow == -1 && row < content.size())
				newRow = row;
			if (newRow != -1)
				getTable().setRowSelectionInterval(newRow, newRow);
		}
	}

	public void update() {
		final Project project = getProject();
		if (project == null) {
			variables = API.SortedSet();
		} else {
			variables = project.getImages()
				.flatMap(Image::getSegments)
				.flatMap(Segment::getInfos)
				.flatMap(AddressInfo::getVariables)
				.map(Variable::getDataBlock)
				.toSortedSet();
		}
		filterAndSort();
	}

	public Project getProject() {
		return projectSupplier.get();
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	private JDialog getDialog() {
		if (dialog == null) {
			dialog = new JDialog();
			dialog.setBounds(100, 100, 800, 600);
			dialog.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
			dialog.getContentPane().add(getPanel(), BorderLayout.NORTH);
			dialog.getContentPane().add(getScrollPane(), BorderLayout.CENTER);
		}
		return dialog;
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			panel.add(getLabelSearch());
			panel.add(getComboStringsOnly());
			panel.add(getComboConstOnly());
			panel.add(getTextSearch());
			panel.add(getButtonClear());
		}
		return panel;
	}

	private JLabel getLabelSearch() {
		if (labelSearch == null) {
			labelSearch = new JLabel("Search:");
		}
		return labelSearch;
	}

	private JCheckBox getComboStringsOnly() {
		if (comboStringsOnly == null) {
			comboStringsOnly = new JCheckBox("Strings only");
			comboStringsOnly.addChangeListener(e -> filterAndSort());
		}
		return comboStringsOnly;
	}

	private JCheckBox getComboConstOnly() {
		if (comboConstOnly == null) {
			comboConstOnly = new JCheckBox("Constants only");
			comboConstOnly.addChangeListener(e -> filterAndSort());
		}
		return comboConstOnly;
	}

	private JTextField getTextSearch() {
		if (textSearch == null) {
			textSearch = new JTextField();
			textSearch.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					final String text = getTextSearch().getText();
					getButtonClear().setEnabled(text != null && !text.trim().isEmpty());
				}
			});
			textSearch.addActionListener(e -> {
				filterAndSort();
			});
			textSearch.setColumns(10);
		}
		return textSearch;
	}

	private JButton getButtonClear() {
		if (buttonClear == null) {
			buttonClear = new JButton("");
			buttonClear.setEnabled(false);
			buttonClear.addActionListener(e -> {
				getTextSearch().setText(null);
				getButtonClear().setEnabled(false);
				filterAndSort();
			});
			buttonClear.setIcon(new ImageIcon(
				VariableListDialogController.class.getResource("/com/jtattoo/plaf/icons/large/closer_12x12.png")));
		}
		return buttonClear;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}

	private JTable getTable() {
		if (table == null) {
			table = new JTable();
			table.getSelectionModel().addListSelectionListener(e -> {
				if (e.getValueIsAdjusting())
					return;
				final int row = getTable().getSelectedRow();
				if (row != -1)
					gotoVariable(content.get(row).getVariable());
			});
			table.setDoubleBuffered(true);
			table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			table.setModel(getTableModel());
		}
		return table;
	}

	private VariableListTableModel getTableModel() {
		if (tableModel == null) {
			tableModel = new VariableListTableModel();
		}
		return tableModel;
	}

	private void gotoVariable(Variable v) {
		final TreePath path = ((ContentTreeModel) segmentTree.getModel()).pathTo(v);
		segmentTree.setSelectionPath(path);
		segmentTree.scrollPathToVisible(path);
	}

	private final class VariableListTableModel extends AbstractTableModel {
		private static final String[] COLUMNS = new String[] { "Image", "Address", "Constant", "Name", "Content" };
		private static final Class<?>[] CLASSES = new Class[] { Image.class, Address16bit.class, Boolean.class,
			String.class, String.class };

		@Override
		public int getRowCount() {
			return content.size();
		}

		@Override
		public int getColumnCount() {
			return COLUMNS.length;
		}

		@Override
		public String getColumnName(int columnIndex) {
			return COLUMNS[columnIndex];
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			return CLASSES[columnIndex];
		}

		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return columnIndex == 3;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			final DataBlock block = content.get(rowIndex);
			return switch (columnIndex) {
				case 0 -> block.getInfo().getImage();
				case 1 -> block.getInfo().getAddress();
				case 2 -> block.isConst();
				case 3 -> block.getVariable().toString();
				case 4 -> block.getContentAsString();
				default -> null;
			};
		}

		@Override
		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			content.get(rowIndex).setName(aValue.toString());
		}

		private Set<TableModelListener> listeners = API.Set();

		@Override
		public void addTableModelListener(TableModelListener l) {
			listeners = listeners.add(l);
		}

		@Override
		public void removeTableModelListener(TableModelListener l) {
			listeners = listeners.remove(l);
		}
	}
}
