package ui;

import static java.awt.event.InputEvent.CTRL_DOWN_MASK;
import static java.awt.event.KeyEvent.VK_Q;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;
import static javax.swing.JComponent.WHEN_IN_FOCUSED_WINDOW;
import static javax.swing.KeyStroke.getKeyStroke;
import static javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
import static javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import static javax.swing.tree.TreeSelectionModel.SINGLE_TREE_SELECTION;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.TreePath;

import io.vavr.API;
import io.vavr.collection.Set;
import io.vavr.collection.SortedSet;

import assembly.Address16bit;
import assembly.Instruction;
import code.AddressInfo;
import code.AddressInfo.VariableAccess;
import code.AnalyzerMonitor;
import code.AnalyzerTask;
import code.DataBlock;
import code.Image;
import code.Image.InitialStateMap;
import code.Project;
import code.Segment;
import code.Segment.CodeModificator;
import data.DOSProgramFile;
import data.ProjectFile;
import jdos.debug.Debug;
import jdos.gui.MainFrame;
import jdosbox.DebugListenerImpl;
import ui.code.ContentTreeModel;
import ui.code.DisassemblyPanel;
import ui.code.FileBasedContentTreeModel;
import ui.code.ImageBasedContentTreeModel;
import ui.code.MethodListDialogController;
import ui.code.VariableListDialogController;

public class DesktopFrame implements ExceptionHandler {
	private static final String ERROR_DURING_SAVE = "Error during save";

	private static final Font DEFAULT_TABLE_FONT = new Font(Font.MONOSPACED, Font.BOLD, 16);

	private static final ScheduledExecutorService WORKER = Executors.newScheduledThreadPool(1,
		r -> new Thread(r, "Project Worker"));

	private static final ScheduledExecutorService UPDATER = Executors.newScheduledThreadPool(1,
		r -> new Thread(r, "Tree Updater"));

	private boolean update = false;
	private boolean inUpdate = false;
	private ProjectFile projectFile;

	public DesktopFrame() {
		Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
			System.err.println("Uncaught exception:");
			e.printStackTrace(System.err);
		});
		UPDATER.scheduleAtFixedRate(this::onDirtyProject, 0, 5, TimeUnit.SECONDS);
	}

	private void onDirtyProject() {
		if (!inUpdate && update) {
			inUpdate = true;
			update = false;
			updateProject();
			inUpdate = false;
		}
	}

	public void show() {
		getFrame().setVisible(true);
		getFrame().pack();
		getSplitPane().setDividerLocation(0.4);
		getSplitPane().setResizeWeight(0.5);
		getFrame().requestFocus();
	}

	public void openBinary(@Nonnull File selectedBinary) {
		try {
			DOSProgramFile bf = DOSProgramFile.create(selectedBinary);
			setProjectFile(new ProjectFile(new Project(bf)));
			WORKER.execute(this::disassembleProject);
		} catch (IOException e) {
			handleException("Error parsing binary", e);
		}
	}

	public void openProject(@Nonnull File selectedFile) {
		setProjectFile(new ProjectFile(selectedFile));
		this.update = true;
	}

	public void saveProjectAs() throws IOException {
		int result = getProjectChooser().showSaveDialog(getFrame());
		if (result != JFileChooser.APPROVE_OPTION)
			return;
		getProjectFile().saveAs(getProjectChooser().getSelectedFile());
	}

	public void setProjectFile(ProjectFile projectFile) {
		update = false;
		if (projectFile == null) {
			getActionShowVariables().setEnabled(false);
			getActionShowMethods().setEnabled(false);
			getActionDosboxConfig().setEnabled(false);
			getActionDosboxStart().setEnabled(false);
			saveProject.setEnabled(false);
			saveProjectAs.setEnabled(false);
			closeProject.setEnabled(false);
			this.projectFile = null;
		} else {
			this.projectFile = projectFile;
			getActionShowMethods().setEnabled(true);
			getActionShowVariables().setEnabled(true);
			getActionDosboxConfig().setEnabled(true);
			getActionDosboxStart().setEnabled(true);
			saveProject.setEnabled(getProjectFile().canSave());
			saveProjectAs.setEnabled(true);
			closeProject.setEnabled(true);
			projectFile.getProject().addListener(() -> update = true);
		}
		getCurrentTreeModel().update();
		getShowVariables().update();
		getShowMethods().update();
	}

	private void disassembleProject() {
		final Project project = getProject();
		if (project == null)
			return;
		try {
			project.disassemble();
		} catch (Exception e) {
			handleException("Error disassembling " + project.getName(), e);
		}
		this.update = true;
	}

	private void updateProject() {
		final Project project = getProject();
		if (project == null)
			return;
		try {
			project.update(new ActivityMonitor());
		} catch (Exception e) {
			handleException("Error updating " + project.getName(), e);
		} finally {
			getCurrentTreeModel().update();
			getShowVariables().update();
			getShowMethods().update();
		}
	}

	private boolean saveIfNeededBeforeClosing() {
		final Project project = getProject();
		if (project == null || !project.isDirty())
			return true;
		final int option = JOptionPane.showConfirmDialog(getFrame(),
			"The project has unsaved changes. Save before closing?", "Save before closing?",
			JOptionPane.YES_NO_CANCEL_OPTION);
		if (option == JOptionPane.CANCEL_OPTION)
			return false;
		if (option == JOptionPane.YES_OPTION) {
			try {
				if (getProjectFile().canSave())
					getProjectFile().save();
				else
					saveProjectAs();
			} catch (IOException e) {
				handleException(ERROR_DURING_SAVE, e);
			}
		}
		return true;
	}

	private ProjectFile getProjectFile() {
		return this.projectFile;
	}

	private Project getProject() {
		if (getProjectFile() == null)
			return null;
		return getProjectFile().getProject();
	}

	private JFrame frame;

	private JFrame getFrame() {
		if (frame == null) {
			frame = new JFrame("DOS Decompiler");
			frame.setLocationByPlatform(true);
			frame.setJMenuBar(getMainMenu());
			frame.addWindowListener(new DesktopWindowAdapter());
			frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			frame.setLayout(new BorderLayout());
			final Dimension size = new Dimension(1280, 600);
			frame.setMinimumSize(size);
			frame.setPreferredSize(size);
			frame.add(getSplitPane(), BorderLayout.CENTER);
			frame.add(getBottomBar(), BorderLayout.SOUTH);

			final JComponent root = frame.getRootPane();
			root.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(getKeyStroke(VK_Q, CTRL_DOWN_MASK), "QUIT");
			root.getActionMap().put("QUIT", new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					quit();
				}
			});

		}
		return frame;
	}

	private JMenuBar mainMenu;
	private JMenuItem saveProject;
	private JMenuItem saveProjectAs;
	private JMenuItem closeProject;

	private JMenuBar getMainMenu() {
		if (mainMenu == null) {
			final JMenu decompiler = new JMenu("Decompiler");

			final JMenuItem newProject = decompiler.add("New Project");
			newProject.addActionListener(ev -> {
				if (saveIfNeededBeforeClosing()) {
					Project project = new Project();
					setProjectFile(new ProjectFile(project));
				}
			});

			final JMenuItem openBinary = decompiler.add("Open Binary");
			openBinary.addActionListener(ev -> {
				int result = getChooser().showOpenDialog(frame);
				if (result == JFileChooser.APPROVE_OPTION) {
					openBinary(getChooser().getSelectedFile());
				}
			});

			final JMenuItem openProject = decompiler.add("Open Project");
			openProject.addActionListener(ev -> {
				if (saveIfNeededBeforeClosing()) {
					int result = getProjectChooser().showOpenDialog(getFrame());
					if (result == JFileChooser.APPROVE_OPTION) {
						openProject(getProjectChooser().getSelectedFile());
					}
				}
			});

			saveProject = decompiler.add("Save Project");
			saveProject.setEnabled(false);
			saveProject.addActionListener(ev -> {
				try {
					getProjectFile().save();
				} catch (IOException e) {
					handleException(ERROR_DURING_SAVE, e);
				}
			});

			saveProjectAs = decompiler.add("Save Project As");
			saveProjectAs.setEnabled(false);
			saveProjectAs.addActionListener(ev -> {
				try {
					saveProjectAs();
				} catch (IOException e) {
					handleException(ERROR_DURING_SAVE, e);
				}
			});

			closeProject = decompiler.add("Close Project");
			closeProject.setEnabled(false);
			closeProject.addActionListener(ev -> {
				if (saveIfNeededBeforeClosing()) {
					setProjectFile(null);
				}
			});

			decompiler.addSeparator();

			final JMenuItem quit = decompiler.add("Quit");
			quit.setMnemonic(VK_Q);
			quit.addActionListener(ev -> quit());

			mainMenu = new JMenuBar();
			mainMenu.add(decompiler);
			mainMenu.add(getMenuView());
			mainMenu.add(getMenuDosBox());
		}
		return mainMenu;
	}

	private JMenu menuView;

	private JMenu getMenuView() {
		if (menuView == null) {
			menuView = new JMenu("View");
			menuView.add(getTreeViewMenu());
			menuView.add(getActionShowVariables());
			menuView.add(getActionShowMethods());
		}
		return menuView;
	}

	private JMenu treeViewMenu;

	private JMenu getTreeViewMenu() {
		if (treeViewMenu == null) {
			treeViewMenu = new JMenu("Tree View");
			treeViewMenu.add(getFileBasedTreeViewMenuItem());
			treeViewMenu.add(getImageBasedTreeViewMenuItem());
		}
		return treeViewMenu;
	}

	private JMenuItem fileBasedTreeViewMenuItem;

	private JMenuItem getFileBasedTreeViewMenuItem() {
		if (fileBasedTreeViewMenuItem == null) {
			fileBasedTreeViewMenuItem = new JRadioButtonMenuItem("File-Based");
			fileBasedTreeViewMenuItem.addActionListener(e -> {
				getImageBasedTreeViewMenuItem().setSelected(false);
				getFileBasedTreeViewMenuItem().setSelected(true);
				getContentTree().setModel(getFileBasedTreeModel());
				getContentTree().invalidate();
				getContentTree().repaint();
			});
			fileBasedTreeViewMenuItem.setSelected(true);
		}
		return fileBasedTreeViewMenuItem;
	}

	private JMenuItem imageBasedTreeViewMenuItem;

	private JMenuItem getImageBasedTreeViewMenuItem() {
		if (imageBasedTreeViewMenuItem == null) {
			imageBasedTreeViewMenuItem = new JRadioButtonMenuItem("Image-Based");
			imageBasedTreeViewMenuItem.addActionListener(e -> {
				getImageBasedTreeViewMenuItem().setSelected(true);
				getFileBasedTreeViewMenuItem().setSelected(false);
				getContentTree().setModel(getImageBasedTreeModel());
				getContentTree().invalidate();
				getContentTree().repaint();
			});
			imageBasedTreeViewMenuItem.setSelected(false);
		}
		return imageBasedTreeViewMenuItem;
	}

	private JMenuItem actionShowVariables;

	private JMenuItem getActionShowVariables() {
		if (actionShowVariables == null) {
			actionShowVariables = new JMenuItem("Show Variables");
			actionShowVariables.setEnabled(false);
			actionShowVariables.addActionListener(ev -> {
				getShowVariables().show();
			});
		}
		return actionShowVariables;
	}

	private VariableListDialogController showVariables;

	private VariableListDialogController getShowVariables() {
		if (showVariables == null) {
			showVariables = new VariableListDialogController(getContentTree(), this::getProject);
		}
		return showVariables;
	}

	private JMenuItem actionShowMethods;

	private JMenuItem getActionShowMethods() {
		if (actionShowMethods == null) {
			actionShowMethods = new JMenuItem("Show Methods");
			actionShowMethods.setEnabled(false);
			actionShowMethods.addActionListener(ev -> {
				getShowMethods().show();
			});
		}
		return actionShowMethods;
	}

	private MethodListDialogController showMethods;

	private MethodListDialogController getShowMethods() {
		if (showMethods == null) {
			showMethods = new MethodListDialogController(getContentTree(), this::getProject);
		}
		return showMethods;
	}

	private JMenu menuDosBox;

	private JMenu getMenuDosBox() {
		if (menuDosBox == null) {
			menuDosBox = new JMenu("DosBox");
			menuDosBox.add(getActionDosboxConfig());
			menuDosBox.add(getActionDosboxStart());
		}
		return menuDosBox;
	}

	private JMenuItem actionDosboxConfig;

	private JMenuItem getActionDosboxConfig() {
		if (actionDosboxConfig == null) {
			actionDosboxConfig = new JMenuItem("Edit Config");
			actionDosboxConfig.setEnabled(false);
			actionDosboxConfig.addActionListener(ev -> {
				getDosBoxConfig().show();
			});
		}
		return actionDosboxConfig;
	}

	private DosboxConfigDialogController dosboxConfig;

	private DosboxConfigDialogController getDosBoxConfig() {
		if (dosboxConfig == null) {
			dosboxConfig = new DosboxConfigDialogController(this::getProject);
		}
		return dosboxConfig;
	}

	private JMenuItem actionDosboxStart;

	private JMenuItem getActionDosboxStart() {
		if (actionDosboxStart == null) {
			actionDosboxStart = new JMenuItem("Start");
			actionDosboxStart.setEnabled(false);
			actionDosboxStart.addActionListener(ev -> {
				getProject().reset();
				Debug.listener = new DebugListenerImpl(getProject());
				try {
					final File configFile = File.createTempFile("dosboxConfig", "conf");
					final File mapperFile = File.createTempFile("dosboxKeyMap", "txt");
					Files.writeString(configFile.toPath(), getProject().getDosboxConfig(), CREATE, TRUNCATE_EXISTING);
					Files.writeString(mapperFile.toPath(), getProject().getDosboxKeyMap(), CREATE, TRUNCATE_EXISTING);
					final String[] args = new String[] { "-conf", configFile.getAbsolutePath(), "-mapper",
						mapperFile.getAbsolutePath() };
					MainFrame.main(args);
				} catch (AWTException | IOException e) {
					handleException("Error starting dosbox", e);
				}
			});
		}
		return actionDosboxStart;
	}

	private JFileChooser chooser;

	private JFileChooser getChooser() {
		if (chooser == null) {
			chooser = new JFileChooser((File) null);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setDialogType(JFileChooser.OPEN_DIALOG);
			chooser.setDialogTitle("Choosea binary to decompile");
			chooser.setMultiSelectionEnabled(false);
			chooser.setAcceptAllFileFilterUsed(false);
			chooser.setFileFilter(new FileFilter() {

				@Override
				public String getDescription() {
					return "Dos Binaries (COM,EXE)";
				}

				@Override
				public boolean accept(File f) {
					if (!f.isFile()) {
						return true;
					}
					final String name = f.getName().toLowerCase();
					return name.endsWith(".exe") || name.endsWith(".com");
				}
			});
		}
		return chooser;
	}

	private JFileChooser projectChooser;

	private JFileChooser getProjectChooser() {
		if (projectChooser == null) {
			projectChooser = new JFileChooser((File) null);
			projectChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			projectChooser.setDialogType(JFileChooser.OPEN_DIALOG);
			projectChooser.setDialogTitle("Choosea project to load");
			projectChooser.setMultiSelectionEnabled(false);
			projectChooser.setAcceptAllFileFilterUsed(false);
			projectChooser.setFileFilter(new FileFilter() {

				@Override
				public String getDescription() {
					return "Dos Decompiler Projects (.decj)";
				}

				@Override
				public boolean accept(File f) {
					if (!f.isFile()) {
						return true;
					}
					final String name = f.getName().toLowerCase();
					return name.endsWith(".decj");
				}
			});
		}
		return projectChooser;
	}

	private JSplitPane splitPane;

	private JSplitPane getSplitPane() {
		if (splitPane == null) {
			splitPane = new JSplitPane();
			splitPane.setLeftComponent(getLeftScroll());
			splitPane.setRightComponent(getRightScroll());
		}
		return splitPane;
	}

	private JPanel bottomBar;

	private JPanel getBottomBar() {
		if (bottomBar == null) {
			bottomBar = new JPanel();
			bottomBar.add(getActivityMonitor());
		}
		return bottomBar;
	}

	private JScrollPane leftScroll;

	private JScrollPane getLeftScroll() {
		if (leftScroll == null) {
			leftScroll = new JScrollPane(getContentTree());
			leftScroll.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_AS_NEEDED);
			leftScroll.setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_AS_NEEDED);
		}
		return leftScroll;
	}

	private JScrollPane rightScroll;

	private JScrollPane getRightScroll() {
		if (rightScroll == null) {
			rightScroll = new JScrollPane();
			rightScroll.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_AS_NEEDED);
			rightScroll.setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_AS_NEEDED);
			rightScroll.getVerticalScrollBar().setUnitIncrement(50);
		}
		return rightScroll;
	}

	private ImageBasedContentTreeModel imageBasedTreeModel;

	private ImageBasedContentTreeModel getImageBasedTreeModel() {
		if (imageBasedTreeModel == null) {
			imageBasedTreeModel = new ImageBasedContentTreeModel(this::getProject);
		}
		return imageBasedTreeModel;
	}

	private FileBasedContentTreeModel fileBasedTreeModel;

	private FileBasedContentTreeModel getFileBasedTreeModel() {
		if (fileBasedTreeModel == null) {
			fileBasedTreeModel = new FileBasedContentTreeModel(this::getProject);
		}
		return fileBasedTreeModel;
	}

	public ContentTreeModel getCurrentTreeModel() {
		if (getFileBasedTreeViewMenuItem().isSelected())
			return getFileBasedTreeModel();
		return getImageBasedTreeModel();
	}

	private JTree contentTree;

	private JTree getContentTree() {
		if (contentTree == null) {
			contentTree = new JTree(getCurrentTreeModel());
			contentTree.setRootVisible(false);
			contentTree.setFont(DEFAULT_TABLE_FONT);
			contentTree.getSelectionModel().setSelectionMode(SINGLE_TREE_SELECTION);
			contentTree.addMouseListener(new TreeMouseListener());
			contentTree.addTreeSelectionListener(new TreeSelectionListenerImpl());
		}
		return contentTree;
	}

	private DisassemblyPanel disassemblyList;

	private DisassemblyPanel getDisassemblyList() {
		if (disassemblyList == null) {
			disassemblyList = new DisassemblyPanel(getContentTree());
		}
		return disassemblyList;
	}

	private JPanel emptyPanel;

	private JPanel getEmptyPanel() {
		if (emptyPanel == null) {
			emptyPanel = new JPanel();
		}
		return emptyPanel;
	}

	private JLabel activityMonitor;

	private JLabel getActivityMonitor() {
		if (activityMonitor == null) {
			activityMonitor = new JLabel();
			activityMonitor.setText("No background activity");
		}
		return activityMonitor;
	}

	@Override
	public void handleException(@Nonnull String title, @Nonnull Exception e) {
		e.printStackTrace(System.err);
		JOptionPane.showMessageDialog(frame, e.getMessage(), title, JOptionPane.ERROR_MESSAGE);
	}

	private void quit() {
		saveIfNeededBeforeClosing();
		saveSettings();
		System.exit(0);
	}

	private void loadSettings() {
	}

	private void saveSettings() {
		File cofigPath = getConfigPath();
		if (!cofigPath.exists()) {
			boolean result = cofigPath.mkdirs();
			if (!result) {
				JOptionPane.showMessageDialog(frame, "Directory couldn't be created.", "Error writing settings file",
					JOptionPane.ERROR_MESSAGE);
				return;
			}
		}
		File config = new File(cofigPath, "uisettings.properties");
		try (FileChannel fc = FileChannel.open(config.toPath(), CREATE, WRITE, TRUNCATE_EXISTING);) {
			//			settings.writeTo(fc);
		} catch (IOException e) {
			handleException("Error writing settings file", e);
		}
	}

	@Nonnull
	private File getConfigPath() {
		File parent = null;
		if (System.getenv("XDG_CONFIG_DIR") != null) {
			parent = new File(System.getenv("XDG_CONFIG_DIR"));
		} else if (System.getProperty("user.home") != null) {
			parent = new File(System.getProperty("user.home"), ".config");
		} else {
			parent = new File(System.getProperty("user.dir"));
		}
		return new File(parent, "dos-decompiler");
	}

	private final class TreeMouseListener extends MouseAdapter {
		@Override
		public void mousePressed(MouseEvent e) {
			mouseClicked(e);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			mouseClicked(e);
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			if (!e.isPopupTrigger())
				return;
			final TreePath selectionPath = getContentTree().getSelectionModel().getSelectionPath();
			final Object selection = selectionPath.getLastPathComponent();
			final JPopupMenu menu = new JPopupMenu("Tree Actions");
			JMenuItem item = new JMenuItem("Rename");
			item.setEnabled(selection instanceof AddressInfo || selection instanceof DataBlock
				|| selection instanceof VariableAccess access && access.getSourceMethod() != null);
			item.addActionListener(evt -> {
				if (selection instanceof AddressInfo info) {
					if (info.isMethodStart()) {
						String value = JOptionPane.showInputDialog("Rename to...", info.getMethodInfo().getName());
						if (value != null) {
							info.getMethodInfo().setName(value);
							getProject().setDirty(true);
						}
					} else {
						String value = JOptionPane.showInputDialog("Rename to...", info.getDataBlock().getName());
						if (value != null) {
							info.getDataBlock().setName(value);
							getProject().setDirty(true);
						}
					}
				} else if (selection instanceof DataBlock data) {
					String value = JOptionPane.showInputDialog("Rename to...", data.getName());
					if (value != null) {
						data.setName(value);
						getProject().setDirty(true);
					}
				} else if (selection instanceof VariableAccess access && access.getSourceMethod() != null) {
					AddressInfo method = access.getSourceMethod();
					String value = JOptionPane.showInputDialog("Rename to...", method.getMethodInfo().getName());
					if (value != null) {
						method.getMethodInfo().setName(value);
						getProject().setDirty(true);
					}
				}
			});
			menu.add(item);
			menu.show((Component) e.getSource(), e.getX(), e.getY());
		}
	}

	private final class TreeSelectionListenerImpl implements TreeSelectionListener {
		@Override
		public void valueChanged(TreeSelectionEvent event) {
			final TreePath path = event.getPath();

			Component view = getEmptyPanel();
			if (path.getLastPathComponent() instanceof Segment segment) {
				view = handleSegmentSelection(segment);
			} else if (path.getLastPathComponent() instanceof InitialStateMap map) {
				view = handleInitialStateMapSelection(map);
			} else if (path.getLastPathComponent() instanceof AddressInfo info) {
				view = handleAddressInfoSelection(path, info);
			} else if (path.getLastPathComponent() instanceof CodeModificator mod) {
				view = handleCodeModSelection(mod);
			} else if (path.getLastPathComponent() instanceof DataBlock data) {
				view = handleDatablockSelection(data);
			} else if (path.getLastPathComponent() instanceof VariableAccess access) {
				view = handleVarAccessSelection(access);
			}
			getRightScroll().setViewportView(view);
			getRightScroll().revalidate();
			getRightScroll().repaint();
		}

		private Component handleSegmentSelection(Segment segment) {
			getDisassemblyList().update(segment);
			return getDisassemblyList();
		}

		private Component handleInitialStateMapSelection(InitialStateMap map) {
			return new JTable(map.getState(), map.getColumnNames());
		}

		private Component handleAddressInfoSelection(TreePath path, AddressInfo info) {
			if (info.isMethodStart()) {
				final Segment segment = info.getImage().getSegment(info.getAddress());
				final SortedSet<Instruction> blocks = info.getInstructions();
				Set<Address16bit> markers = API.Set();
				if (path.getParentPath() != null
					&& path.getParentPath().getLastPathComponent() instanceof AddressInfo callee) {
					markers = callee.getMethodInfo().getCallerInst(info).map(Instruction::getAddress);
				}
				getDisassemblyList().update(segment, blocks, markers);
			} else {
				getDisassemblyList().update(info.getDataBlock());
			}
			return getDisassemblyList();
		}

		private Component handleCodeModSelection(CodeModificator mod) {
			final Image image = mod.source().image();
			final Address16bit address = mod.source().address();
			final Segment segment = image.getSegment(address);
			final AddressInfo method = image.getMethodsFor(address).getOrNull();
			if (method != null) {
				final SortedSet<Instruction> blocks = method.getInstructions();
				getDisassemblyList().update(segment, blocks, API.Set(address));
			} else {
				final SortedSet<Instruction> blocks = API.SortedSet(segment.getInstruction(address.getOffset()));
				getDisassemblyList().update(segment, blocks, API.Set());
			}
			return getDisassemblyList();
		}

		private Component handleDatablockSelection(DataBlock data) {
			getDisassemblyList().update(data);
			return getDisassemblyList();
		}

		private Component handleVarAccessSelection(VariableAccess access) {
			final Address16bit address = access.source().address();
			final Segment segment = access.getSegment();
			final AddressInfo method = access.getSourceMethod();
			if (method != null) {
				final SortedSet<Instruction> blocks = method.getInstructions();
				getDisassemblyList().update(segment, blocks, API.Set(address));
			} else {
				final SortedSet<Instruction> blocks = API.SortedSet(segment.getInstruction(address.getOffset()));
				getDisassemblyList().update(segment, blocks, API.Set());
			}
			return getDisassemblyList();
		}
	}

	private final class DesktopWindowAdapter extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			quit();
		}
	}

	private final class ActivityMonitor implements AnalyzerMonitor {
		@Override
		public void taskStarted(AnalyzerTask task) {
//			System.out.println("Currently starting " + task.name());
			getActivityMonitor().setText("Currently running " + task.name());
		}

		@Override
		public void taskEnded(AnalyzerTask task) {
//			System.out.println("Currently ending " + task.name());
			getActivityMonitor().setText("No analyzer activity");
		}
	}
}
