package emulation.memory.impl;

import static java.nio.ByteOrder.LITTLE_ENDIAN;

import java.nio.ByteBuffer;
import java.util.function.Supplier;

import assembly.ArgumentSize;
import assembly.ByteSource;
import code.ByteBufferSource;
import code.Image;
import code.Project;
import emulation.memory.MemoryController;
import emulation.memory.MemorySegment;

public class MemoryControllerImpl implements MemoryController {
	private final Supplier<Project> projectSupplier;
	private final ByteBuffer memory;

	public MemoryControllerImpl(Supplier<Project> projectSupplier, int size) {
		this.projectSupplier = projectSupplier;
		memory = ByteBuffer.allocate(size).order(LITTLE_ENDIAN);
	}

	public Project getProject() {
		return projectSupplier.get();
	}

	@Override
	public void putImage(Image image) {
		memory.position(image.getStart().linearAddress()).put(image.getBuffer().array());
	}

	@Override
	public MemorySegment getSegment(int segment) {
		return new MemorySegment() {
			@Override
			public int read(int offset, ArgumentSize size) {
				return switch (size) {
					case _16bit -> read16(offset);
					case _8bit -> read8(offset);
					default -> throw new IllegalArgumentException("Unknown size: " + size);
				};
			}

			@Override
			public int read8(int offset) {
				final int off = toMemoryOffset(offset);
				return memory.get(off);
			}

			@Override
			public int read16(int offset) {
				final int off = toMemoryOffset(offset);
				return memory.getShort(off);
			}

			@Override
			public void write(int offset, int value, ArgumentSize size) {
				switch (size) {
					case _16bit -> write16(offset, value);
					case _8bit -> write8(offset, value);
					default -> throw new IllegalArgumentException("Unknown size: " + size);
				}
			}

			@Override
			public void write8(int offset, int value) {
				final int off = toMemoryOffset(offset);
				memory.put(off, (byte) value);
			}

			@Override
			public void write16(int offset, int value) {
				final int off = toMemoryOffset(offset);
				memory.putShort(off, (short) value);
			}

			@Override
			public ByteSource at(int offset) {
				final int off = toMemoryOffset(offset);
				return new ByteBufferSource(memory.duplicate().order(LITTLE_ENDIAN).position(off));
			}

			private int toMemoryOffset(int offset) {
				return (segment << 4) + offset;
			}
		};
	}
}
