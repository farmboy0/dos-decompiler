package emulation.memory;

import assembly.ArgumentSize;
import assembly.ByteSource;

public interface MemorySegment {
	int read(int offset, ArgumentSize size);

	int read8(int offset);

	int read16(int offset);

	void write(int offset, int value, ArgumentSize size);

	void write8(int offset, int value);

	void write16(int offset, int value);

	ByteSource at(int offset);
}
