package emulation.memory;

import code.Image;

public interface MemoryController {
	void putImage(Image image);

	MemorySegment getSegment(int segment);
}
