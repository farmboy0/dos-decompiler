package emulation.processor;

public interface RegisterValue {
	String getName();

	int get();

	void set(int value);

	void addListener(RegisterValueListener listener);

	void removeListener(RegisterValueListener listener);
}
