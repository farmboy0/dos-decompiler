package emulation.processor;

import io.vavr.collection.Seq;

import code.Image;

public interface ProcessorControl {
	void setUp(Image image);

	void start();

	void stepInto();

	void stepOver();

	void stepReturn();

	void stop();

	void addProcessorListener(ProcessorListener l);

	void removeProcessorListener(ProcessorListener l);

	Seq<RegisterValue> getRegisters();
}
