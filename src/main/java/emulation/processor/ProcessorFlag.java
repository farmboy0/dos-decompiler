package emulation.processor;

public enum ProcessorFlag {
	CARRY(0x1, "CF"), //
	PARITY(0x4, "PF"), //
	ADJUST(0x10, "AF"), //
	ZERO(0x40, "ZF"), //
	SIGN(0x80, "SF"), //
	TRAP(0x100, "TF"), //
	INTERRUPT(0x200, "IF"), //
	DIRECTION(0x400, "DF"), //
	OVERFLOW(0x800, "OF"), //
	;

	private final int bitmask;
	private final String shortName;

	private ProcessorFlag(int bitmask, String shortName) {
		this.bitmask = bitmask;
		this.shortName = shortName;
	}

	public int getBitmask() {
		return bitmask;
	}

	public String getShortName() {
		return shortName;
	}

	/**
	 * Shifting this by the number we want to test gives 1 if number of bit is even
	 * and 0 if odd.<br/>
	 * Hardcoded numbers:<br/>
	 * 0 -> 0000: even -> 1<br/>
	 * 1 -> 0001: 1 bit so odd -> 0<br/>
	 * 2 -> 0010: 1 bit so odd -> 0<br/>
	 * 3 -> 0011: 2 bit so even -> 1<br/>
	 * 4 -> 0100: 1 bit so odd -> 0<br/>
	 * 5 -> 0101: even -> 1<br/>
	 * 6 -> 0110: even -> 1<br/>
	 * 7 -> 0111: odd -> 0<br/>
	 * 8 -> 1000: odd -> 0<br/>
	 * 9 -> 1001: even -> 1<br/>
	 * A -> 1010: even -> 1<br/>
	 * B -> 1011: odd -> 0<br/>
	 * C -> 1100: even -> 1<br/>
	 * D -> 1101: odd -> 0<br/>
	 * E -> 1110: odd -> 0<br/>
	 * F -> 1111: even -> 1<br/>
	 * => lookup table is 1001011001101001
	 */
	private static final int PARITY_RESULTS = 0b1001011001101001;

	public static boolean isParity(int value) {
		return ((PARITY_RESULTS >> ((value >> 4) & 0xF)) & 1) == ((PARITY_RESULTS >> (value & 0xF)) & 1);
	}

	public static int carryAdjustBitsAdd(int dstValue, int srcValue, int result) {
		return (((dstValue ^ srcValue) ^ result) ^ ((dstValue ^ result) & (~(dstValue ^ srcValue))));
	}

	public static int carryAdjustBitsSub(int dstValue, int srcValue, int result) {
		return (((dstValue ^ srcValue) ^ result) ^ ((dstValue ^ result) & (dstValue ^ srcValue)));
	}

	public static boolean isCarry8(int carrybits) {
		return ((carrybits >> 7) & 1) == 1;
	}

	public static boolean isCarry16(int carrybits) {
		return ((carrybits >> 15) & 1) == 1;
	}

	public static boolean isAdjust(int carrybits) {
		return ((carrybits >> 3) & 1) == 1;
	}

	public static boolean isOverflowAdd8(int dstValue, int srcValue, int result) {
		final int overflowBits = ((dstValue ^ result) & (~(dstValue ^ srcValue)));
		return ((overflowBits >> 7) & 1) == 1;
	}

	public static boolean isOverflowAdd16(int dstValue, int srcValue, int result) {
		final int overflowBits = ((dstValue ^ result) & (~(dstValue ^ srcValue)));
		return ((overflowBits >> 15) & 1) == 1;
	}

	public static boolean isOverflowSub8(int dstValue, int srcValue, int result) {
		final int overflowBits = ((dstValue ^ result) & (dstValue ^ srcValue));
		return ((overflowBits >> 7) & 1) == 1;
	}

	public static boolean isOverflowSub16(int dstValue, int srcValue, int result) {
		final int overflowBits = ((dstValue ^ result) & (dstValue ^ srcValue));
		return ((overflowBits >> 15) & 1) == 1;
	}
}
