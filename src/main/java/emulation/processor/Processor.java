package emulation.processor;

import assembly.Address16bit;
import assembly.Argument;
import assembly.Register;

public interface Processor {
	Alu getAlu();

	int read(Register arg);

	int read(Argument arg, Register segmentOveride);

	boolean read(ProcessorFlag flag);

	Address16bit readAddr(Argument arg);

	int readEip();

	int readFlags();

	int readStack();

	int read8FromMem(Register segment, Register offset);

	int read16FromMem(Register segment, Register offset);

	int read8FromPort(int port);

	int read16FromPort(int port);

	void write(Register arg, int value);

	void write(Argument arg, Register segmentOveride, int value);

	void write(ProcessorFlag flag, boolean set);

	void writeCsEip(int cs, int eip);

	void writeCsEip(int relativeOffset);

	void writeEip(int eip);

	void writeFlags(int flags);

	void writeStack(int value);

	void write8ToMem(Register segment, Register offset, int value);

	void write16ToMem(Register segment, Register offset, int value);

	void write8ToPort(int port, int value);

	void write16ToPort(int port, int value);
}
