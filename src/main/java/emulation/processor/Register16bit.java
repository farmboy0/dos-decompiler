package emulation.processor;

public interface Register16bit extends RegisterValue {
	Register8bit getHigh8();

	Register8bit getLow8();
}
