package emulation.processor;

public interface Register32bit extends RegisterValue {
	Register16bit getLow16();
}
