package emulation.processor;

public interface Alu {

	int add8(int value1, int value2);

	int add16(int value1, int value2);

	int adc8(int value1, int value2);

	int adc16(int value1, int value2);

	int inc8(int value);

	int inc16(int value);

	int or8(int value1, int value2);

	int or16(int value1, int value2);

	int sub8(int value1, int value2);

	int sub16(int value1, int value2);

	int sbb8(int value1, int value2);

	int sbb16(int value1, int value2);

	int dec8(int value1);

	int dec16(int value1);

	int and8(int value1, int value2);

	int and16(int value1, int value2);

	int xor8(int value1, int value2);

	int xor16(int value1, int value2);

	int mul8(int value1, int value2);

	int mul16(int value1, int value2);

	int imul8(int value1, int value2);

	int imul16(int value1, int value2);

	Integer div8(int value1, int value2);

	Integer div16(int value1, int value2);

	Integer idiv8(int value1, int value2);

	Integer idiv16(int value1, int value2);

	int shl8(int value, int count);

	int shl16(int value, int count);

	int shr8(int value, int count);

	int shr16(int value, int count);

	int sar8(int value, int count);

	int sar16(int value, int count);

	int rcl8(int value, int count);

	int rcl16(int value, int count);

	int rcr8(int value, int count);

	int rcr16(int value, int count);

	int rol8(int value, int count);

	int rol16(int value, int count);

	int ror8(int value, int count);

	int ror16(int value, int count);

}