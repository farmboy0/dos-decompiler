package emulation.processor;

public interface RegisterValueListener {
	void valueChanged(int oldValue, int newValue);
}
