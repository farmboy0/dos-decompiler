package emulation.processor;

import assembly.Instruction;

public interface ProcessorListener {

	void found(Instruction inst);

	void executed(Instruction inst);
}
