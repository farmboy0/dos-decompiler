package emulation.processor.impl;

import io.vavr.API;
import io.vavr.collection.Set;

import emulation.processor.Register16bit;
import emulation.processor.Register8bit;
import emulation.processor.RegisterValueListener;

/**
 * Implementation of a general 16-bit register. The value saved here is treated
 * as a signed value.
 * 
 * @author eho
 */
class RegisterImpl implements Register16bit {
	private final High8 high8 = new High8();
	private final Low8 low8 = new Low8();

	private final String name;
	private int regValue;

	private Set<RegisterValueListener> listeners = API.Set();

	public RegisterImpl(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int get() {
		return regValue;
	}

	@Override
	public void set(int value) {
		int oldValue = get();
		regValue = value;
		listeners.forEach(l -> l.valueChanged(oldValue, get()));
	}

	@Override
	public Register8bit getHigh8() {
		return high8;
	}

	@Override
	public Register8bit getLow8() {
		return low8;
	}

	@Override
	public void addListener(RegisterValueListener listener) {
		listeners = listeners.add(listener);
	}

	@Override
	public void removeListener(RegisterValueListener listener) {
		listeners = listeners.remove(listener);
	}

	private class High8 implements Register8bit {
		private Set<RegisterValueListener> listeners = API.Set();

		@Override
		public String getName() {
			return name.replace('X', 'H');
		}

		@Override
		public int get() {
			return (regValue & 0xFF00) >> 8;
		}

		@Override
		public void set(int value) {
			int oldValue = get();
			regValue = (((value & 0xFF) << 8) | (regValue & 0xFF)) & 0xFFFF;
			listeners.forEach(l -> l.valueChanged(oldValue, get()));
		}

		@Override
		public void addListener(RegisterValueListener listener) {
			listeners = listeners.add(listener);
		}

		@Override
		public void removeListener(RegisterValueListener listener) {
			listeners = listeners.remove(listener);
		}
	}

	private class Low8 implements Register8bit {
		private Set<RegisterValueListener> listeners = API.Set();

		@Override
		public String getName() {
			return name.replace('X', 'L');
		}

		@Override
		public int get() {
			return regValue;
		}

		@Override
		public void set(int value) {
			int oldValue = get();
			regValue = (regValue & 0xFFFFFF00) | (value & 0xFF);
			listeners.forEach(l -> l.valueChanged(oldValue, get()));
		}

		@Override
		public void addListener(RegisterValueListener listener) {
			listeners = listeners.add(listener);
		}

		@Override
		public void removeListener(RegisterValueListener listener) {
			listeners = listeners.remove(listener);
		}
	}
}
