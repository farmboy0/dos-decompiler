package emulation.processor.impl;

import java.util.function.ToIntBiFunction;

import io.vavr.API;
import io.vavr.NotImplementedError;

import assembly.Instruction;
import assembly.Mnemonics;
import assembly.Opcode;
import assembly.Register;
import emulation.processor.Processor;
import emulation.processor.ProcessorFlag;

public enum StringOp {
	INSB6C(Mnemonics.INSB, false, StringOp::read8PortDX, StringOp::write8ESDI), //
	INSW6D(Mnemonics.INSW, false, StringOp::read16PortDX, StringOp::write16ESDI), //
	OUTSB6E(Mnemonics.OUTSB, false, StringOp::read8DSSI, StringOp::write8PortDX), //
	OUTSW6F(Mnemonics.OUTSW, false, StringOp::read16DSSI, StringOp::write16PortDX), //

	MOVSBA4(Mnemonics.MOVSB, false, StringOp::read8DSSI, StringOp::write8ESDI), //
	MOVSWA5(Mnemonics.MOVSW, false, StringOp::read16DSSI, StringOp::write16ESDI), //

	STOSBAA(Mnemonics.STOSB, false, (cpu, inst) -> cpu.read(Register.AL), StringOp::write8ESDI), //
	STOSWAB(Mnemonics.STOSW, false, (cpu, inst) -> cpu.read(Register.AX), StringOp::write8ESDI), //
	LODSBAC(Mnemonics.LODSB, false, StringOp::read8DSSI, (cpu, inst, value) -> cpu.write(Register.AL, value)), //
	LODSWAD(Mnemonics.LODSW, false, StringOp::read16DSSI, (cpu, inst, value) -> cpu.write(Register.AX, value)), //

	CMPSBA6(Mnemonics.CMPSB, true, StringOp::read8DSSI, StringOp::sub8ESDI), //
	CMPSWA7(Mnemonics.CMPSW, true, StringOp::read16DSSI, StringOp::sub16ESDI), //
	SCASBAE(Mnemonics.SCASB, true, (cpu, inst) -> cpu.read(Register.AL), StringOp::sub8ESDI), //
	SCASWAF(Mnemonics.SCASW, true, (cpu, inst) -> cpu.read(Register.AX), StringOp::sub16ESDI), //
	;

	private final Mnemonics mnemonics;
	private final boolean isCompare;
	private final ToIntBiFunction<Processor, Instruction> valueReader;
	private final BiObjIntConsumer<Processor, Instruction> valueWriter;

	private StringOp(Mnemonics mnemonics, boolean isCompare, ToIntBiFunction<Processor, Instruction> valueReader,
		BiObjIntConsumer<Processor, Instruction> valueWriter) {

		this.mnemonics = mnemonics;
		this.isCompare = isCompare;
		this.valueReader = valueReader;
		this.valueWriter = valueWriter;
	}

	public static void exec(Processor cpu, Instruction inst) {
		final Opcode repetition = inst.getStringRepetition();
		final StringOp stringOp = byMnemonics(inst.getMnemonic());
		if (repetition != null) {
			final boolean zeroFlagToContinue = Opcode.REPZF3.equals(repetition);
			int cx = cpu.read(Register.CX);
			while (cx != 0) {
				exec(cpu, inst, stringOp);
				cx -= 1;
				if (stringOp.isCompare && zeroFlagToContinue != cpu.read(ProcessorFlag.ZERO)) {
					break;
				}
			}
			cpu.write(Register.CX, cx);
		} else {
			exec(cpu, inst, stringOp);
		}
	}

	private static void exec(Processor cpu, Instruction inst, StringOp stringOp) {
		final int value = stringOp.valueReader.applyAsInt(cpu, inst);
		stringOp.valueWriter.accept(cpu, inst, value);
	}

	private static StringOp byMnemonics(Mnemonics mnemonics) {
		return API.Seq(values())
			.find(i -> i.mnemonics == mnemonics)
			.getOrElseThrow(() -> new NotImplementedError(mnemonics.name()));
	}

	private static int read8DSSI(Processor cpu, Instruction inst) {
		final Register segment = inst.getSegmentOverride();
		final int value = cpu.read8FromMem(segment != null ? segment : Register.DS, Register.SI);
		cpu.write(Register.SI, cpu.read(Register.SI) + (cpu.read(ProcessorFlag.DIRECTION) ? -1 : 1));
		return value;
	}

	private static int read16DSSI(Processor cpu, Instruction inst) {
		final Register segment = inst.getSegmentOverride();
		final int value = cpu.read16FromMem(segment != null ? segment : Register.DS, Register.SI);
		cpu.write(Register.SI, cpu.read(Register.SI) + (cpu.read(ProcessorFlag.DIRECTION) ? -2 : 2));
		return value;
	}

	private static int read8PortDX(Processor cpu, Instruction inst) {
		final int port = cpu.read(Register.DX);
		return cpu.read8FromPort(port);
	}

	private static int read16PortDX(Processor cpu, Instruction inst) {
		final int port = cpu.read(Register.DX);
		return cpu.read16FromPort(port);
	}

	private static void sub8ESDI(Processor cpu, Instruction inst, int value1) {
		final Register segment = inst.getSegmentOverride();
		final int value2 = cpu.read8FromMem(segment != null ? segment : Register.ES, Register.DI);
		cpu.getAlu().sub8(value1, value2);
		cpu.write(Register.DI, cpu.read(Register.DI) + (cpu.read(ProcessorFlag.DIRECTION) ? -1 : 1));
	}

	private static void sub16ESDI(Processor cpu, Instruction inst, int value1) {
		final Register segment = inst.getSegmentOverride();
		final int value2 = cpu.read16FromMem(segment != null ? segment : Register.ES, Register.DI);
		cpu.getAlu().sub16(value1, value2);
		cpu.write(Register.DI, cpu.read(Register.DI) + (cpu.read(ProcessorFlag.DIRECTION) ? -2 : 2));
	}

	private static void write8ESDI(Processor cpu, Instruction inst, int value) {
		final Register segment = inst.getSegmentOverride();
		cpu.write8ToMem(segment != null ? segment : Register.ES, Register.DI, value);
		cpu.write(Register.DI, cpu.read(Register.DI) + (cpu.read(ProcessorFlag.DIRECTION) ? -1 : 1));
	}

	private static void write16ESDI(Processor cpu, Instruction inst, int value) {
		final Register segment = inst.getSegmentOverride();
		cpu.write16ToMem(segment != null ? segment : Register.ES, Register.DI, value);
		cpu.write(Register.DI, cpu.read(Register.DI) + (cpu.read(ProcessorFlag.DIRECTION) ? -2 : 2));
	}

	private static void write8PortDX(Processor cpu, Instruction inst, int value) {
		final int port = cpu.read(Register.DX);
		cpu.write8ToPort(port, value);
	}

	private static void write16PortDX(Processor cpu, Instruction inst, int value) {
		final int port = cpu.read(Register.DX);
		cpu.write16ToPort(port, value);
	}

	@FunctionalInterface
	public interface BiObjIntConsumer<T, U> {
		void accept(T t, U u, int value);
	}
}
