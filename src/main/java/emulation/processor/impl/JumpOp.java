package emulation.processor.impl;

import static emulation.processor.ProcessorFlag.CARRY;
import static emulation.processor.ProcessorFlag.OVERFLOW;
import static emulation.processor.ProcessorFlag.PARITY;
import static emulation.processor.ProcessorFlag.SIGN;
import static emulation.processor.ProcessorFlag.ZERO;

import io.vavr.API;
import io.vavr.NotImplementedError;

import assembly.Address16bit;
import assembly.Argument;
import assembly.Displacement;
import assembly.Instruction;
import assembly.Mnemonics;
import assembly.Register;
import emulation.processor.Processor;

public enum JumpOp {
	JO(Mnemonics.JO, cpu -> cpu.read(OVERFLOW)), //
	JNO(Mnemonics.JNO, cpu -> !cpu.read(OVERFLOW)), //

	JB(Mnemonics.JB, cpu -> cpu.read(CARRY)), //
	JNB(Mnemonics.JNB, cpu -> !cpu.read(CARRY)), //

	JZ(Mnemonics.JZ, cpu -> cpu.read(ZERO)), //
	JNZ(Mnemonics.JNZ, cpu -> !cpu.read(ZERO)), //

	JBE(Mnemonics.JBE, cpu -> cpu.read(CARRY) || cpu.read(ZERO)), //
	JA(Mnemonics.JA, cpu -> !cpu.read(CARRY) && !cpu.read(ZERO)), //

	JS(Mnemonics.JS, cpu -> cpu.read(SIGN)), //
	JNS(Mnemonics.JNS, cpu -> !cpu.read(SIGN)), //

	JPE(Mnemonics.JPE, cpu -> cpu.read(PARITY)), //
	JPO(Mnemonics.JPO, cpu -> !cpu.read(PARITY)), //

	JL(Mnemonics.JL, cpu -> cpu.read(SIGN) != cpu.read(OVERFLOW)), //
	JGE(Mnemonics.JGE, cpu -> cpu.read(SIGN) == cpu.read(OVERFLOW)), //

	JLE(Mnemonics.JLE, cpu -> cpu.read(ZERO) || cpu.read(SIGN) != cpu.read(OVERFLOW)), //
	JG(Mnemonics.JG, cpu -> cpu.read(ZERO) && cpu.read(SIGN) == cpu.read(OVERFLOW)), //

	LOOPNZE0(Mnemonics.LOOPNZ, cpu -> cpu.read(Register.CX) != 0), //
	LOOPZE1(Mnemonics.LOOPZ, cpu -> cpu.read(Register.CX) == 0), //
	LOOPE2(Mnemonics.LOOP, cpu -> true), //
	JCXZE3(Mnemonics.JCXZ, cpu -> cpu.read(Register.CX) == 0), //
	;

	private final Mnemonics mnemonics;
	private final JumpCondition<Processor> condition;

	private JumpOp(Mnemonics mnemonics, JumpCondition<Processor> condition) {
		this.mnemonics = mnemonics;
		this.condition = condition;
	}

	public static void exec(Processor cpu, Instruction inst) {
		if (byMnemonics(inst.getMnemonic()).condition.applyAsBoolean(cpu)) {
			jmp(cpu, inst);
		}
	}

	private static JumpOp byMnemonics(Mnemonics mnemonics) {
		return API.Seq(values())
			.find(i -> i.mnemonics == mnemonics)
			.getOrElseThrow(() -> new NotImplementedError(mnemonics.name()));
	}

	public static void call(Processor cpu, Instruction inst) {
		final Argument dst = inst.getArgument(0);
		if (dst instanceof Address16bit csEip) {
			cpu.writeStack(cpu.read(Register.CS));
			cpu.writeStack(cpu.readEip());
			cpu.writeCsEip(csEip.getSegment(), csEip.getOffset());
		} else if (dst instanceof Displacement disp) {
			cpu.writeStack(cpu.readEip());
			cpu.writeCsEip(disp.getValue());
		}
	}

	public static void iret(Processor cpu, Instruction inst) {
		cpu.writeEip(cpu.readStack());
		cpu.write(Register.CS, cpu.readStack());
		cpu.writeFlags(cpu.readStack());
	}

	public static void jmp(Processor cpu, Instruction inst) {
		final Argument dst = inst.getArgument(0);
		if (dst instanceof Address16bit csEip) {
			cpu.writeCsEip(csEip.getSegment(), csEip.getOffset());
		} else if (dst instanceof Displacement disp) {
			cpu.writeCsEip(disp.getValue());
		}
	}

	public static void ret0(Processor cpu, Instruction inst) {
		cpu.writeEip(cpu.readStack());
	}

	public static void ret1(Processor cpu, Instruction inst) {
		final int count = cpu.read(inst.getArgument(0), inst.getSegmentOverride());
		cpu.writeEip(cpu.readStack());
		cpu.write(Register.SP, cpu.read(Register.SP) + count);
	}

	public static void retf0(Processor cpu, Instruction inst) {
		cpu.writeEip(cpu.readStack());
		cpu.write(Register.CS, cpu.readStack());
	}

	public static void retf1(Processor cpu, Instruction inst) {
		final int count = cpu.read(inst.getArgument(0), inst.getSegmentOverride());
		cpu.writeEip(cpu.readStack());
		cpu.write(Register.CS, cpu.readStack());
		cpu.write(Register.SP, cpu.read(Register.SP) + count);
	}

	@FunctionalInterface
	private interface JumpCondition<T> {
		boolean applyAsBoolean(T value);
	}
}
