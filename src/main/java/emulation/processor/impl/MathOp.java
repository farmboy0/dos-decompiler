package emulation.processor.impl;

import io.vavr.API;
import io.vavr.NotImplementedError;

import assembly.Argument;
import assembly.Instruction;
import assembly.Opcode;
import emulation.processor.Alu;
import emulation.processor.Processor;
import emulation.processor.ProcessorFlag;

enum MathOp {
	ADD00(Opcode.ADD00, Alu::add8, true), //
	ADD01(Opcode.ADD01, Alu::add16, true), //
	ADD02(Opcode.ADD02, Alu::add8, true), //
	ADD03(Opcode.ADD03, Alu::add16, true), //
	ADD04(Opcode.ADD04, Alu::add8, true), //
	ADD05(Opcode.ADD05, Alu::add16, true), //

	OR08(Opcode.OR08, Alu::or8, true), //
	OR09(Opcode.OR09, Alu::or16, true), //
	OR0A(Opcode.OR0A, Alu::or8, true), //
	OR0B(Opcode.OR0B, Alu::or16, true), //
	OR0C(Opcode.OR0C, Alu::or8, true), //
	OR0D(Opcode.OR0D, Alu::or16, true), //

	ADC10(Opcode.ADC10, Alu::adc8, true), //
	ADC11(Opcode.ADC11, Alu::adc16, true), //
	ADC12(Opcode.ADC12, Alu::adc8, true), //
	ADC13(Opcode.ADC13, Alu::adc16, true), //
	ADC14(Opcode.ADC14, Alu::adc8, true), //
	ADC15(Opcode.ADC15, Alu::adc16, true), //

	SBB18(Opcode.SBB18, Alu::sbb8, true), //
	SBB19(Opcode.SBB19, Alu::sbb16, true), //
	SBB1A(Opcode.SBB1A, Alu::sbb8, true), //
	SBB1B(Opcode.SBB1B, Alu::sbb16, true), //
	SBB1C(Opcode.SBB1C, Alu::sbb8, true), //
	SBB1D(Opcode.SBB1D, Alu::sbb16, true), //

	AND20(Opcode.AND20, Alu::and8, true), //
	AND21(Opcode.AND21, Alu::and16, true), //
	AND22(Opcode.AND22, Alu::and8, true), //
	AND23(Opcode.AND23, Alu::and16, true), //
	AND24(Opcode.AND24, Alu::and8, true), //
	AND25(Opcode.AND25, Alu::and16, true), //

	SUB28(Opcode.SUB28, Alu::sub8, true), //
	SUB29(Opcode.SUB29, Alu::sub16, true), //
	SUB2A(Opcode.SUB2A, Alu::sub8, true), //
	SUB2B(Opcode.SUB2B, Alu::sub16, true), //
	SUB2C(Opcode.SUB2C, Alu::sub8, true), //
	SUB2D(Opcode.SUB2D, Alu::sub16, true), //

	XOR30(Opcode.XOR30, Alu::xor8, true), //
	XOR31(Opcode.XOR31, Alu::xor16, true), //
	XOR32(Opcode.XOR32, Alu::xor8, true), //
	XOR33(Opcode.XOR33, Alu::xor16, true), //
	XOR34(Opcode.XOR34, Alu::xor8, true), //
	XOR35(Opcode.XOR35, Alu::xor16, true), //

	CMP38(Opcode.CMP38, Alu::sub8, false), //
	CMP39(Opcode.CMP39, Alu::sub16, false), //
	CMP3A(Opcode.CMP3A, Alu::sub8, false), //
	CMP3B(Opcode.CMP3B, Alu::sub16, false), //
	CMP3C(Opcode.CMP3C, Alu::sub8, false), //
	CMP3D(Opcode.CMP3D, Alu::sub16, false), //

	IMUL69(Opcode.IMUL69, Alu::imul16, true), //
	IMUL6B(Opcode.IMUL6B, Alu::imul16, true), //

	ADD80(Opcode.ADD80, Alu::add8, true), //
	OR80(Opcode.OR80, Alu::or8, true), //
	ADC80(Opcode.ADC80, Alu::adc8, true), //
	SBB80(Opcode.SBB80, Alu::sbb8, true), //
	AND80(Opcode.AND80, Alu::and8, true), //
	SUB80(Opcode.SUB80, Alu::sub8, true), //
	XOR80(Opcode.XOR80, Alu::xor8, true), //
	CMP80(Opcode.CMP80, Alu::sub8, false), //

	ADD81(Opcode.ADD81, Alu::add16, true), //
	OR81(Opcode.OR81, Alu::or16, true), //
	ADC81(Opcode.ADC81, Alu::adc16, true), //
	SBB81(Opcode.SBB81, Alu::sbb16, true), //
	AND81(Opcode.AND81, Alu::and16, true), //
	SUB81(Opcode.SUB81, Alu::sub16, true), //
	XOR81(Opcode.XOR81, Alu::xor16, true), //
	CMP81(Opcode.CMP81, Alu::sub16, false), //

	ADD82(Opcode.ADD82, Alu::add8, true), //
	OR82(Opcode.OR82, Alu::or8, true), //
	ADC82(Opcode.ADC82, Alu::adc8, true), //
	SBB82(Opcode.SBB82, Alu::sbb8, true), //
	AND82(Opcode.AND82, Alu::and8, true), //
	SUB82(Opcode.SUB82, Alu::sub8, true), //
	XOR82(Opcode.XOR82, Alu::xor8, true), //
	CMP82(Opcode.CMP82, Alu::sub8, false), //

	ADD83(Opcode.ADD83, Alu::add16, true), //
	OR83(Opcode.OR83, Alu::or16, true), //
	ADC83(Opcode.ADC83, Alu::adc16, true), //
	SBB83(Opcode.SBB83, Alu::sbb16, true), //
	AND83(Opcode.AND83, Alu::and16, true), //
	SUB83(Opcode.SUB83, Alu::sub16, true), //
	XOR83(Opcode.XOR83, Alu::xor16, true), //
	CMP83(Opcode.CMP83, Alu::sub16, false), //

	TEST84(Opcode.TEST84, Alu::and8, false), //
	TEST85(Opcode.TEST85, Alu::and16, false), //

	TESTA8(Opcode.TESTA8, Alu::and8, false), //
	TESTA9(Opcode.TESTA9, Alu::and16, false), //

	ROLC0(Opcode.ROLC0, Alu::rol8, true), //
	RORC0(Opcode.RORC0, Alu::ror8, true), //
	RCLC0(Opcode.RCLC0, Alu::rcl8, true), //
	RCRC0(Opcode.RCRC0, Alu::rcr8, true), //
	SHLC0(Opcode.SHLC0, Alu::shl8, true), //
	SHRC0(Opcode.SHRC0, Alu::shr8, true), //
	SARC0(Opcode.SARC0, Alu::sar8, true), //

	ROLC1(Opcode.ROLC1, Alu::rol16, true), //
	RORC1(Opcode.RORC1, Alu::ror16, true), //
	RCLC1(Opcode.RCLC1, Alu::rcl16, true), //
	RCRC1(Opcode.RCRC1, Alu::rcr16, true), //
	SHLC1(Opcode.SHLC1, Alu::shl16, true), //
	SHRC1(Opcode.SHRC1, Alu::shr16, true), //
	SARC1(Opcode.SARC1, Alu::sar16, true), //

	ROLD0(Opcode.ROLD0, Alu::rol8, true), //
	RORD0(Opcode.RORD0, Alu::ror8, true), //
	RCLD0(Opcode.RCLD0, Alu::rcl8, true), //
	RCRD0(Opcode.RCRD0, Alu::rcr8, true), //
	SHLD0(Opcode.SHLD0, Alu::shl8, true), //
	SHRD0(Opcode.SHRD0, Alu::shr8, true), //
	SARD0(Opcode.SARD0, Alu::sar8, true), //

	ROLD1(Opcode.ROLD1, Alu::rol16, true), //
	RORD1(Opcode.RORD1, Alu::ror16, true), //
	RCLD1(Opcode.RCLD1, Alu::rcl16, true), //
	RCRD1(Opcode.RCRD1, Alu::rcr16, true), //
	SHLD1(Opcode.SHLD1, Alu::shl16, true), //
	SHRD1(Opcode.SHRD1, Alu::shr16, true), //
	SARD1(Opcode.SARD1, Alu::sar16, true), //

	ROLD2(Opcode.ROLD2, Alu::rol8, true), //
	RORD2(Opcode.RORD2, Alu::ror8, true), //
	RCLD2(Opcode.RCLD2, Alu::rcl8, true), //
	RCRD2(Opcode.RCRD2, Alu::rcr8, true), //
	SHLD2(Opcode.SHLD2, Alu::shl8, true), //
	SHRD2(Opcode.SHRD2, Alu::shr8, true), //
	SARD2(Opcode.SARD2, Alu::sar8, true), //

	ROLD3(Opcode.ROLD3, Alu::rol16, true), //
	RORD3(Opcode.RORD3, Alu::ror16, true), //
	RCLD3(Opcode.RCLD3, Alu::rcl16, true), //
	RCRD3(Opcode.RCRD3, Alu::rcr16, true), //
	SHLD3(Opcode.SHLD3, Alu::shl16, true), //
	SHRD3(Opcode.SHRD3, Alu::shr16, true), //
	SARD3(Opcode.SARD3, Alu::sar16, true), //

	TESTF6_GRP0(Opcode.TESTF6_GRP0, Alu::and8, false), //
	TESTF6_GRP1(Opcode.TESTF6_GRP1, Alu::and8, false), //
	MULF6(Opcode.MULF6, Alu::mul8, true), //
	IMULF6(Opcode.IMULF6, Alu::imul8, true), //
	DIVF6(Opcode.DIVF6, Alu::div8, true), //
	IDIVF6(Opcode.IDIVF6, Alu::idiv8, true), //

	TESTF7_GRP0(Opcode.TESTF7_GRP0, Alu::and16, false), //
	TESTF7_GRP1(Opcode.TESTF7_GRP1, Alu::and16, false), //
	MULF7(Opcode.MULF7, Alu::mul16, true), //
	IMULF7(Opcode.IMULF7, Alu::imul16, true), //
	DIVF7(Opcode.DIVF7, Alu::div16, true), //
	IDIVF7(Opcode.IDIVF7, Alu::idiv16, true), //
	;

	private final Opcode opcode;
	private final AluOperation operation;
	private final boolean writeResult;

	private MathOp(Opcode opcode, AluOperation operation, boolean writeResult) {
		this.opcode = opcode;
		this.operation = operation;
		this.writeResult = writeResult;
	}

	public static void exec(Processor cpu, Instruction inst) {
		final MathOp op = byOpcode(inst.getOpcode());
		final Argument dst = inst.getArgument(0);
		final int dstValue = cpu.read(dst, inst.getSegmentOverride());
		final int srcValue = cpu.read(inst.getArgument(1), inst.getSegmentOverride());
		final int result = op.operation.compute(cpu.getAlu(), dstValue, srcValue);
		if (op.writeResult) {
			cpu.write(dst, inst.getSegmentOverride(), result);
		}
	}

	private static MathOp byOpcode(Opcode opcode) {
		return API.Seq(values())
			.find(i -> i.opcode == opcode)
			.getOrElseThrow(() -> new NotImplementedError(opcode.name()));
	}

	public static void dec8(Processor cpu, Instruction inst) {
		final Argument dst = inst.getArgument(0);
		final int dstValue = cpu.read(dst, inst.getSegmentOverride());
		final int result = cpu.getAlu().dec8(dstValue);
		cpu.write(dst, inst.getSegmentOverride(), result);
	}

	public static void dec16(Processor cpu, Instruction inst) {
		final Argument dst = inst.getArgument(0);
		final int dstValue = cpu.read(dst, inst.getSegmentOverride());
		final int result = cpu.getAlu().dec16(dstValue);
		cpu.write(dst, inst.getSegmentOverride(), result);
	}

	public static void inc8(Processor cpu, Instruction inst) {
		final Argument dst = inst.getArgument(0);
		final int dstValue = cpu.read(dst, inst.getSegmentOverride());
		final int result = cpu.getAlu().inc8(dstValue);
		cpu.write(dst, inst.getSegmentOverride(), result);
	}

	public static void inc16(Processor cpu, Instruction inst) {
		final Argument dst = inst.getArgument(0);
		final int dstValue = cpu.read(dst, inst.getSegmentOverride());
		final int result = cpu.getAlu().inc16(dstValue);
		cpu.write(dst, inst.getSegmentOverride(), result);
	}

	public static void neg8(Processor cpu, Instruction inst) {
		final Argument dst = inst.getArgument(0);
		final int dstValue = cpu.read(dst, inst.getSegmentOverride());
		final int result = cpu.getAlu().sub8(0, dstValue);
		cpu.write(dst, inst.getSegmentOverride(), result);
		cpu.write(ProcessorFlag.CARRY, result != 0);
	}

	public static void neg16(Processor cpu, Instruction inst) {
		final Argument dst = inst.getArgument(0);
		final int dstValue = cpu.read(dst, inst.getSegmentOverride());
		final int result = cpu.getAlu().sub16(0, dstValue);
		cpu.write(dst, inst.getSegmentOverride(), result);
		cpu.write(ProcessorFlag.CARRY, result != 0);
	}

	public static void not8(Processor cpu, Instruction inst) {
		final Argument dst = inst.getArgument(0);
		final int dstValue = cpu.read(dst, inst.getSegmentOverride());
		final int result = ~dstValue;
		cpu.write(dst, inst.getSegmentOverride(), result);
	}

	public static void not16(Processor cpu, Instruction inst) {
		final Argument dst = inst.getArgument(0);
		final int dstValue = cpu.read(dst, inst.getSegmentOverride());
		final int result = ~dstValue;
		cpu.write(dst, inst.getSegmentOverride(), result);
	}

	private interface AluOperation {
		int compute(Alu alu, int t, int u);
	}
}
