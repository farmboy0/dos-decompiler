package emulation.processor.impl;

import emulation.processor.Alu;
import emulation.processor.Processor;
import emulation.processor.ProcessorFlag;

/**
 * Implementation for the arithmetic operations performed by the CPU. originally
 * from https://github.com/kevinferrare/spice86
 */
public class AluImpl implements Alu {
	/**
	 * Shifting this by the number we want to test gives 1 if number of bit is even
	 * and 0 if odd.<br/>
	 * Hardcoded numbers:<br/>
	 * 0 -> 0000: even -> 1<br/>
	 * 1 -> 0001: 1 bit so odd -> 0<br/>
	 * 2 -> 0010: 1 bit so odd -> 0<br/>
	 * 3 -> 0011: 2 bit so even -> 1<br/>
	 * 4 -> 0100: 1 bit so odd -> 0<br/>
	 * 5 -> 0101: even -> 1<br/>
	 * 6 -> 0110: even -> 1<br/>
	 * 7 -> 0111: odd -> 0<br/>
	 * 8 -> 1000: odd -> 0<br/>
	 * 9 -> 1001: even -> 1<br/>
	 * A -> 1010: even -> 1<br/>
	 * B -> 1011: odd -> 0<br/>
	 * C -> 1100: even -> 1<br/>
	 * D -> 1101: odd -> 0<br/>
	 * E -> 1110: odd -> 0<br/>
	 * F -> 1111: even -> 1<br/>
	 * => lookup table is 1001011001101001
	 */
	private static final int FOUR_BIT_PARITY_EVEN_TABLE = 0b1001011001101001;
	private static final int SHIFT_COUNT_MASK = 0x1F;
	private static final int MSB_MASK_8 = 0x80;
	private static final int BEFORE_MSB_MASK_8 = 0x40;
	private static final int MSB_MASK_16 = 0x8000;
	private static final int BEFORE_MSB_MASK_16 = 0x4000;

	private final Processor cpu;

	public AluImpl(Processor cpu) {
		this.cpu = cpu;
	}

	private int add8(int value1, int value2, boolean useCarry) {
		final int carry = (useCarry && cpu.read(ProcessorFlag.CARRY)) ? 1 : 0;
		final int res = uint8(value1 + value2 + carry);
		updateFlags8(res);
		final int carryBits = carryBitsAdd(value1, value2, res);
		final int overflowBits = overflowBitsAdd(value1, value2, res);
		cpu.write(ProcessorFlag.CARRY, ((carryBits >> 7) & 1) == 1);
		cpu.write(ProcessorFlag.ADJUST, ((carryBits >> 3) & 1) == 1);
		cpu.write(ProcessorFlag.OVERFLOW, ((overflowBits >> 7) & 1) == 1);
		return res;
	}

	private int add16(int value1, int value2, boolean useCarry) {
		int carry = (useCarry && cpu.read(ProcessorFlag.CARRY)) ? 1 : 0;
		int res = uint16(value1 + value2 + carry);
		updateFlags16(res);
		int carryBits = carryBitsAdd(value1, value2, res);
		int overflowBits = overflowBitsAdd(value1, value2, res);
		cpu.write(ProcessorFlag.CARRY, ((carryBits >> 15) & 1) == 1);
		cpu.write(ProcessorFlag.ADJUST, ((carryBits >> 3) & 1) == 1);
		cpu.write(ProcessorFlag.OVERFLOW, ((overflowBits >> 15) & 1) == 1);
		return res;
	}

	@Override
	public int add8(int value1, int value2) {
		return add8(value1, value2, false);
	}

	@Override
	public int add16(int value1, int value2) {
		return add16(value1, value2, false);
	}

	@Override
	public int adc8(int value1, int value2) {
		return add8(value1, value2, true);
	}

	@Override
	public int adc16(int value1, int value2) {
		return add16(value1, value2, true);
	}

	@Override
	public int inc8(int value) {
		// CF is not modified
		boolean carry = cpu.read(ProcessorFlag.CARRY);
		int res = add8(value, 1, false);
		cpu.write(ProcessorFlag.CARRY, carry);
		return res;
	}

	@Override
	public int inc16(int value) {
		// CF is not modified
		boolean carry = cpu.read(ProcessorFlag.CARRY);
		int res = add16(value, 1, false);
		cpu.write(ProcessorFlag.CARRY, carry);
		return res;
	}

	@Override
	public int or8(int value1, int value2) {
		int res = value1 | value2;
		updateFlags8(res);
		cpu.write(ProcessorFlag.CARRY, false);
		cpu.write(ProcessorFlag.OVERFLOW, false);
		return res;
	}

	@Override
	public int or16(int value1, int value2) {
		int res = value1 | value2;
		updateFlags16(res);
		cpu.write(ProcessorFlag.CARRY, false);
		cpu.write(ProcessorFlag.OVERFLOW, false);
		return res;
	}

	private int sub8(int value1, int value2, boolean useCarry) {
		int carry = (useCarry && cpu.read(ProcessorFlag.CARRY)) ? 1 : 0;
		int res = uint8(value1 - value2 - carry);
		updateFlags8(res);
		int borrowBits = borrowBitsSub(value1, value2, res);
		int overflowBits = overflowBitsSub(value1, value2, res);
		cpu.write(ProcessorFlag.CARRY, ((borrowBits >> 7) & 1) == 1);
		cpu.write(ProcessorFlag.ADJUST, ((borrowBits >> 3) & 1) == 1);
		cpu.write(ProcessorFlag.OVERFLOW, ((overflowBits >> 7) & 1) == 1);

		return res;
	}

	private int sub16(int value1, int value2, boolean useCarry) {
		int carry = (useCarry && cpu.read(ProcessorFlag.CARRY)) ? 1 : 0;
		int res = uint16(value1 - value2 - carry);
		updateFlags16(res);
		int borrowBits = borrowBitsSub(value1, value2, res);
		int overflowBits = overflowBitsSub(value1, value2, res);
		cpu.write(ProcessorFlag.CARRY, ((borrowBits >> 15) & 1) == 1);
		cpu.write(ProcessorFlag.ADJUST, ((borrowBits >> 3) & 1) == 1);
		cpu.write(ProcessorFlag.OVERFLOW, ((overflowBits >> 15) & 1) == 1);

		return res;
	}

	@Override
	public int sub8(int value1, int value2) {
		return sub8(value1, value2, false);
	}

	@Override
	public int sub16(int value1, int value2) {
		return sub16(value1, value2, false);
	}

	@Override
	public int sbb8(int value1, int value2) {
		return sub8(value1, value2, true);
	}

	@Override
	public int sbb16(int value1, int value2) {
		return sub16(value1, value2, true);
	}

	@Override
	public int dec8(int value1) {
		// CF is not modified
		boolean carry = cpu.read(ProcessorFlag.CARRY);
		int res = sub8(value1, 1, false);
		cpu.write(ProcessorFlag.CARRY, carry);
		return res;
	}

	@Override
	public int dec16(int value1) {
		// CF is not modified
		boolean carry = cpu.read(ProcessorFlag.CARRY);
		int res = sub16(value1, 1, false);
		cpu.write(ProcessorFlag.CARRY, carry);
		return res;
	}

	@Override
	public int and8(int value1, int value2) {
		int res = value1 & value2;
		updateFlags8(res);
		cpu.write(ProcessorFlag.CARRY, false);
		cpu.write(ProcessorFlag.OVERFLOW, false);
		return res;
	}

	@Override
	public int and16(int value1, int value2) {
		int res = value1 & value2;
		updateFlags16(res);
		cpu.write(ProcessorFlag.CARRY, false);
		cpu.write(ProcessorFlag.OVERFLOW, false);
		return res;
	}

	@Override
	public int xor8(int value1, int value2) {
		int res = value1 ^ value2;
		updateFlags8(res);
		cpu.write(ProcessorFlag.CARRY, false);
		cpu.write(ProcessorFlag.OVERFLOW, false);
		return res;
	}

	@Override
	public int xor16(int value1, int value2) {
		int res = value1 ^ value2;
		updateFlags16(res);
		cpu.write(ProcessorFlag.CARRY, false);
		cpu.write(ProcessorFlag.OVERFLOW, false);
		return res;
	}

	@Override
	public int mul8(int value1, int value2) {
		int res = value1 * value2;
		boolean upperHalfNonZero = (res & 0xFF00) != 0;
		cpu.write(ProcessorFlag.OVERFLOW, upperHalfNonZero);
		cpu.write(ProcessorFlag.CARRY, upperHalfNonZero);
		setZeroFlag(res);
		setParityFlag(res);
		setSignFlag8(res);
		return res;
	}

	@Override
	public int mul16(int value1, int value2) {
		int res = value1 * value2;
		boolean upperHalfNonZero = (res & 0xFFFF0000) != 0;
		cpu.write(ProcessorFlag.OVERFLOW, upperHalfNonZero);
		cpu.write(ProcessorFlag.CARRY, upperHalfNonZero);
		setZeroFlag(res);
		setParityFlag(res);
		setSignFlag16(res);
		return res;
	}

	@Override
	public int imul8(int value1, int value2) {
		int res = int8(value1) * int8(value2);
		boolean doesNotFitInByte = res != int8(res);
		cpu.write(ProcessorFlag.OVERFLOW, doesNotFitInByte);
		cpu.write(ProcessorFlag.CARRY, doesNotFitInByte);
		return res;
	}

	@Override
	public int imul16(int value1, int value2) {
		int res = int16(value1) * int16(value2);
		boolean doesNotFitInWord = res != int16(res);
		cpu.write(ProcessorFlag.OVERFLOW, doesNotFitInWord);
		cpu.write(ProcessorFlag.CARRY, doesNotFitInWord);
		return res;
	}

	@Override
	public Integer div8(int value1, int value2) {
		if (value2 == 0) {
			return null;
		}
		int res = value1 / value2;
		if (res > 0xFF) {
			return null;
		}
		return res;
	}

	@Override
	public Integer div16(int value1, int value2) {
		if (value2 == 0) {
			return null;
		}
		long res = (uint32(value1) / value2);
		if (res > 0xFFFF) {
			return null;
		}
		return (int) res;
	}

	@Override
	public Integer idiv8(int value1, int value2) {
		if (value2 == 0) {
			return null;
		}
		int res = int16(value1) / int8(value2);
		if ((res > 0x7F) || (res < (byte) 0x80)) {
			return null;
		}
		return res;
	}

	@Override
	public Integer idiv16(int value1, int value2) {
		if (value2 == 0) {
			return null;
		}
		int res = value1 / int16(value2);
		if ((res > 0x7FFF) || (res < (short) 0x8000)) {
			return null;
		}
		return res;
	}

	@Override
	public int shl8(int value, int count) {
		count &= SHIFT_COUNT_MASK;
		if (count == 0) {
			return value;
		}
		int msbBefore = (value << (count - 1)) & MSB_MASK_8;
		cpu.write(ProcessorFlag.CARRY, msbBefore != 0);
		int res = value << count;
		res = uint8(res);
		updateFlags8(res);
		int msb = res & MSB_MASK_8;
		cpu.write(ProcessorFlag.OVERFLOW, (msb ^ msbBefore) != 0);
		return res;
	}

	@Override
	public int shl16(int value, int count) {
		count &= SHIFT_COUNT_MASK;
		if (count == 0) {
			return value;
		}
		int msbBefore = (value << (count - 1)) & MSB_MASK_16;
		cpu.write(ProcessorFlag.CARRY, msbBefore != 0);
		int res = value << count;
		res = uint16(res);
		updateFlags16(res);
		int msb = res & MSB_MASK_16;
		cpu.write(ProcessorFlag.OVERFLOW, (msb ^ msbBefore) != 0);
		return res;
	}

	@Override
	public int shr8(int value, int count) {
		count &= SHIFT_COUNT_MASK;
		if (count == 0) {
			return value;
		}
		int msb = value & MSB_MASK_8;
		cpu.write(ProcessorFlag.OVERFLOW, msb != 0);
		setCarryFlagForRightShifts(value, count);
		int res = value >>> count;
		res = uint8(res);
		updateFlags8(res);
		return res;
	}

	@Override
	public int shr16(int value, int count) {
		count &= SHIFT_COUNT_MASK;
		if (count == 0) {
			return value;
		}
		int msb = value & MSB_MASK_16;
		cpu.write(ProcessorFlag.OVERFLOW, msb != 0);
		setCarryFlagForRightShifts(value, count);
		int res = value >>> count;
		res = uint16(res);
		updateFlags16(res);
		return res;
	}

	@Override
	public int sar8(int value, int count) {
		count &= SHIFT_COUNT_MASK;
		if (count == 0) {
			return value;
		}
		int res = int8(value);
		setCarryFlagForRightShifts(res, count);
		res >>= count;
		res = uint8(res);
		updateFlags8(res);
		cpu.write(ProcessorFlag.OVERFLOW, false);
		return res;
	}

	@Override
	public int sar16(int value, int count) {
		count &= SHIFT_COUNT_MASK;
		if (count == 0) {
			return value;
		}
		int res = int16(value);
		setCarryFlagForRightShifts(res, count);
		res >>= count;
		res = uint16(res);
		updateFlags16(res);
		cpu.write(ProcessorFlag.OVERFLOW, false);
		return res;
	}

	private void setCarryFlagForRightShifts(int value, int count) {
		int lastBit = (value >>> (count - 1)) & 0x1;
		cpu.write(ProcessorFlag.CARRY, lastBit == 1);
	}

	@Override
	public int rcl8(int value, int count) {
		count = (count & SHIFT_COUNT_MASK) % 9;
		if (count == 0) {
			return value;
		}
		int carry = (value >>> (8 - count)) & 0x1;
		int res = (value << count);
		int mask = (1 << (count - 1)) - 1;
		res |= (value >>> (9 - count)) & mask;
		if (cpu.read(ProcessorFlag.CARRY)) {
			res |= 1 << (count - 1);
		}
		res = uint8(res);
		cpu.write(ProcessorFlag.CARRY, carry != 0);

		boolean msb = (res & MSB_MASK_8) != 0;
		cpu.write(ProcessorFlag.OVERFLOW, msb ^ cpu.read(ProcessorFlag.CARRY));

		return res;
	}

	@Override
	public int rcl16(int value, int count) {
		count = (count & SHIFT_COUNT_MASK) % 17;
		if (count == 0) {
			return value;
		}
		int carry = (value >>> (16 - count)) & 0x1;
		int res = (value << count);
		int mask = (1 << (count - 1)) - 1;
		res |= (value >>> (17 - count)) & mask;
		if (cpu.read(ProcessorFlag.CARRY)) {
			res |= 1 << (count - 1);
		}
		res = uint16(res);
		cpu.write(ProcessorFlag.CARRY, carry != 0);

		boolean msb = (res & MSB_MASK_16) != 0;
		cpu.write(ProcessorFlag.OVERFLOW, msb ^ cpu.read(ProcessorFlag.CARRY));
		return res;
	}

	@Override
	public int rcr8(int value, int count) {
		count = (count & SHIFT_COUNT_MASK) % 9;
		if (count == 0) {
			return value;
		}
		int carry = (value >>> (count - 1)) & 0x1;
		int mask = (1 << (8 - count)) - 1;
		int res = (value >>> count) & mask;
		res |= (value << (9 - count));
		if (cpu.read(ProcessorFlag.CARRY)) {
			res |= 1 << (8 - count);
		}
		res = uint8(res);
		cpu.write(ProcessorFlag.CARRY, carry != 0);

		setOverflowForRigthRotate8(res);
		return res;
	}

	@Override
	public int rcr16(int value, int count) {
		count = (count & SHIFT_COUNT_MASK) % 17;
		if (count == 0) {
			return value;
		}
		int carry = (value >>> (count - 1)) & 0x1;
		int mask = (1 << (16 - count)) - 1;
		int res = (value >>> count) & mask;
		res |= (value << (17 - count));
		if (cpu.read(ProcessorFlag.CARRY)) {
			res |= 1 << (16 - count);
		}
		res = uint16(res);
		cpu.write(ProcessorFlag.CARRY, carry != 0);

		setOverflowForRigthRotate16(res);
		return res;
	}

	@Override
	public int rol8(int value, int count) {
		count = (count & SHIFT_COUNT_MASK) % 8;
		if (count == 0) {
			return value;
		}
		int carry = (value >>> (8 - count)) & 0x1;
		int res = (value << count);
		res |= (value >>> (8 - count));
		res = uint8(res);
		cpu.write(ProcessorFlag.CARRY, carry != 0);
		boolean msb = (res & MSB_MASK_8) != 0;
		cpu.write(ProcessorFlag.OVERFLOW, msb ^ cpu.read(ProcessorFlag.CARRY));
		return res;
	}

	@Override
	public int rol16(int value, int count) {
		count = (count & SHIFT_COUNT_MASK) % 16;
		if (count == 0) {
			return value;
		}
		int carry = (value >>> (16 - count)) & 0x1;
		int res = (value << count);
		res |= (value >>> (16 - count));
		res = uint16(res);
		cpu.write(ProcessorFlag.CARRY, carry != 0);
		boolean msb = (res & MSB_MASK_16) != 0;
		cpu.write(ProcessorFlag.OVERFLOW, msb ^ cpu.read(ProcessorFlag.CARRY));
		return res;
	}

	@Override
	public int ror8(int value, int count) {
		count = (count & SHIFT_COUNT_MASK) % 8;
		if (count == 0) {
			return value;
		}
		int carry = (value >>> (count - 1)) & 0x1;
		int mask = (1 << (8 - count)) - 1;
		int res = (value >>> count) & mask;
		res |= (value << (8 - count));
		res = uint8(res);
		cpu.write(ProcessorFlag.CARRY, carry != 0);

		setOverflowForRigthRotate8(res);
		return res;
	}

	@Override
	public int ror16(int value, int count) {
		count = (count & SHIFT_COUNT_MASK) % 16;
		if (count == 0) {
			return value;
		}
		int carry = (value >>> (count - 1)) & 0x1;
		int mask = (1 << (16 - count)) - 1;
		int res = (value >>> count) & mask;
		res |= (value << (16 - count));
		res = uint16(res);
		cpu.write(ProcessorFlag.CARRY, carry != 0);

		setOverflowForRigthRotate16(res);
		return res;
	}

	private void setOverflowForRigthRotate8(int res) {
		boolean msb = (res & MSB_MASK_8) != 0;
		boolean beforeMsb = (res & BEFORE_MSB_MASK_8) != 0;
		cpu.write(ProcessorFlag.OVERFLOW, msb ^ beforeMsb);
	}

	private void setOverflowForRigthRotate16(int res) {
		boolean msb = (res & MSB_MASK_16) != 0;
		boolean beforeMsb = (res & BEFORE_MSB_MASK_16) != 0;
		cpu.write(ProcessorFlag.OVERFLOW, msb ^ beforeMsb);
	}

	private void updateFlags16(int value) {
		setZeroFlag(value);
		setParityFlag(value);
		setSignFlag16(value);
	}

	private void updateFlags8(int value) {
		setZeroFlag(value);
		setParityFlag(value);
		setSignFlag8(value);
	}

	private void setZeroFlag(int value) {
		cpu.write(ProcessorFlag.ZERO, value == 0);
	}

	private void setSignFlag8(int value) {
		cpu.write(ProcessorFlag.SIGN, (value & MSB_MASK_8) != 0);
	}

	private void setSignFlag16(int value) {
		cpu.write(ProcessorFlag.SIGN, (value & MSB_MASK_16) != 0);
	}

	private void setParityFlag(int value) {
		cpu.write(ProcessorFlag.PARITY, isParity(uint8(value)));
	}

	private static boolean isParity(int value) {
		int low4 = value & 0xF;
		int high4 = (value >>> 4) & 0xF;
		return ((FOUR_BIT_PARITY_EVEN_TABLE >>> low4) & 1) == ((FOUR_BIT_PARITY_EVEN_TABLE >>> high4) & 1);
	}

	// from https://www.vogons.org/viewtopic.php?t=55377
	private static int overflowBitsAdd(int value1, int value2, int dst) {
		return ((value1 ^ dst) & (~(value1 ^ value2)));
	}

	private static int carryBitsAdd(int value1, int value2, int dst) {
		return (((value1 ^ value2) ^ dst) ^ ((value1 ^ dst) & (~(value1 ^ value2))));
	}

	private static int overflowBitsSub(int value1, int value2, int dst) {
		return ((value1 ^ dst) & (value1 ^ value2));
	}

	private static int borrowBitsSub(int value1, int value2, int dst) {
		return (((value1 ^ value2) ^ dst) ^ ((value1 ^ dst) & (value1 ^ value2)));
	}

	private static int int8(int value) {
		return (byte) value;
	}

	private static int int16(int value) {
		return (short) value;
	}

	private static int uint8(int value) {
		return value & 0xFF;
	}

	private static int uint16(int value) {
		return value & 0xFFFF;
	}

	private static long uint32(long value) {
		return value & 0xFFFFFFFFL;
	}
}
