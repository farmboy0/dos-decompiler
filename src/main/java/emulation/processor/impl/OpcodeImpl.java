package emulation.processor.impl;

import java.util.function.BiConsumer;

import io.vavr.API;
import io.vavr.NotImplementedError;

import assembly.Address16bit;
import assembly.Argument;
import assembly.Instruction;
import assembly.Opcode;
import assembly.Register;
import emulation.processor.Processor;
import emulation.processor.ProcessorFlag;

enum OpcodeImpl {
	ADD00(Opcode.ADD00, MathOp::exec), //
	ADD01(Opcode.ADD01, MathOp::exec), //
	ADD02(Opcode.ADD02, MathOp::exec), //
	ADD03(Opcode.ADD03, MathOp::exec), //
	ADD04(Opcode.ADD04, MathOp::exec), //
	ADD05(Opcode.ADD05, MathOp::exec), //
	PUSH06(Opcode.PUSH06, OpcodeImpl::push), //
	POP07(Opcode.POP07, OpcodeImpl::pop), //
	OR08(Opcode.OR08, MathOp::exec), //
	OR09(Opcode.OR09, MathOp::exec), //
	OR0A(Opcode.OR0A, MathOp::exec), //
	OR0B(Opcode.OR0B, MathOp::exec), //
	OR0C(Opcode.OR0C, MathOp::exec), //
	OR0D(Opcode.OR0D, MathOp::exec), //
	PUSH0E(Opcode.PUSH0E, OpcodeImpl::push), //

	JZ0F84(Opcode.JZ0F84, JumpOp::exec), //
	JNZ0F85(Opcode.JNZ0F85, JumpOp::exec), //

	ADC10(Opcode.ADC10, MathOp::exec), //
	ADC11(Opcode.ADC11, MathOp::exec), //
	ADC12(Opcode.ADC12, MathOp::exec), //
	ADC13(Opcode.ADC13, MathOp::exec), //
	ADC14(Opcode.ADC14, MathOp::exec), //
	ADC15(Opcode.ADC15, MathOp::exec), //
	PUSH16(Opcode.PUSH16, OpcodeImpl::push), //
	POP17(Opcode.POP17, OpcodeImpl::pop), //
	SBB18(Opcode.SBB18, MathOp::exec), //
	SBB19(Opcode.SBB19, MathOp::exec), //
	SBB1A(Opcode.SBB1A, MathOp::exec), //
	SBB1B(Opcode.SBB1B, MathOp::exec), //
	SBB1C(Opcode.SBB1C, MathOp::exec), //
	SBB1D(Opcode.SBB1D, MathOp::exec), //
	PUSH1E(Opcode.PUSH1E, OpcodeImpl::push), //
	POP1F(Opcode.POP1F, OpcodeImpl::pop), //
	AND20(Opcode.AND20, MathOp::exec), //
	AND21(Opcode.AND21, MathOp::exec), //
	AND22(Opcode.AND22, MathOp::exec), //
	AND23(Opcode.AND23, MathOp::exec), //
	AND24(Opcode.AND24, MathOp::exec), //
	AND25(Opcode.AND25, MathOp::exec), //

	DAA27(Opcode.DAA27, OpcodeImpl::daa), //

	SUB28(Opcode.SUB28, MathOp::exec), //
	SUB29(Opcode.SUB29, MathOp::exec), //
	SUB2A(Opcode.SUB2A, MathOp::exec), //
	SUB2B(Opcode.SUB2B, MathOp::exec), //
	SUB2C(Opcode.SUB2C, MathOp::exec), //
	SUB2D(Opcode.SUB2D, MathOp::exec), //

	DAS2F(Opcode.DAS2F, OpcodeImpl::das), //

	XOR30(Opcode.XOR30, MathOp::exec), //
	XOR31(Opcode.XOR31, MathOp::exec), //
	XOR32(Opcode.XOR32, MathOp::exec), //
	XOR33(Opcode.XOR33, MathOp::exec), //
	XOR34(Opcode.XOR34, MathOp::exec), //
	XOR35(Opcode.XOR35, MathOp::exec), //

	AAA37(Opcode.AAA37, OpcodeImpl::aaa), //

	CMP38(Opcode.CMP38, MathOp::exec), //
	CMP39(Opcode.CMP39, MathOp::exec), //
	CMP3A(Opcode.CMP3A, MathOp::exec), //
	CMP3B(Opcode.CMP3B, MathOp::exec), //
	CMP3C(Opcode.CMP3C, MathOp::exec), //
	CMP3D(Opcode.CMP3D, MathOp::exec), //

	AAS3F(Opcode.AAS3F, OpcodeImpl::aas), //

	INC40(Opcode.INC40, MathOp::inc16), //
	INC41(Opcode.INC41, MathOp::inc16), //
	INC42(Opcode.INC42, MathOp::inc16), //
	INC43(Opcode.INC43, MathOp::inc16), //
	INC44(Opcode.INC44, MathOp::inc16), //
	INC45(Opcode.INC45, MathOp::inc16), //
	INC46(Opcode.INC46, MathOp::inc16), //
	INC47(Opcode.INC47, MathOp::inc16), //

	DEC48(Opcode.DEC48, MathOp::dec16), //
	DEC49(Opcode.DEC49, MathOp::dec16), //
	DEC4A(Opcode.DEC4A, MathOp::dec16), //
	DEC4B(Opcode.DEC4B, MathOp::dec16), //
	DEC4C(Opcode.DEC4C, MathOp::dec16), //
	DEC4D(Opcode.DEC4D, MathOp::dec16), //
	DEC4E(Opcode.DEC4E, MathOp::dec16), //
	DEC4F(Opcode.DEC4F, MathOp::dec16), //

	PUSH50(Opcode.PUSH50, OpcodeImpl::push), //
	PUSH51(Opcode.PUSH51, OpcodeImpl::push), //
	PUSH52(Opcode.PUSH52, OpcodeImpl::push), //
	PUSH53(Opcode.PUSH53, OpcodeImpl::push), //
	PUSH54(Opcode.PUSH54, OpcodeImpl::push), //
	PUSH55(Opcode.PUSH55, OpcodeImpl::push), //
	PUSH56(Opcode.PUSH56, OpcodeImpl::push), //
	PUSH57(Opcode.PUSH57, OpcodeImpl::push), //

	POP58(Opcode.POP58, OpcodeImpl::pop), //
	POP59(Opcode.POP59, OpcodeImpl::pop), //
	POP5A(Opcode.POP5A, OpcodeImpl::pop), //
	POP5B(Opcode.POP5B, OpcodeImpl::pop), //
	POP5C(Opcode.POP5C, OpcodeImpl::pop), //
	POP5D(Opcode.POP5D, OpcodeImpl::pop), //
	POP5E(Opcode.POP5E, OpcodeImpl::pop), //
	POP5F(Opcode.POP5F, OpcodeImpl::pop), //

	PUSHA60(Opcode.PUSHA60, OpcodeImpl::pusha), //
	POPA61(Opcode.POPA61, OpcodeImpl::popa), //
	BOUND62(Opcode.BOUND62, OpcodeImpl::bound), //
	ARPL63(Opcode.ARPL63, OpcodeImpl::arpl), //

	PUSH68(Opcode.PUSH68, OpcodeImpl::push), //
	IMUL69(Opcode.IMUL69, MathOp::exec), //
	PUSH6A(Opcode.PUSH6A, OpcodeImpl::push), //
	IMUL6B(Opcode.IMUL6B, MathOp::exec), //

	INSB6C(Opcode.INSB6C, StringOp::exec), //
	INSW6D(Opcode.INSW6D, StringOp::exec), //
	OUTSB6E(Opcode.OUTSB6E, StringOp::exec), //
	OUTSW6F(Opcode.OUTSW6F, StringOp::exec), //

	JO70(Opcode.JO70, JumpOp::exec), //
	JNO71(Opcode.JNO71, JumpOp::exec), //
	JB72(Opcode.JB72, JumpOp::exec), //
	JNB73(Opcode.JNB73, JumpOp::exec), //
	JZ74(Opcode.JZ74, JumpOp::exec), //
	JNZ75(Opcode.JNZ75, JumpOp::exec), //
	JBE76(Opcode.JBE76, JumpOp::exec), //
	JA77(Opcode.JA77, JumpOp::exec), //
	JS78(Opcode.JS78, JumpOp::exec), //
	JNS79(Opcode.JNS79, JumpOp::exec), //
	JPE7A(Opcode.JPE7A, JumpOp::exec), //
	JPO7B(Opcode.JPO7B, JumpOp::exec), //
	JL7C(Opcode.JL7C, JumpOp::exec), //
	JGE7D(Opcode.JGE7D, JumpOp::exec), //
	JLE7E(Opcode.JLE7E, JumpOp::exec), //
	JG7F(Opcode.JG7F, JumpOp::exec), //

	ADD80(Opcode.ADD80, MathOp::exec), //
	OR80(Opcode.OR80, MathOp::exec), //
	ADC80(Opcode.ADC80, MathOp::exec), //
	SBB80(Opcode.SBB80, MathOp::exec), //
	AND80(Opcode.AND80, MathOp::exec), //
	SUB80(Opcode.SUB80, MathOp::exec), //
	XOR80(Opcode.XOR80, MathOp::exec), //
	CMP80(Opcode.CMP80, MathOp::exec), //

	ADD81(Opcode.ADD81, MathOp::exec), //
	OR81(Opcode.OR81, MathOp::exec), //
	ADC81(Opcode.ADC81, MathOp::exec), //
	SBB81(Opcode.SBB81, MathOp::exec), //
	AND81(Opcode.AND81, MathOp::exec), //
	SUB81(Opcode.SUB81, MathOp::exec), //
	XOR81(Opcode.XOR81, MathOp::exec), //
	CMP81(Opcode.CMP81, MathOp::exec), //

	ADD82(Opcode.ADD82, MathOp::exec), //
	OR82(Opcode.OR82, MathOp::exec), //
	ADC82(Opcode.ADC82, MathOp::exec), //
	SBB82(Opcode.SBB82, MathOp::exec), //
	AND82(Opcode.AND82, MathOp::exec), //
	SUB82(Opcode.SUB82, MathOp::exec), //
	XOR82(Opcode.XOR82, MathOp::exec), //
	CMP82(Opcode.CMP82, MathOp::exec), //

	ADD83(Opcode.ADD83, MathOp::exec), //
	OR83(Opcode.OR83, MathOp::exec), //
	ADC83(Opcode.ADC83, MathOp::exec), //
	SBB83(Opcode.SBB83, MathOp::exec), //
	AND83(Opcode.AND83, MathOp::exec), //
	SUB83(Opcode.SUB83, MathOp::exec), //
	XOR83(Opcode.XOR83, MathOp::exec), //
	CMP83(Opcode.CMP83, MathOp::exec), //

	TEST84(Opcode.TEST84, MathOp::exec), //
	TEST85(Opcode.TEST85, MathOp::exec), //
	XCHG86(Opcode.XCHG86, OpcodeImpl::xchg8), //
	XCHG87(Opcode.XCHG87, OpcodeImpl::xchg16), //
	MOV88(Opcode.MOV88, OpcodeImpl::mov), //
	MOV89(Opcode.MOV89, OpcodeImpl::mov), //
	MOV8A(Opcode.MOV8A, OpcodeImpl::mov), //
	MOV8B(Opcode.MOV8B, OpcodeImpl::mov), //
	MOV8C(Opcode.MOV8C, OpcodeImpl::mov), //
	LEA8D(Opcode.LEA8D, OpcodeImpl::lea), //
	MOV8E(Opcode.MOV8E, OpcodeImpl::mov), //
	POP8F(Opcode.POP8F, OpcodeImpl::pop), //

	NOP90(Opcode.NOP90, OpcodeImpl::nop), //

	XCHG91(Opcode.XCHG91, OpcodeImpl::xchg16), //
	XCHG92(Opcode.XCHG92, OpcodeImpl::xchg16), //
	XCHG93(Opcode.XCHG93, OpcodeImpl::xchg16), //
	XCHG94(Opcode.XCHG94, OpcodeImpl::xchg16), //
	XCHG95(Opcode.XCHG95, OpcodeImpl::xchg16), //
	XCHG96(Opcode.XCHG96, OpcodeImpl::xchg16), //
	XCHG97(Opcode.XCHG97, OpcodeImpl::xchg16), //

	CBW98(Opcode.CBW98, OpcodeImpl::cbw), //
	CWD99(Opcode.CWD99, OpcodeImpl::cwd), //

	CALL9A(Opcode.CALL9A, JumpOp::call), //

	WAIT9B(Opcode.WAIT9B, OpcodeImpl::wait), //
	PUSHF9C(Opcode.PUSHF9C, OpcodeImpl::pushf), //
	POPF9D(Opcode.POPF9D, OpcodeImpl::popf), //
	SAHF9E(Opcode.SAHF9E, OpcodeImpl::sahf), //
	LAHF9F(Opcode.LAHF9F, OpcodeImpl::lahf), //

	MOVA0(Opcode.MOVA0, OpcodeImpl::mov), //
	MOVA1(Opcode.MOVA1, OpcodeImpl::mov), //
	MOVA2(Opcode.MOVA2, OpcodeImpl::mov), //
	MOVA3(Opcode.MOVA3, OpcodeImpl::mov), //

	MOVSBA4(Opcode.MOVSBA4, StringOp::exec), //
	MOVSWA5(Opcode.MOVSWA5, StringOp::exec), //
	CMPSBA6(Opcode.CMPSBA6, StringOp::exec), //
	CMPSWA7(Opcode.CMPSWA7, StringOp::exec), //

	TESTA8(Opcode.TESTA8, MathOp::exec), //
	TESTA9(Opcode.TESTA9, MathOp::exec), //

	STOSBAA(Opcode.STOSBAA, StringOp::exec), //
	STOSWAB(Opcode.STOSWAB, StringOp::exec), //
	LODSBAC(Opcode.LODSBAC, StringOp::exec), //
	LODSWAD(Opcode.LODSWAD, StringOp::exec), //
	SCASBAE(Opcode.SCASBAE, StringOp::exec), //
	SCASWAF(Opcode.SCASWAF, StringOp::exec), //

	MOVB0(Opcode.MOVB0, OpcodeImpl::mov), //
	MOVB1(Opcode.MOVB1, OpcodeImpl::mov), //
	MOVB2(Opcode.MOVB2, OpcodeImpl::mov), //
	MOVB3(Opcode.MOVB3, OpcodeImpl::mov), //
	MOVB4(Opcode.MOVB4, OpcodeImpl::mov), //
	MOVB5(Opcode.MOVB5, OpcodeImpl::mov), //
	MOVB6(Opcode.MOVB6, OpcodeImpl::mov), //
	MOVB7(Opcode.MOVB7, OpcodeImpl::mov), //
	MOVB8(Opcode.MOVB8, OpcodeImpl::mov), //
	MOVB9(Opcode.MOVB9, OpcodeImpl::mov), //
	MOVBA(Opcode.MOVBA, OpcodeImpl::mov), //
	MOVBB(Opcode.MOVBB, OpcodeImpl::mov), //
	MOVBC(Opcode.MOVBC, OpcodeImpl::mov), //
	MOVBD(Opcode.MOVBD, OpcodeImpl::mov), //
	MOVBE(Opcode.MOVBE, OpcodeImpl::mov), //
	MOVBF(Opcode.MOVBF, OpcodeImpl::mov), //

	ROLC0(Opcode.ROLC0, MathOp::exec), //
	RORC0(Opcode.RORC0, MathOp::exec), //
	RCLC0(Opcode.RCLC0, MathOp::exec), //
	RCRC0(Opcode.RCRC0, MathOp::exec), //
	SHLC0(Opcode.SHLC0, MathOp::exec), //
	SHRC0(Opcode.SHRC0, MathOp::exec), //
	SARC0(Opcode.SARC0, MathOp::exec), //

	ROLC1(Opcode.ROLC1, MathOp::exec), //
	RORC1(Opcode.RORC1, MathOp::exec), //
	RCLC1(Opcode.RCLC1, MathOp::exec), //
	RCRC1(Opcode.RCRC1, MathOp::exec), //
	SHLC1(Opcode.SHLC1, MathOp::exec), //
	SHRC1(Opcode.SHRC1, MathOp::exec), //
	SARC1(Opcode.SARC1, MathOp::exec), //

	RETC2(Opcode.RETC2, JumpOp::ret1), //
	RETC3(Opcode.RETC3, JumpOp::ret0), //
	LESC4(Opcode.LESC4, OpcodeImpl::les), //
	LDSC5(Opcode.LDSC5, OpcodeImpl::lds), //
	MOVC6(Opcode.MOVC6, OpcodeImpl::mov), //
	MOVC7(Opcode.MOVC7, OpcodeImpl::mov), //

	ENTERC8(Opcode.ENTERC8, OpcodeImpl::enter), //
	LEAVEC9(Opcode.LEAVEC9, OpcodeImpl::leave), //
	RETFCA(Opcode.RETFCA, JumpOp::retf1), //
	RETFCB(Opcode.RETFCB, JumpOp::retf0), //
	INT3CC(Opcode.INT3CC, OpcodeImpl::interrupt), //
	INTCD(Opcode.INTCD, OpcodeImpl::interrupt), //
	INTOCE(Opcode.INTOCE, OpcodeImpl::interruptO), //
	IRETCF(Opcode.IRETCF, JumpOp::iret), //

	ROLD0(Opcode.ROLD0, MathOp::exec), //
	RORD0(Opcode.RORD0, MathOp::exec), //
	RCLD0(Opcode.RCLD0, MathOp::exec), //
	RCRD0(Opcode.RCRD0, MathOp::exec), //
	SHLD0(Opcode.SHLD0, MathOp::exec), //
	SHRD0(Opcode.SHRD0, MathOp::exec), //
	SARD0(Opcode.SARD0, MathOp::exec), //

	ROLD1(Opcode.ROLD1, MathOp::exec), //
	RORD1(Opcode.RORD1, MathOp::exec), //
	RCLD1(Opcode.RCLD1, MathOp::exec), //
	RCRD1(Opcode.RCRD1, MathOp::exec), //
	SHLD1(Opcode.SHLD1, MathOp::exec), //
	SHRD1(Opcode.SHRD1, MathOp::exec), //
	SARD1(Opcode.SARD1, MathOp::exec), //

	ROLD2(Opcode.ROLD2, MathOp::exec), //
	RORD2(Opcode.RORD2, MathOp::exec), //
	RCLD2(Opcode.RCLD2, MathOp::exec), //
	RCRD2(Opcode.RCRD2, MathOp::exec), //
	SHLD2(Opcode.SHLD2, MathOp::exec), //
	SHRD2(Opcode.SHRD2, MathOp::exec), //
	SARD2(Opcode.SARD2, MathOp::exec), //

	ROLD3(Opcode.ROLD3, MathOp::exec), //
	RORD3(Opcode.RORD3, MathOp::exec), //
	RCLD3(Opcode.RCLD3, MathOp::exec), //
	RCRD3(Opcode.RCRD3, MathOp::exec), //
	SHLD3(Opcode.SHLD3, MathOp::exec), //
	SHRD3(Opcode.SHRD3, MathOp::exec), //
	SARD3(Opcode.SARD3, MathOp::exec), //

	AAMD4(Opcode.AAMD4, OpcodeImpl::aam), //
	AADD5(Opcode.AADD5, OpcodeImpl::aad), //

	SALCD6(Opcode.SALCD6, OpcodeImpl::salc), //

	XLATD7(Opcode.XLATD7, OpcodeImpl::xlat), //

	LOOPNZE0(Opcode.LOOPNZE0, JumpOp::exec), //
	LOOPZE1(Opcode.LOOPZE1, JumpOp::exec), //
	LOOPE2(Opcode.LOOPE2, JumpOp::exec), //
	JCXZE3(Opcode.JCXZE3, JumpOp::exec), //
	INE4(Opcode.INE4, OpcodeImpl::in8), //
	INE5(Opcode.INE5, OpcodeImpl::in16), //
	OUTE6(Opcode.OUTE6, OpcodeImpl::out8), //
	OUTE7(Opcode.OUTE7, OpcodeImpl::out16), //
	CALLE8(Opcode.CALLE8, JumpOp::call), //
	JMPE9(Opcode.JMPE9, JumpOp::jmp), //
	JMPEA(Opcode.JMPEA, JumpOp::jmp), //
	JMPEB(Opcode.JMPEB, JumpOp::jmp), //
	INEC(Opcode.INEC, OpcodeImpl::in8), //
	INED(Opcode.INED, OpcodeImpl::in16), //
	OUTEE(Opcode.OUTEE, OpcodeImpl::out8), //
	OUTEF(Opcode.OUTEF, OpcodeImpl::out16), //

	INT1F1(Opcode.INT1F1, OpcodeImpl::interrupt), //
	HLTF4(Opcode.HLTF4, OpcodeImpl::hlt), //
	CMCF5(Opcode.CMCF5, OpcodeImpl::cmc), //

	TESTF6_GRP0(Opcode.TESTF6_GRP0, MathOp::exec), //
	TESTF6_GRP1(Opcode.TESTF6_GRP1, MathOp::exec), //
	NOTF6(Opcode.NOTF6, MathOp::not8), //
	NEGF6(Opcode.NEGF6, MathOp::neg8), //
	MULF6(Opcode.MULF6, MathOp::exec), //
	IMULF6(Opcode.IMULF6, MathOp::exec), //
	DIVF6(Opcode.DIVF6, MathOp::exec), //
	IDIVF6(Opcode.IDIVF6, MathOp::exec), //

	TESTF7_GRP0(Opcode.TESTF7_GRP0, MathOp::exec), //
	TESTF7_GRP1(Opcode.TESTF7_GRP1, MathOp::exec), //
	NOTF7(Opcode.NOTF7, MathOp::not16), //
	NEGF7(Opcode.NEGF7, MathOp::neg16), //
	MULF7(Opcode.MULF7, MathOp::exec), //
	IMULF7(Opcode.IMULF7, MathOp::exec), //
	DIVF7(Opcode.DIVF7, MathOp::exec), //
	IDIVF7(Opcode.IDIVF7, MathOp::exec), //

	CLCF8(Opcode.CLCF8, OpcodeImpl::clc), //
	STCF9(Opcode.STCF9, OpcodeImpl::stc), //
	CLIFA(Opcode.CLIFA, OpcodeImpl::cli), //
	STIFB(Opcode.STIFB, OpcodeImpl::sti), //
	CLDFC(Opcode.CLDFC, OpcodeImpl::cld), //
	STDFD(Opcode.STDFD, OpcodeImpl::std), //

	INCFE(Opcode.INCFE, MathOp::inc8), //
	DECFE(Opcode.DECFE, MathOp::dec8), //

	INCFF(Opcode.INCFF, MathOp::inc16), //
	DECFF(Opcode.DECFF, MathOp::dec16), //
	CALLFF_2(Opcode.CALLFF_2, JumpOp::call), //
	CALLFF_3(Opcode.CALLFF_3, JumpOp::call), //
	JMPFF_4(Opcode.JMPFF_4, JumpOp::jmp), //
	JMPFF_5(Opcode.JMPFF_5, JumpOp::jmp), //
	PUSHFF(Opcode.PUSHFF, OpcodeImpl::push), //
	;

	private final Opcode opcode;
	private final BiConsumer<Processor, Instruction> execHandler;

	private OpcodeImpl(Opcode opcode, BiConsumer<Processor, Instruction> execHandler) {
		this.opcode = opcode;
		this.execHandler = execHandler;
	}

	public static void exec(Processor cpu, Instruction inst) {
		byOpcode(inst.getOpcode()).execHandler.accept(cpu, inst);
	}

	private static OpcodeImpl byOpcode(Opcode opcode) {
		return API.Seq(values())
			.find(i -> i.opcode == opcode)
			.getOrElseThrow(() -> new NotImplementedError(opcode.name()));
	}

	private static void aaa(Processor cpu, Instruction inst) {
		boolean isAdjust = false;
		boolean isCarry = false;
		if ((cpu.read(Register.AL, null) & 0x0F) > 0x9 || cpu.read(ProcessorFlag.ADJUST)) {
			cpu.write(Register.AX, null, cpu.read(Register.AX, null) + 0x106);
			isAdjust = true;
			isCarry = true;
		}
		final int al = cpu.read(Register.AL, null) & 0x0F;
		cpu.write(Register.AL, null, al);
		cpu.write(ProcessorFlag.ADJUST, isAdjust);
		cpu.write(ProcessorFlag.CARRY, isCarry);
		// Undocumented behaviour
		cpu.write(ProcessorFlag.ZERO, al == 0);
		cpu.write(ProcessorFlag.PARITY, ProcessorFlag.isParity(al));
		cpu.write(ProcessorFlag.SIGN, (al & 0x80) != 0);
	}

	private static void aad(Processor cpu, Instruction inst) {
		throw new NotImplementedError("aad");
	}

	private static void aam(Processor cpu, Instruction inst) {
		throw new NotImplementedError("aam");
	}

	private static void aas(Processor cpu, Instruction inst) {
		boolean isAdjust = false;
		boolean isCarry = false;
		if ((cpu.read(Register.AL, null) & 0x0F) > 0x9 || cpu.read(ProcessorFlag.ADJUST)) {
			cpu.write(Register.AX, null, cpu.read(Register.AX, null) - 0x6);
			cpu.write(Register.AH, null, cpu.read(Register.AH, null) - 0x1);
			isAdjust = true;
			isCarry = true;
		}
		final int al = cpu.read(Register.AL, null) & 0x0F;
		cpu.write(Register.AL, null, al);
		cpu.write(ProcessorFlag.ADJUST, isAdjust);
		cpu.write(ProcessorFlag.CARRY, isCarry);
		// Undocumented behaviour
		cpu.write(ProcessorFlag.ZERO, al == 0);
		cpu.write(ProcessorFlag.PARITY, ProcessorFlag.isParity(al));
		cpu.write(ProcessorFlag.SIGN, (al & 0x80) != 0);
	}

	private static void arpl(Processor cpu, Instruction inst) {
		throw new NotImplementedError("arpl");
	}

	private static void bound(Processor cpu, Instruction inst) {
		throw new NotImplementedError("bound");
	}

	private static void cbw(Processor cpu, Instruction inst) {
		throw new NotImplementedError("cbw");
	}

	private static void clc(Processor cpu, Instruction inst) {
		cpu.write(ProcessorFlag.CARRY, false);
	}

	private static void cld(Processor cpu, Instruction inst) {
		cpu.write(ProcessorFlag.DIRECTION, false);
	}

	private static void cli(Processor cpu, Instruction inst) {
		cpu.write(ProcessorFlag.INTERRUPT, false);
	}

	private static void cmc(Processor cpu, Instruction inst) {
		throw new NotImplementedError("cmc");
	}

	private static void cwd(Processor cpu, Instruction inst) {
		throw new NotImplementedError("cwd");
	}

	private static void daa(Processor cpu, Instruction inst) {
		final boolean isCarryOld = cpu.read(ProcessorFlag.CARRY);

		int al = cpu.read(Register.AL, null);
		boolean isCarryNew = false;
		if ((al & 0x0F) > 0x9 || cpu.read(ProcessorFlag.ADJUST)) {
			final int result = (al + 0x6) & 0xFF;
			cpu.write(Register.AL, null, result);
			final int bits = ProcessorFlag.carryAdjustBitsAdd(al, 0x6, result);
			isCarryNew = isCarryOld || ProcessorFlag.isCarry8(bits);
			cpu.write(ProcessorFlag.ADJUST, true);
		} else {
			cpu.write(ProcessorFlag.ADJUST, false);
		}
		if (al > 0x99 || isCarryOld) {
			cpu.write(Register.AL, null, al + 0x60);
			isCarryNew = true;
		} else {
			isCarryNew = false;
		}
		cpu.write(ProcessorFlag.CARRY, isCarryNew);

		// Undocumented behaviour
		al = cpu.read(Register.AL, null);
		cpu.write(ProcessorFlag.ZERO, al == 0);
		cpu.write(ProcessorFlag.PARITY, ProcessorFlag.isParity(al));
		cpu.write(ProcessorFlag.SIGN, (al & 0x80) != 0);
	}

	private static void das(Processor cpu, Instruction inst) {
		final boolean isCarryOld = cpu.read(ProcessorFlag.CARRY);

		int al = cpu.read(Register.AL, null);
		boolean isCarryNew = false;
		if ((al & 0x0F) > 0x9 || cpu.read(ProcessorFlag.ADJUST)) {
			final int result = (al - 0x6) & 0xFF;
			cpu.write(Register.AL, null, result);
			final int bits = ProcessorFlag.carryAdjustBitsSub(al, 0x6, result);
			isCarryNew = isCarryOld || ProcessorFlag.isCarry8(bits);
			cpu.write(ProcessorFlag.ADJUST, true);
		} else {
			cpu.write(ProcessorFlag.ADJUST, false);
		}
		if (al > 0x99 || isCarryOld) {
			cpu.write(Register.AL, null, al - 0x60);
			isCarryNew = true;
		} else {
			isCarryNew = false;
		}
		cpu.write(ProcessorFlag.CARRY, isCarryNew);

		// Undocumented behaviour
		al = cpu.read(Register.AL, null);
		cpu.write(ProcessorFlag.ZERO, al == 0);
		cpu.write(ProcessorFlag.PARITY, ProcessorFlag.isParity(al));
		cpu.write(ProcessorFlag.SIGN, (al & 0x80) != 0);
	}

	private static void enter(Processor cpu, Instruction inst) {
		throw new NotImplementedError("enter");
	}

	private static void hlt(Processor cpu, Instruction inst) {
		// TODO
	}

	private static void in8(Processor cpu, Instruction inst) {
		final int port = cpu.read(inst.getArgument(0), inst.getSegmentOverride());
		cpu.write(Register.AL, cpu.read8FromPort(port));
	}

	private static void in16(Processor cpu, Instruction inst) {
		final int port = cpu.read(inst.getArgument(0), inst.getSegmentOverride());
		cpu.write(Register.AX, cpu.read8FromPort(port));
	}

	private static void interrupt(Processor cpu, Instruction inst) {
		// TODO
	}

	private static void interruptO(Processor cpu, Instruction inst) {
		// TODO
	}

	private static void lahf(Processor cpu, Instruction inst) {
		// TODO
	}

	private static void lds(Processor cpu, Instruction inst) {
		lXs(Register.DS, cpu, inst);
	}

	private static void lea(Processor cpu, Instruction inst) {
		throw new NotImplementedError("lea");
	}

	private static void leave(Processor cpu, Instruction inst) {
		throw new NotImplementedError("leave");
	}

	private static void les(Processor cpu, Instruction inst) {
		lXs(Register.ES, cpu, inst);
	}

	private static void lXs(Register segDst, Processor cpu, Instruction inst) {
		final Argument offDst = inst.getArgument(0);
		final Address16bit addr = cpu.readAddr(inst.getArgument(1));
		cpu.write(segDst, inst.getSegmentOverride(), addr.getSegment());
		cpu.write(offDst, inst.getSegmentOverride(), addr.getOffset());
	}

	private static void mov(Processor cpu, Instruction inst) {
		final Argument dst = inst.getArgument(0);
		final int srcValue = cpu.read(inst.getArgument(1), inst.getSegmentOverride());
		cpu.write(dst, inst.getSegmentOverride(), srcValue);
	}

	private static void out8(Processor cpu, Instruction inst) {
		final int port = cpu.read(inst.getArgument(0), inst.getSegmentOverride());
		cpu.write8ToPort(port, cpu.read(Register.AL));
	}

	private static void out16(Processor cpu, Instruction inst) {
		final int port = cpu.read(inst.getArgument(0), inst.getSegmentOverride());
		cpu.write16ToPort(port, cpu.read(Register.AX));
	}

	private static void nop(Processor cpu, Instruction inst) {
		// no op
	}

	private static void pop(Processor cpu, Instruction inst) {
		final Argument dst = inst.getArgument(0);
		final int srcValue = cpu.readStack();
		cpu.write(dst, inst.getSegmentOverride(), srcValue);
	}

	private static void popa(Processor cpu, Instruction inst) {
		cpu.write(Register.DI, cpu.readStack());
		cpu.write(Register.SI, cpu.readStack());
		cpu.write(Register.BP, cpu.readStack());
		cpu.readStack(); // not restoring SP
		cpu.write(Register.BX, cpu.readStack());
		cpu.write(Register.DX, cpu.readStack());
		cpu.write(Register.CX, cpu.readStack());
		cpu.write(Register.AX, cpu.readStack());
	}

	private static void popf(Processor cpu, Instruction inst) {
		// TODO
	}

	private static void push(Processor cpu, Instruction inst) {
		final int srcValue = cpu.read(inst.getArgument(0), inst.getSegmentOverride());
		cpu.writeStack(srcValue);
	}

	private static void pusha(Processor cpu, Instruction inst) {
		int sp = cpu.read(Register.SP);
		cpu.writeStack(cpu.read(Register.AX));
		cpu.writeStack(cpu.read(Register.CX));
		cpu.writeStack(cpu.read(Register.DX));
		cpu.writeStack(cpu.read(Register.BX));
		cpu.writeStack(sp);
		cpu.writeStack(cpu.read(Register.BP));
		cpu.writeStack(cpu.read(Register.SI));
		cpu.writeStack(cpu.read(Register.DI));
	}

	private static void pushf(Processor cpu, Instruction inst) {
		// TODO
	}

	private static void sahf(Processor cpu, Instruction inst) {
		// TODO
	}

	private static void salc(Processor cpu, Instruction inst) {
		// TODO
	}

	private static void stc(Processor cpu, Instruction inst) {
		cpu.write(ProcessorFlag.CARRY, true);
	}

	private static void std(Processor cpu, Instruction inst) {
		cpu.write(ProcessorFlag.DIRECTION, true);
	}

	private static void sti(Processor cpu, Instruction inst) {
		cpu.write(ProcessorFlag.INTERRUPT, true);
	}

	private static void wait(Processor cpu, Instruction inst) {
		throw new NotImplementedError("wait");
	}

	private static void xchg8(Processor cpu, Instruction inst) {
		final int value1 = cpu.read(inst.getArgument(0), inst.getSegmentOverride());
		final int value2 = cpu.read(inst.getArgument(1), inst.getSegmentOverride());
		cpu.write(inst.getArgument(0), inst.getSegmentOverride(), value2);
		cpu.write(inst.getArgument(1), inst.getSegmentOverride(), value1);
	}

	private static void xchg16(Processor cpu, Instruction inst) {
		final int value1 = cpu.read(inst.getArgument(0), inst.getSegmentOverride());
		final int value2 = cpu.read(inst.getArgument(1), inst.getSegmentOverride());
		cpu.write(inst.getArgument(0), inst.getSegmentOverride(), value2);
		cpu.write(inst.getArgument(1), inst.getSegmentOverride(), value1);
	}

	private static void xlat(Processor cpu, Instruction inst) {
		// TODO
	}
}
