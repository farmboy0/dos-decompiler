package emulation.processor.impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.vavr.API;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;

import assembly.Address16bit;
import assembly.Argument;
import assembly.Dereference;
import assembly.Displacement;
import assembly.Immediate;
import assembly.Instruction;
import assembly.Register;
import code.Image;
import emulation.memory.MemoryController;
import emulation.memory.MemorySegment;
import emulation.processor.Alu;
import emulation.processor.Processor;
import emulation.processor.ProcessorControl;
import emulation.processor.ProcessorFlag;
import emulation.processor.ProcessorListener;
import emulation.processor.Register16bit;
import emulation.processor.RegisterValue;

public class ProcessorImpl implements Runnable, Processor, ProcessorControl {
	private static final ExecutorService EXEC = Executors.newSingleThreadExecutor();

	private final MemoryController memory;
	private final Alu alu;
	private final Register16bit ax, bx, cx, dx, si, di, bp, sp, ip, cs, ds, es, ss;
	private final Map<Register, RegisterValue> regMap;

	private int flags;
	private Instruction currentInstruction;

	private Set<ProcessorListener> listeners = API.Set();
	private boolean running;

	public ProcessorImpl(MemoryController memory) {
		this.memory = memory;
		this.alu = new AluImpl(this);
		ax = new RegisterImpl("AX");
		bx = new RegisterImpl("BX");
		cx = new RegisterImpl("CX");
		dx = new RegisterImpl("DX");
		si = new RegisterImpl("SI");
		di = new RegisterImpl("DI");
		bp = new RegisterImpl("BP");
		sp = new RegisterImpl("SP");
		ip = new RegisterImpl("IP");
		cs = new RegisterImpl("CS");
		ds = new RegisterImpl("DS");
		es = new RegisterImpl("ES");
		ss = new RegisterImpl("SS");
		regMap = HashMap.ofEntries( //
			Map.entry(Register.AX, ax), Map.entry(Register.BX, bx), Map.entry(Register.CX, cx),
			Map.entry(Register.DX, dx), Map.entry(Register.SI, si), Map.entry(Register.DI, di),
			Map.entry(Register.BP, bp), Map.entry(Register.SP, sp), Map.entry(Register.CS, cs),
			Map.entry(Register.DS, ds), Map.entry(Register.ES, es), Map.entry(Register.SS, ss),
			Map.entry(Register.AH, ax.getHigh8()), Map.entry(Register.BH, bx.getHigh8()),
			Map.entry(Register.CH, cx.getHigh8()), Map.entry(Register.DH, dx.getHigh8()),
			Map.entry(Register.AL, ax.getLow8()), Map.entry(Register.BL, bx.getLow8()),
			Map.entry(Register.CL, cx.getLow8()), Map.entry(Register.DL, dx.getLow8()) //
		);
	}

	@Override
	public void setUp(Image image) {
		memory.putImage(image);
		cs.set(image.getCodeEntry().getSegment());
		ds.set(image.getStart().getSegment() - 16);
		es.set(image.getStart().getSegment() - 16);
		ip.set(image.getCodeEntry().getOffset());
		ss.set(image.getInitialStack().getSegment());
		sp.set(image.getInitialStack().getOffset());
		nextInst();
	}

	@Override
	public void run() {
		while (running)
			exec();
	}

	private void exec() {
		ip.set(currentInstruction.getNextAddress().getOffset());
		OpcodeImpl.exec(this, currentInstruction);
		listeners.forEach(l -> l.executed(currentInstruction));
		nextInst();
	}

	private void nextInst() {
		final int segment = cs.get();
		final int offset = ip.get();
		final MemorySegment codeSegment = memory.getSegment(segment);
		currentInstruction = Instruction.parseNext(codeSegment.at(offset), new Address16bit(segment, offset));
		listeners.forEach(l -> l.found(currentInstruction));
	}

	@Override
	public void start() {
		running = true;
		EXEC.execute(this);
	}

	@Override
	public void stepInto() {
		exec();
	}

	@Override
	public void stepOver() {
		exec();
	}

	@Override
	public void stepReturn() {
		exec();
	}

	@Override
	public void stop() {
		running = false;
	}

	@Override
	public void addProcessorListener(ProcessorListener l) {
		listeners = listeners.add(l);
	}

	@Override
	public void removeProcessorListener(ProcessorListener l) {
		listeners = listeners.remove(l);
	}

	@Override
	public Alu getAlu() {
		return alu;
	}

	@Override
	public Seq<RegisterValue> getRegisters() {
		return API.Seq(ax, bx, cx, dx, si, di, ds, es, cs, ip, ss, sp, bp);
	}

	@Override
	public int read(Argument arg, Register segmentOveride) {
		if (arg instanceof Immediate imm) {
			return imm.getValue();
		} else if (arg instanceof Displacement disp) {
			return disp.getValue();
		} else if (arg instanceof Register reg) {
			return regMap.apply(reg).get();
		} else if (arg instanceof Dereference deref) {
			int base = deref.getBase().map(regMap::apply).map(RegisterValue::get).orElse(0);
			int index = deref.getIndex().map(regMap::apply).map(RegisterValue::get).orElse(0);
			int offset = base + index + deref.getDisplacement();
			return memory
				.getSegment(regMap.apply(segmentOveride != null ? segmentOveride : deref.getSegment()).get() & 0xFFFF)
				.read(offset, deref.getType().getSize());
		} else
			throw new IllegalArgumentException(arg + " not valid for read.");
	}

	@Override
	public int read(Register arg) {
		return regMap.apply(arg).get();
	}

	@Override
	public boolean read(ProcessorFlag flag) {
		return (flags & flag.getBitmask()) > 0;
	}

	@Override
	public Address16bit readAddr(Argument arg) {
		return (Address16bit) arg;
	}

	@Override
	public int readEip() {
		return ip.get();
	}

	@Override
	public int readFlags() {
		return flags;
	}

	@Override
	public int readStack() {
		final int value = read16FromMem(Register.SS, Register.SP);
		this.sp.set(this.sp.get() + 2);
		return value;
	}

	@Override
	public int read8FromMem(Register segment, Register offset) {
		final int segmentValue = regMap.apply(segment).get();
		final int offsetValue = regMap.apply(offset).get();
		return memory.getSegment(segmentValue).read8(offsetValue);
	}

	@Override
	public int read8FromPort(int port) {
		// TODO Auto-generated method stub
		return -1;
	}

	@Override
	public int read16FromMem(Register segment, Register offset) {
		final int segmentValue = regMap.apply(segment).get();
		final int offsetValue = regMap.apply(offset).get();
		return memory.getSegment(segmentValue).read16(offsetValue);
	}

	@Override
	public int read16FromPort(int port) {
		// TODO Auto-generated method stub
		return -1;
	}

	@Override
	public void write(Argument arg, Register segmentOveride, int value) {
		if (arg instanceof Register reg) {
			regMap.apply(reg).set(value);
		} else if (arg instanceof Dereference deref) {
			int base = deref.getBase().map(regMap::apply).map(RegisterValue::get).orElse(0);
			int index = deref.getIndex().map(regMap::apply).map(RegisterValue::get).orElse(0);
			int offset = base + index + deref.getDisplacement();
			memory.getSegment(regMap.apply(segmentOveride != null ? segmentOveride : deref.getSegment()).get())
				.write(offset, value, deref.getType().getSize());
		} else
			throw new IllegalArgumentException(arg + " not valid for write.");
	}

	@Override
	public void write(Register arg, int value) {
		regMap.apply(arg).set(value);
	}

	@Override
	public void write(ProcessorFlag flag, boolean set) {
		int mask = flag.getBitmask();
		if (!set)
			flags &= ~mask;
		else
			flags |= mask;
	}

	@Override
	public void writeCsEip(int cs, int eip) {
		this.cs.set(cs);
		this.ip.set(eip);
	}

	@Override
	public void writeCsEip(int relativeOffset) {
		this.ip.set(this.ip.get() + relativeOffset);
	}

	@Override
	public void writeEip(int eip) {
		this.ip.set(eip);
	}

	@Override
	public void writeFlags(int flags) {
		this.flags = flags;
	}

	@Override
	public void writeStack(int value) {
		this.sp.set(this.sp.get() - 2);
		write16ToMem(Register.SS, Register.SP, value);
	}

	@Override
	public void write8ToMem(Register segment, Register offset, int value) {
		final int segmentValue = regMap.apply(segment).get();
		final int offsetValue = regMap.apply(offset).get();
		memory.getSegment(segmentValue).write8(offsetValue, value);
	}

	@Override
	public void write8ToPort(int port, int value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void write16ToMem(Register segment, Register offset, int value) {
		final int segmentValue = regMap.apply(segment).get();
		final int offsetValue = regMap.apply(offset).get();
		memory.getSegment(segmentValue).write16(offsetValue, value);
	}

	@Override
	public void write16ToPort(int port, int value) {
		// TODO Auto-generated method stub

	}
}
