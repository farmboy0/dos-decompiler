package code.imageref;

import java.nio.ByteBuffer;
import java.util.Objects;

import assembly.Address16bit;
import data.COMFile;
import data.DOSProgramFile;
import data.EXEFile;

public class ProgramFileRef implements ImageRef {
	private final DOSProgramFile program;
	private final Address16bit start;
	private final int index;

	public ProgramFileRef(DOSProgramFile program, Address16bit start, int index) {
		this.program = program;
		this.start = start;
		this.index = index;
	}

	public int getIndex() {
		return index;
	}

	@Override
	public boolean hasLoadModule() {
		return true;
	}

	@Override
	public String getName() {
		return program.getName();
	}

	@Override
	public String getFilename() {
		return program.getFilename();
	}

	@Override
	public ImageRefType getType() {
		if (program instanceof EXEFile)
			return ImageRefType.EXE;
		if (program instanceof COMFile)
			return ImageRefType.COM;
		return ImageRefType.DATA;
	}

	@Override
	public ByteBuffer getBuffer() {
		return program.getBuffer(index, start.getSegment());
	}

	@Override
	public Address16bit getCodeEntry() {
		return program.getCodeEntry(index, start.getSegment());
	}

	@Override
	public Address16bit getInitialStack() {
		return program.getInitialStack(index, start.getSegment());
	}

	@Override
	public int hashCode() {
		return Objects.hash(program, index, start);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProgramFileRef other = (ProgramFileRef) obj;
		return Objects.equals(program, other.program) && index == other.index && Objects.equals(start, other.start);
	}

	@Override
	public String toString() {
		return getName();
	}

}