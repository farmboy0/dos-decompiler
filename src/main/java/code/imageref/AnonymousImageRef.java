package code.imageref;

import java.nio.ByteBuffer;

import assembly.Address16bit;

public final class AnonymousImageRef implements ImageRef {
	private final ByteBuffer buf;

	public AnonymousImageRef(int ramSize) {
		buf = ByteBuffer.allocate(ramSize);
	}

	@Override
	public boolean hasLoadModule() {
		return false;
	}

	@Override
	public String getName() {
		return "Anonymous";
	}

	@Override
	public String getFilename() {
		return null;
	}

	@Override
	public ImageRefType getType() {
		return ImageRefType.ANONYMOUS;
	}

	@Override
	public Address16bit getInitialStack() {
		return null;
	}

	@Override
	public Address16bit getCodeEntry() {
		return null;
	}

	@Override
	public ByteBuffer getBuffer() {
		return buf;
	}

	@Override
	public String toString() {
		return getName();
	}

}