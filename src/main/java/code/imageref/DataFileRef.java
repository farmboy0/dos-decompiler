package code.imageref;

import java.nio.ByteBuffer;
import java.util.Objects;

import assembly.Address16bit;
import data.DataFile;

public class DataFileRef implements ImageRef {
	private final Address16bit loadAddress;
	private final DataFile dataFile;
	private final int start;
	private int length;

	public DataFileRef(Address16bit loadAddress, DataFile dataFile, int start, int length) {
		this.loadAddress = loadAddress;
		this.dataFile = dataFile;
		this.start = start;
		this.length = length;
	}

	@Override
	public String getName() {
		return dataFile.getName();
	}

	@Override
	public String getFilename() {
		return dataFile.getFilename();
	}

	@Override
	public ImageRefType getType() {
		return ImageRefType.DATA;
	}

	@Override
	public ByteBuffer getBuffer() {
		return dataFile.getContent(start, length);
	}

	@Override
	public boolean hasLoadModule() {
		return false;
	}

	@Override
	public Address16bit getCodeEntry() {
		return null;
	}

	@Override
	public Address16bit getInitialStack() {
		return null;
	}

	public Address16bit getLoadAddress() {
		return loadAddress;
	}

	public DataFile getDataFile() {
		return dataFile;
	}

	public int getStart() {
		return start;
	}

	public int getLength() {
		return length;
	}

	public int getEnd() {
		return getStart() + getLength();
	}

	public void addLength(int added) {
		length += added;
	}

	@Override
	public int hashCode() {
		return Objects.hash(loadAddress, dataFile, length, start);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof DataFileRef)) {
			return false;
		}
		DataFileRef other = (DataFileRef) obj;
		return Objects.equals(loadAddress, other.loadAddress) && Objects.equals(dataFile, other.dataFile)
			&& length == other.length && start == other.start;
	}

	@Override
	public String toString() {
		return String.format("%s(%d-%d)", dataFile.getName(), getStart(), getEnd());
	}
}