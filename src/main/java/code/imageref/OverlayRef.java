package code.imageref;

import java.nio.ByteBuffer;
import java.util.Objects;

import assembly.Address16bit;
import data.OverlayFile;

public class OverlayRef implements ImageRef {
	private final OverlayFile overlayFile;

	public OverlayRef(OverlayFile overlayFile) {
		this.overlayFile = overlayFile;
	}

	@Override
	public String getName() {
		return overlayFile.getName();
	}

	@Override
	public String getFilename() {
		return overlayFile.getFilename();
	}

	@Override
	public ImageRefType getType() {
		return ImageRefType.OVERLAY;
	}

	@Override
	public ByteBuffer getBuffer() {
		return overlayFile.getContent();
	}

	@Override
	public boolean hasLoadModule() {
		return false;
	}

	@Override
	public Address16bit getCodeEntry() {
		return null;
	}

	@Override
	public Address16bit getInitialStack() {
		return null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(overlayFile);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OverlayRef other = (OverlayRef) obj;
		return Objects.equals(overlayFile, other.overlayFile);
	}

	@Override
	public String toString() {
		return getName();
	}

}