package code.imageref;

import java.nio.ByteBuffer;

import assembly.Address16bit;

public interface ImageRef {

	String getName();

	String getFilename();

	ImageRefType getType();

	ByteBuffer getBuffer();

	boolean hasLoadModule();

	Address16bit getCodeEntry();

	Address16bit getInitialStack();

	public enum ImageRefType {
		ANONYMOUS, COM, DATA, EXE, OVERLAY;
	}
}
