package code;

import io.vavr.collection.Seq;
import io.vavr.collection.SortedSet;

public interface ParentWithChildren<T> {
	SortedSet<T> getChildren();

	default Seq<T> getChildrenAsSeq() {
		return getChildren().toArray();
	}

	default T getChild(int index) {
		return getChildrenAsSeq().get(index);
	}

	default int childrenCount() {
		return getChildrenAsSeq().size();
	}

	@SuppressWarnings("unchecked")
	default int indexOfChild(Object child) {
		return getChildrenAsSeq().indexOf((T) child);
	}
}
