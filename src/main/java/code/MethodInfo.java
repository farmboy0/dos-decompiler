package code;

import io.vavr.API;
import io.vavr.collection.Set;
import io.vavr.collection.SortedMap;
import io.vavr.collection.SortedSet;

import assembly.Address16bit;
import assembly.Instruction;

public class MethodInfo {
	private String name;
	private SortedSet<Address16bit> blocks = API.SortedSet();
	private SortedMap<AddressInfo, Set<Instruction>> callers = API.SortedMap();;
	private SortedSet<AddressInfo> calledMethods = API.SortedSet();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SortedSet<Address16bit> getBlocks() {
		return blocks;
	}

	public void setBlocks(SortedSet<Address16bit> blocks) {
		this.blocks = blocks.toSortedSet();
	}

	public void reset() {
		blocks = API.SortedSet();
		callers = API.SortedMap();
		calledMethods = API.SortedSet();
	}

	public void addCalledMethod(AddressInfo calledMethod) {
		calledMethods = calledMethods.add(calledMethod);
	}

	public void addCaller(AddressInfo callingMethod, Instruction inst) {
		callers = callers.computeIfAbsent(callingMethod, x -> API.Set())
			.apply((set, map) -> map.replace(callingMethod, set, set.add(inst)));
	}

	public AddressInfo getCaller(int index) {
		return callers.keySet().toArray().get(index);
	}

	public int getIndexOfCaller(AddressInfo caller) {
		return callers.keySet().toArray().indexOf(caller);
	}

	public Set<Instruction> getCallerInst(AddressInfo caller) {
		return callers.get(caller).getOrElse(API::Set);
	}

	public SortedSet<AddressInfo> getCallers() {
		return callers.keySet();
	}

	public int getCallersCount() {
		return callers.size();
	}

	public boolean isEmpty() {
		return this.blocks.isEmpty();
	}
}
