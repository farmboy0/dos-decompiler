package code;

import javax.annotation.Nonnull;

public interface AnalyzerMonitor {

	void taskStarted(@Nonnull AnalyzerTask task);

	void taskEnded(@Nonnull AnalyzerTask task);
}
