package code;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.function.Function;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.Tuple2;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.collection.SortedMap;
import io.vavr.collection.SortedSet;

import assembly.Address16bit;
import assembly.Instruction;
import code.AddressInfo.MemoryRef;

public class Segment implements Comparable<Segment> {

	private final Image image;
	private final int segment;
	private final int segmentOffset;
	private final CodeModifications modifications = new CodeModifications();

	private Instruction[] instructions;
	private SortedMap<Integer, AddressInfo> infos = API.SortedMap();
	private Seq<AddressInfo> infoSeq = API.Seq();

	private Map<Address16bit, Code> codeMap = API.Map();
	private SortedSet<Code> codeSet = API.SortedSet();

	private int start, end;

	public Segment(@Nonnull Image image, int segment) {
		this.image = image;
		this.segment = segment;
		this.segmentOffset = segment * 16 - image.getStart().linearAddress();
	}

	public SortedSet<Instruction> getCodeAt(Address16bit startAddress) {
		if (startAddress.getSegment() != getSegment()) {
			return API.SortedSet();
		}
		return codeMap.get(startAddress).map(Code::getCode).getOrElse(API::SortedSet);
	}

	public int getSegment() {
		return segment;
	}

	public Image getImage() {
		return image;
	}

	public SortedSet<Instruction> getInstructions() {
		return codeSet.flatMap(Code::getCode);
	}

	public AddressInfo getInfo(Address16bit target) {
		final int offset = target.getOffset();
		AddressInfo info = infos.getOrElse(offset, null);
		if (info == null) {
			info = new AddressInfo(getImage(), target);
			infos = infos.put(offset, info);
			infoSeq = API.Seq();
		}
		return info;
	}

	public Seq<AddressInfo> getInfos() {
		return infos.toArray().map(Tuple2::_2);
	}

	public Seq<AddressInfo> getDataAndMethodInfos() {
		if (infoSeq.isEmpty())
			infoSeq = getInfos().filter(AddressInfo::isStart);
		return infoSeq;
	}

	public Seq<AddressInfo> getMethodInfos() {
		return getInfos().filter(AddressInfo::isMethodStart);
	}

	public Seq<Address16bit> getJumpTargets() {
		return getInfos().filter(AddressInfo::isJumpTarget).map(AddressInfo::getAddress);
	}

	public CodeModifications getModifications() {
		return modifications;
	}

	public boolean isModified() {
		return getModifications().size() > 0;
	}

	public Seq<Content> getContentOrdered() {
		return codeSet.toArray()
			.flatMap(Code::toImageBufferOffsets)
			.prepend(getStartOffset())
			.append(getEndOffset())
			.sliding(2, 2)
			.map(Segment::trimNegativeOffsets)
			.filter(Segment::isDifferentStartEndOffsets)
			.map(Data::new)
			.map(Content.class::cast)
			.toSortedSet()
			.addAll(codeSet)
			.toArray();
	}

	private static Seq<Integer> trimNegativeOffsets(Seq<Integer> offsets) {
		if (offsets.get(0) < 0) {
			return API.Seq(0, offsets.get(1) < 0 ? 0 : offsets.get(1));
		}
		return offsets;
	}

	private static boolean isDifferentStartEndOffsets(Seq<Integer> offsets) {
		return !Objects.equals(offsets.get(0), offsets.get(1)) && offsets.get(0) < offsets.get(1);
	}

	public Instruction getInstruction(int offset) {
		if (instructions == null) {
			return null;
		}
		return instructions[offset];
	}

	public boolean hasInstruction(OffsetRange range) {
		if (instructions == null) {
			return false;
		}
		for (int offset = range.from(); offset < range.to(); offset++) {
			if (instructions[offset] != null)
				return true;
		}
		return false;
	}

	public void update() {
		if (instructions == null) {
			return;
		}
		splitBlocks();
		for (int i = 0; i < instructions.length; i++) {
			Instruction inst = instructions[i];
			if (inst == null)
				continue;
			if (!codeSet.exists(c -> c.contains(inst))) {
				Code code = new Code(inst);
				codeSet = codeSet.add(code);
				codeMap = codeMap.put(inst.getAddress(), code);
			}
			int j = i;
			while (j + 1 - i < inst.getSize()) {
				instructions[++j] = inst;
			}
			i = j;
		}
		mergeBlocks();
	}

	public void updateSegmentOffsets() {
		final Seq<AddressInfo> dataBlocks = getInfos().filter(AddressInfo::isDataBlockStart);
		start = dataBlocks.headOption().map(AddressInfo::getAddress).map(Address16bit::getOffset).getOrElse(0xFFFF);
		end = dataBlocks.lastOption().map(AddressInfo::getDataBlock).map(DataBlock::end).getOrElse(0);
		if (codeSet.headOption().map(Code::getOffset).getOrElse(0xFFFF) < start) {
			start = codeSet.headOption().map(Code::getOffset).getOrElse(0xFFFF);
		}
		if (codeSet.lastOption().map(c -> c.getOffset() + c.getLength()).getOrElse(0) > end) {
			end = codeSet.lastOption().map(c -> c.getOffset() + c.getLength()).getOrElse(0);
		}
		start = start + (segment << 4) - image.getStart().linearAddress();
		if (start < 0)
			start = 0;
		end = end + (segment << 4) - image.getStart().linearAddress();
		if (end > image.getLength())
			end = image.getLength();
	}

	void addInstruction(Instruction inst) {
		if (instructions == null) {
			instructions = new Instruction[0x10000];
		}
		instructions[inst.getAddress().getOffset()] = inst;
	}

	void addCodeModificator(MemoryRef source, Seq<OffsetRange> ranges) {
		getModifications().add(source, ranges);
	}

	void addDataBlock(DataBlock block) {
		block.getInfo().setDataBlock(block);
		infoSeq = API.Seq();
	}

	private void mergeBlocks() {
		Seq<Code> copy;
		do {
			copy = codeSet.toArray();
			for (int i = 0; i < copy.size() - 1; i++) {
				if (isConnected(copy.get(i), copy.get(i + 1))) {
					Code code = new Code(copy.get(i), copy.get(i + 1));
					codeSet = codeSet.remove(copy.get(i)).remove(copy.get(i + 1)).add(code);
					i++;
				}
			}
		} while (copy.size() != codeSet.size());
		codeMap = codeSet.toMap(code -> code.getCode().head().getAddress(), Function.identity());
	}

	private void splitBlocks() {
		codeSet = codeSet.flatMap(this::splitBlock);
		codeMap = codeSet.toMap(code -> code.getCode().head().getAddress(), Function.identity());
	}

	private Seq<Code> splitBlock(Code code) {
		Seq<Code> result = API.Seq();
		SortedSet<Instruction> instructions = code.getCode();
		while (hasJumpTargetInTheMiddle(instructions)) {
			Instruction first = instructions.head();
			SortedSet<Instruction> tail = instructions.tail();
			result = result.append(new Code(tail.takeUntil(this::isJumpTarget).add(first)));
			instructions = tail.dropUntil(this::isJumpTarget);
		}
		return result.append(new Code(instructions));
	}

	private boolean hasJumpTargetInTheMiddle(SortedSet<Instruction> instructions) {
		if (instructions.size() <= 1) {
			return false;
		}
		return instructions.drop(1).exists(this::isJumpTarget);
	}

	private boolean isConnected(Code c1, Code c2) {
		return isConnected(c1.getOffset(), c1.getLength(), c2.getOffset(), c2.getLength())
			&& !c1.getCode().last().isBlockFinisher() && !isJumpTarget(c2.getCode().head());
	}

	private boolean isJumpTarget(Instruction inst) {
		final int offset = inst.getAddress().getOffset();
		final AddressInfo info = infos.getOrElse(offset, null);
		return info != null && info.isJumpTarget();
	}

	private static boolean isConnected(int offset1, int length1, int offset2, int length2) {
		return offset1 - offset2 >= 0 && offset2 + length2 >= offset1
			|| offset2 - offset1 >= 0 && offset1 + length1 >= offset2;
	}

	@Override
	public int compareTo(Segment o) {
		if (o == null) {
			return 1;
		}
		return segment - o.segment;
	}

	@Override
	public int hashCode() {
		return Objects.hash(image, segment);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Segment)) {
			return false;
		}
		Segment other = (Segment) obj;
		return Objects.equals(image, other.image) && segment == other.segment;
	}

	public int getStartOffset() {
		return start;
	}

	public int getEndOffset() {
		return end;
	}

	@Override
	public String toString() {
		return String.format("%04X:%04X (%d-%d)", segment, 0, getStartOffset(), getEndOffset());
	}

	public abstract class Content implements Comparable<Content> {
		public boolean isCode() {
			return this instanceof Code;
		}

		public boolean isData() {
			return this instanceof Data;
		}

		public abstract SortedSet<Instruction> toCode();

		public ByteBuffer toData() {
			int offset = Segment.this.segmentOffset + getOffset();
			int length = getLength();
			return Segment.this.image.getBuffer().slice(offset, length);
		}

		public abstract int getOffset();

		public abstract int getLength();

		@Override
		public int compareTo(Content o) {
			if (o == null) {
				return 1;
			}
			return getOffset() - o.getOffset();
		}
	}

	public class Code extends Content {
		private SortedSet<Instruction> code = API.SortedSet();

		Code(Instruction inst) {
			code = code.add(inst);
		}

		Code(Iterable<Instruction> instructions) {
			code = code.addAll(instructions);
		}

		Code(Code... codeSet) {
			for (Code c : codeSet) {
				code = code.addAll(c.getCode());
			}
		}

		public SortedSet<Instruction> getCode() {
			return code;
		}

		public boolean contains(Instruction inst) {
			return code.contains(inst);
		}

		@Override
		public SortedSet<Instruction> toCode() {
			return this.getCode();
		}

		@Override
		public int getOffset() {
			return code.head().getAddress().getOffset();
		}

		@Override
		public int getLength() {
			return code.toArray().map(Instruction::getSize).sum().intValue();
		}

		Seq<Integer> toImageBufferOffsets() {
			int bufferStartOffset = Segment.this.segmentOffset + getOffset();
			return API.Seq(bufferStartOffset, bufferStartOffset + getLength());
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("(\n");
			sb.append(code.mkString("\n"));
			sb.append("\n)");
			return sb.toString();
		}
	}

	public class Data extends Content {
		private final int offset;
		private final int length;

		Data(int offset, int length) {
			this.offset = offset;
			this.length = length;
		}

		Data(Seq<Integer> bufferOffsets) {
			int startOffset = bufferOffsets.get(0);
			int endOffset = bufferOffsets.get(1);
			this.offset = startOffset - segmentOffset;
			this.length = endOffset - startOffset;
		}

		@Override
		public int getLength() {
			return length;
		}

		@Override
		public int getOffset() {
			return offset;
		}

		@Override
		public SortedSet<Instruction> toCode() {
			return API.SortedSet();
		}
	}

	public class CodeModifications {
		private Map<CodeModificator, Seq<OffsetRange>> codeModifiers = API.LinkedMap();
		private Seq<CodeModificator> keys = API.Seq();

		void add(MemoryRef source, Seq<OffsetRange> ranges) {
			final CodeModificator mod = new CodeModificator(Segment.this, source);
			codeModifiers = codeModifiers.put(mod, ranges);
			keys = codeModifiers.keySet().toArray();
		}

		public CodeModificator get(int index) {
			return keys.get(index);
		}

		public OffsetRange get(CodeModificator mod, int index) {
			return ranges(mod).get(index);
		}

		public int indexOf(CodeModificator mod) {
			return keys.indexOf(mod);
		}

		public int indexOf(CodeModificator mod, OffsetRange range) {
			return ranges(mod).indexOf(range);
		}

		public int size() {
			return codeModifiers.size();
		}

		public int size(CodeModificator mod) {
			return ranges(mod).size();
		}

		private Seq<OffsetRange> ranges(CodeModificator mod) {
			return codeModifiers.getOrElse(mod, API.Seq());
		}

		@Override
		public String toString() {
			return "Code modifications";
		}
	}

	public record CodeModificator(Segment segment, MemoryRef source) {

		@Override
		public String toString() {
			final AddressInfo info = source().image().getMethodsFor(source().address()).getOrNull();
			return String.format("[%s]%s", source().address(),
				info != null ? info.getMethodInfo().getName() : "unknown method");
		}
	}

	public record OffsetRange(int from, int to) implements Comparable<OffsetRange> {

		@Override
		public int compareTo(OffsetRange o) {
			if (o.from == from) {
				return to - o.to;
			}
			return from - o.from;
		}

		@Override
		public String toString() {
			if (from == to) {
				return String.format(":%04X", from);
			}
			return String.format(":%04X-:%04X", from, to);
		}
	}
}
