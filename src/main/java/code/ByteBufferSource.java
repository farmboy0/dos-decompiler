package code;

import java.nio.ByteBuffer;

import assembly.ByteSource;

public class ByteBufferSource implements ByteSource {
	private final ByteBuffer buf;
	private int mark;

	public ByteBufferSource(ByteBuffer buf) {
		this.buf = buf;
	}

	@Override
	public byte signedByte() {
		return buf.get();
	}

	@Override
	public short signedWord() {
		return buf.getShort();
	}

	@Override
	public int unsignedByte() {
		return buf.get() & 0xFF;
	}

	@Override
	public int unsignedWord() {
		return buf.getShort() & 0xFFFF;
	}

	@Override
	public void markStart() {
		mark = buf.position();
	}

	@Override
	public byte[] fromMark() {
		int current = buf.position();
		byte[] result = new byte[current - mark];
		buf.position(mark).get(result).position(current);
		return result;
	}
}
