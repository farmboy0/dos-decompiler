package code;

import java.nio.ByteBuffer;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.collection.Set;
import io.vavr.collection.SortedMap;
import io.vavr.collection.SortedSet;
import io.vavr.collection.TreeMap;

import assembly.Address16bit;
import assembly.Argument;
import assembly.ByteSource;
import assembly.Displacement;
import assembly.Instruction;

public class Disassembler {
	private final Project project;

	public Disassembler(Project project) {
		this.project = project;
	}

	public void disassemble(Image image) {
		final JumpAddresses jumps = new JumpAddresses(image);

		Address16bit a;
		while ((a = jumps.getNextUnparsedAddress()) != null) {
			disassemble(image, a, jumps);
		}
	}

	private void disassemble(Image image, Address16bit start, JumpAddresses jumps) {
		final ByteBuffer buffer = image.getBuffer().position(toBufferOffset(image, start));
		final ByteSource byteSource = new ByteBufferSource(buffer);

		Instruction inst = null;
		Address16bit adr = start;
		do {
			inst = Instruction.parseNext(byteSource, adr);
			jumps.handle(image, inst);
			System.out.println(inst);
			adr = inst.getNextAddress();

			project.addInstruction(inst);
		} while (!inst.isBlockFinisher());
	}

	@Nonnull
	private Set<Address16bit> computeJumpTargets(Instruction inst) {
		if (!inst.isJump()) {
			return API.Set();
		}
		final Argument arg0 = inst.getArgument(0);
		if (arg0 instanceof Address16bit addr) {
			return API.Set(addr);
		} else if (arg0 instanceof Displacement disp) {
			return API.Set(inst.getNextAddress().relativeAddress(disp.getValue()));
		}
		return API.Set();
	}

	private static int toBufferOffset(Image image, Address16bit address) {
		return address.linearAddress() - image.getStart().linearAddress();
	}

	private class JumpAddresses {
		private SortedMap<Address16bit, Boolean> addresses = TreeMap.empty();

		JumpAddresses(Image image) {
			image.getMethodinfos().map(AddressInfo::getAddress).forEach(this::addAddress);
		}

		void handle(Image image, Instruction inst) {
			markAddressParsed(inst.getAddress());

			computeJumpTargets(inst).forEach(target -> {
				final int bufferOffset = toBufferOffset(image, target);
				if (bufferOffset >= 0 && bufferOffset < image.getBuffer().limit()) {
					addAddress(target);
				} else {
					System.out.println("Encountered address outside image: " + target);
				}
				image.getInfo(inst.getAddress()).addJumpTarget(project.getImage(target), target);
			});
		}

		void addAddress(Address16bit addr) {
			addresses = addresses.put(addr, Boolean.FALSE, (oldV, newV) -> oldV);
		}

		boolean isAddressParsed(Address16bit address) {
			return Boolean.TRUE.equals(addresses.getOrElse(address, Boolean.FALSE));
		}

		void markAddressParsed(Address16bit address) {
			addresses = addresses.replaceValue(address, Boolean.TRUE);
		}

		void clearMarks() {
			addresses = addresses.replaceAll((a, b) -> Boolean.FALSE);
		}

		Address16bit getNextUnparsedAddress() {
			return getAddresses().find(a -> !isAddressParsed(a)).getOrNull();
		}

		public SortedSet<Address16bit> getAddresses() {
			return addresses.keySet().toSortedSet();
		}
	}
}
