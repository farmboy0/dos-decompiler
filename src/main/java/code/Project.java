package code;

import java.io.IOException;
import java.util.Comparator;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;

import assembly.Address16bit;
import assembly.Instruction;
import code.imageref.AnonymousImageRef;
import code.imageref.DataFileRef;
import code.imageref.ImageRef;
import code.imageref.OverlayRef;
import code.imageref.ProgramFileRef;
import data.DOSProgramFile;
import data.DataFile;
import data.EXEFile;
import data.OverlayFile;
import jdos.Dosbox;
import jdos.misc.setup.Config;
import jdos.sdl.JavaMapper;

public class Project {
	private static final Address16bit FIRST_ADDRESS_COM = new Address16bit(0x1010, 0x0);
	private static final Address16bit FIRST_ADDRESS_EXE = new Address16bit(0x1000, 0x0);

	private final Disassembler disassembler;
	private final MethodAnalyser methodAnalyzer;
	private final DataAnalyzer dataAnalyzer;
	private final Image[] imageMap;

	private Image anonymous;
	private Seq<Image> images = API.Seq();
	private Seq<Image> imagesWithoutData = API.Seq();
	private Seq<DataFile> dataFilesSeq = API.Seq();
	private Map<ImageRef, Image> imageRefMap = API.Map();
	private Map<String, DataFile> dataFiles = API.Map();
	private Map<DataFile, Seq<Image>> dataFileImages = API.Map();

	private String dosboxConfig = null;
	private String dosboxKeyMap = null;

	private Set<ProjectListener> listeners = API.Set();

	private boolean dirty;

	public Project() {
		this((0xFFFF << 4) + 0xFFFF);
	}

	public Project(int ramSize) {
		disassembler = new Disassembler(this);
		methodAnalyzer = new MethodAnalyser(this);
		dataAnalyzer = new DataAnalyzer(this);
		imageMap = new Image[ramSize];
		anonymous = addImage(new AnonymousImageRef(ramSize), new Address16bit(0, 0));
		updateImagesSeq();
		resetConfig();
		setDirty(false);
	}

	public Project(DOSProgramFile program) {
		this();
		addProgram(program);
		updateImagesSeq();
	}

	public String getName() {
		if (images.size() == 1) {
			return "New Project";
		}
		return images.get(1).getRef().getName();
	}

	public Image getAnonymous() {
		return anonymous;
	}

	public Seq<Image> getImages() {
		return images;
	}

	public Seq<Image> getImagesWithoutData() {
		return imagesWithoutData;
	}

	public Seq<DataFile> getDataFiles() {
		return dataFilesSeq;
	}

	public DataFile getDataFile(int index) {
		return dataFilesSeq.get(index);
	}

	public Seq<Image> getDataFileImages(DataFile dataFile) {
		return dataFileImages.apply(dataFile);
	}

	public Image getDataFileImage(DataFile dataFile, int index) {
		return dataFileImages.apply(dataFile).get(index);
	}

	public Image getImage(int index) {
		return images.get(index);
	}

	public Image getImage(Address16bit addr) {
		if (addr == null)
			return null;
		return imageMap[addr.linearAddress()];
	}

	public int getImagesCount() {
		return imagesWithoutData.size();
	}

	public int getDataFileCount() {
		return dataFilesSeq.size();
	}

	public int getDataFileImagesCount(DataFile dataFile) {
		return dataFileImages.apply(dataFile).size();
	}

	public Instruction getInstruction(Address16bit addr) {
		return getImage(addr).getInstruction(addr);
	}

	public String getDosboxConfig() {
		return dosboxConfig;
	}

	public void setDosboxConfing(String dosboxConfig) {
		this.dosboxConfig = dosboxConfig;
	}

	public String getDosboxKeyMap() {
		return dosboxKeyMap;
	}

	public void setDosboxKeyMap(String dosboxKeyMap) {
		this.dosboxKeyMap = dosboxKeyMap;
	}

	public boolean isDirty() {
		return dirty;
	}

	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

	public void addProgram(DOSProgramFile program) {
		addBinary(program, imageLoadAddress(program));
	}

	public void addBinary(DOSProgramFile program, Address16bit loadAddress) {
		Array.range(0, program.getImageCount())
			.forEach(i -> addImage(new ProgramFileRef(program, loadAddress, i), loadAddress));
	}

	public void addOverlay(OverlayFile overlay, Address16bit loadAddress) {
		addImage(new OverlayRef(overlay), loadAddress);
	}

	public Image addData(String filename, Address16bit loadAddress, int start, int length) throws IOException {
		DataFile dataFile = dataFiles.getOrElse(filename, null);
		if (dataFile == null) {
			dataFile = new DataFile(filename);
			dataFiles = dataFiles.put(filename, dataFile);
		}

		final Image previous = imageMap[loadAddress.linearAddress() - 1];
		if (previous != null && previous.getRef() instanceof DataFileRef dataRef) {
			boolean filesMatch = dataRef.getDataFile().equals(dataFile);
			boolean fileOffsetsMatch = dataRef.getEnd() == start;
			boolean memoryOffsetsMatch = previous.getEnd() == loadAddress.linearAddress();
			if (filesMatch && fileOffsetsMatch && memoryOffsetsMatch) {
				this.imageRefMap = this.imageRefMap.remove(dataRef);
				dataRef.addLength(length);
				this.imageRefMap = this.imageRefMap.put(dataRef, previous);
				updateImageMap(previous, loadAddress.linearAddress(), length);
				return previous;
			}
		}
		return addImage(new DataFileRef(loadAddress, dataFile, start, length), loadAddress);
	}

	public Image addImage(ImageRef imageRef, Address16bit loadAddress) {
		boolean updateListeners = false;
		Image image;
		if (!imageRefMap.containsKey(imageRef)) {
			image = new Image(imageRef, loadAddress);
			this.imageRefMap = this.imageRefMap.put(imageRef, image);
			updateListeners = true;
		} else {
			image = this.imageRefMap.apply(imageRef);
		}

		if (imageRef.hasLoadModule()) {
			image.addMethod(imageRef.getCodeEntry(), "main");
		}

		updateImageMap(image);
		if (updateListeners)
			updateListeners();

		return image;
	}

	private void updateImageMap(Image image) {
		updateImageMap(image, image.getStart().linearAddress(), image.getLength());
	}

	private void updateImageMap(Image image, int start, int length) {
		int end = start + length;
		for (int i = start; i < end; i++) {
			imageMap[i] = image;
		}
	}

	private static final DatafileImageComparator DATA_FILE_COMPARATOR = new DatafileImageComparator();

	public void updateImagesSeq() {
		this.images = this.imageRefMap.toArray().map(Tuple2::_2).sorted();
		this.imagesWithoutData = this.images.filter(image -> !(image.getRef() instanceof DataFileRef));
		this.dataFileImages = this.images.filter(image -> image.getRef() instanceof DataFileRef)
			.groupBy(image -> image.getRef().getFilename())
			.map((filename, dfImages) -> new Tuple2<>(this.dataFiles.apply(filename),
				dfImages.sorted(DATA_FILE_COMPARATOR)));
		this.dataFilesSeq = this.dataFileImages.keySet().toArray().sortBy(DataFile::getFilename);
	}

	public void reset() {
		updateImageMap(anonymous);
	}

	public void addInstruction(Instruction inst) {
		getImage(inst.getAddress()).addInstruction(inst);
		updateListeners();
	}

	public void addMemoryAccess(Address16bit source, Address16bit target, int base, int index) {
		final Image targetImage = getImage(target);
		boolean added = getImage(source).getInfo(source).addMemoryAccess(targetImage, target, base, index);
		if (added)
			updateListeners();
	}

	public void addStringParams(Address16bit source, Address16bit strSource, Address16bit strTarget, int count) {
		final Image srcImage = getImage(strSource);
		final Image dstImage = getImage(strTarget);
		final AddressInfo instInfo = getImage(source).getInfo(source);
		boolean added = instInfo.addStringParams(srcImage, strSource, dstImage, strTarget, count);
		Image sImg, tImg;
		for (int i = 0; i < count; i++) {
			sImg = strSource != null ? imageMap[strSource.linearAddress() + i] : null;
			tImg = strTarget != null ? imageMap[strTarget.linearAddress() + i] : null;
			if (sImg != srcImage || tImg != dstImage)
				added |= instInfo.addStringParams(sImg, strSource, tImg, strTarget, count);
		}
		if (added)
			updateListeners();
	}

	public void disassemble() {
		images.forEach(this::disassemble);
	}

	public void disassemble(Image image) {
		if (image.hasLoadModule())
			disassembler.disassemble(image);
	}

	public void update(@Nonnull AnalyzerMonitor monitor) {
		monitor.taskStarted(AnalyzerTask.UPDATE_IMAGES_LIST);
		updateImagesSeq();
		monitor.taskEnded(AnalyzerTask.UPDATE_IMAGES_LIST);

		monitor.taskStarted(AnalyzerTask.UPDATE_SEGMENT_CONTENT);
		getImages().forEach(Image::update);
		monitor.taskEnded(AnalyzerTask.UPDATE_SEGMENT_CONTENT);

		methodAnalyzer.analyze(monitor);
		dataAnalyzer.analyze(monitor);

		monitor.taskStarted(AnalyzerTask.UPDATE_SEGMENT_OFFSETS);
		getImages().forEach(Image::updateSegmentOffsets);
		monitor.taskEnded(AnalyzerTask.UPDATE_SEGMENT_OFFSETS);
	}

	public void resetConfig() {
		Config cfg = new Config();
		Dosbox.Init(cfg);
		this.dosboxConfig = cfg.PrintConfig();
		JavaMapper.MAPPER_Init();
		this.dosboxKeyMap = JavaMapper.MAPPER_Binds();
	}

	public void addListener(ProjectListener listener) {
		listeners = listeners.add(listener);
	}

	public void removeListener(ProjectListener listener) {
		listeners = listeners.remove(listener);
	}

	private void updateListeners() {
		dirty = true;
		listeners.forEach(ProjectListener::projectChanged);
	}

	private static Address16bit imageLoadAddress(DOSProgramFile program) {
		return program instanceof EXEFile ? FIRST_ADDRESS_EXE : FIRST_ADDRESS_COM;
	}

	private static final class DatafileImageComparator implements Comparator<Image> {
		@Override
		public int compare(Image i1, Image i2) {
			final DataFileRef ref1 = (DataFileRef) i1.getRef();
			final DataFileRef ref2 = (DataFileRef) i2.getRef();
			if (ref1.getStart() == ref2.getStart())
				return ref1.getLoadAddress().compareTo(ref2.getLoadAddress());
			return ref1.getStart() - ref2.getStart();
		}
	}
}
