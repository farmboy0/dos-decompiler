package code;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import io.vavr.API;
import io.vavr.collection.Seq;
import io.vavr.collection.SortedSet;

import code.AddressInfo.Variable;

public class DataBlock implements ParentWithChildren<DataBlockContent<?>>, DataBlockContent<DataBlock> {
	private final Variable variable;
	private final int startOffset;
	private final int byteLength;
	private final int endOffset;

	private DataBlock parent;

	private SortedSet<DataBlock> subRanges = API.SortedSet();
	private Seq<DataBlockContent<?>> children = API.Seq();

	public DataBlock(Variable variable) {
		this.variable = variable;
		this.startOffset = variable.segmentOffset();
		this.byteLength = variable.byteLength();
		this.endOffset = variable.segmentEnd();
	}

	@Override
	public SortedSet<DataBlockContent<?>> getChildren() {
		return API.<DataBlockContent<?>>SortedSet().addAll(subRanges).addAll(variable.getAccesses());
	}

	@Override
	public Seq<DataBlockContent<?>> getChildrenAsSeq() {
		return children;
	}

	public AddressInfo getInfo() {
		return variable.info();
	}

	public String getName() {
		return variable.getName();
	}

	public void setName(String newName) {
		variable.setName(newName);
	}

	public DataBlock getParent() {
		return parent;
	}

	public void setParent(DataBlock parent) {
		this.parent = parent;
	}

	public Variable getVariable() {
		return variable;
	}

	public int getStartOffset() {
		return startOffset;
	}

	public boolean isConst() {
		return variable.isConst() && subRanges.forAll(DataBlock::isConst);
	}

	public int byteLength() {
		return byteLength;
	}

	public int end() {
		return endOffset;
	}

	ByteBuffer content = null;

	public ByteBuffer getContent() {
		if (content == null) {
			final AddressInfo info = variable.info();
			final Image image = info.getImage();
			final int start = info.getAddress().linearAddress() - image.getStart().linearAddress();
			final int end = start + byteLength;

			final int imageLength = image.getBuffer().limit();
			if (start < 0 || end > imageLength) {
				final int newStartPosition = start < 0 ? 0 - start : 0;
				final int oldStartPosition = start < 0 ? 0 : start;
				final int oldEndPosition = end > imageLength ? imageLength : end;
				if (oldStartPosition >= oldEndPosition) {
					content = ByteBuffer.allocate(byteLength);
				} else {
					content = ByteBuffer.allocate(byteLength)
						.position(newStartPosition)
						.put(image.getBuffer().slice(oldStartPosition, oldEndPosition - oldStartPosition));
				}
			} else {
				content = image.getBuffer().slice(start, byteLength);
			}
		}
		return content.duplicate().rewind();
	}

	private String contentAsString = null;

	public String getContentAsString() {
		if (contentAsString == null) {
			final byte[] b = new byte[byteLength];
			getContent().get(b);
			for (int i = 0; i < b.length; i++) {
				if (b[i] < 32 || b[i] > 126) {
					b[i] = ' ';
				}
			}
			contentAsString = new String(b, StandardCharsets.US_ASCII);
		}
		return contentAsString;
	}

	public boolean isSubRange(DataBlock dataBlock) {
		if (this.variable.equals(dataBlock.variable)) {
			return false;
		}
		return getStartOffset() <= dataBlock.getStartOffset() && end() >= dataBlock.end();
	}

	public void addSubRange(DataBlock dataBlock) {
		if (!isSubRange(dataBlock)) {
			return;
		}
		subRanges = subRanges.add(dataBlock);
		dataBlock.setParent(this);
	}

	public void updateChildren() {
		this.children = ParentWithChildren.super.getChildrenAsSeq();
	}

	@Override
	public int compare(DataBlock o) {
		return variable.compareTo(o.variable);
	}

	@Override
	public int hashCode() {
		return Objects.hash(variable);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataBlock other = (DataBlock) obj;
		return Objects.equals(variable, other.variable);
	}

	@Override
	public String toString() {
		return String.format("%s %s%s", variable.info().getAddress(), isConst() ? "const " : "", variable);
	}
}
