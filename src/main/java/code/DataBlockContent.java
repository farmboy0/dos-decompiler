package code;

public interface DataBlockContent<T> extends Comparable<DataBlockContent<?>> {

	int compare(T other);

	@Override
	@SuppressWarnings("unchecked")
	default int compareTo(DataBlockContent<?> o) {
		if (this.getClass().equals(o.getClass())) {
			return this.compare((T) o);
		}
		return this instanceof DataBlock ? 1 : -1;
	}

}
