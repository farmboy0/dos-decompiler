package code;

import static code.AddressInfo.VariableAccessType.READ;
import static code.AddressInfo.VariableSize.BYTE;
import static code.AddressInfo.VariableSize.BYTE_STR;
import static code.AddressInfo.VariableSize.WORD;
import static code.AddressInfo.VariableSize.WORD_STR;
import static java.util.function.Function.identity;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import io.vavr.collection.SortedMap;
import io.vavr.collection.SortedSet;

import assembly.Address16bit;
import assembly.Instruction;
import code.Segment.OffsetRange;

public class AddressInfo implements Comparable<AddressInfo> {
	private final MemoryRef ref;
	private DataBlock dataBlock;
	private MethodInfo methodInfo;
	// from which locations this address is jumped to
	// excluding CALL returns
	private Set<MemoryRef> jumpSources = API.Set();
	// the targets the instruction at this address jumps to
	private Set<MemoryRef> jumpTargets = API.Set();
	private Set<MemoryAccess> memoryAccesses = API.Set();
	private Set<StringParameter> stringParameter = API.Set();

	private SortedMap<Variable, String> variableNames = API.SortedMap();
	private SortedMap<Variable, SortedSet<VariableAccess>> variables = API.SortedMap();
	private SortedSet<Variable> variableRefs = API.SortedSet();
	private Map<Variable, DataBlock> blockMap = API.Map();

	private boolean codeModificator = false;

	public AddressInfo(@Nonnull Image image, @Nonnull Address16bit address) {
		this.ref = new MemoryRef(image, address);
	}

	public MemoryRef getRef() {
		return ref;
	}

	public Image getImage() {
		return ref.image();
	}

	public Address16bit getAddress() {
		return ref.address();
	}

	public int getOffset() {
		return ref.address().getOffset();
	}

	public Segment getSegment() {
		return getRef().getSegment();
	}

	public Instruction getiInstruction() {
		return getSegment().getInstruction(getOffset());
	}

	public Set<MemoryRef> getJumpSources() {
		return jumpSources;
	}

	public Set<MemoryRef> getJumpTargets() {
		return jumpTargets;
	}

	public Set<MemoryAccess> getMemoryAccesses() {
		return memoryAccesses;
	}

	public Set<StringParameter> getStringParameter() {
		return stringParameter;
	}

	public SortedSet<Variable> getVariables() {
		return variables.keySet();
	}

	public SortedSet<Variable> getVariableRefs() {
		return variableRefs;
	}

	public SortedMap<Variable, String> getVariableNames() {
		return variableNames;
	}

	public DataBlock getDataBlock() {
		return dataBlock;
	}

	public MethodInfo getMethodInfo() {
		return methodInfo;
	}

	public SortedSet<Instruction> getInstructions() {
		if (!isMethodStart())
			return API.SortedSet();
		final Segment segment = getSegment();
		return getMethodInfo().getBlocks().flatMap(segment::getCodeAt);
	}

	private static final Predicate<SortedSet<Instruction>> NOT_EMPTY = ((Predicate<SortedSet<Instruction>>) SortedSet::isEmpty)
		.negate();

	public Map<Address16bit, SortedSet<Instruction>> getCodeblocks() {
		if (!isMethodStart())
			return API.Map();
		final Segment segment = getSegment();
		return getMethodInfo().getBlocks()
			.toSet()
			.map(segment::getCodeAt)
			.filter(NOT_EMPTY)
			.toMap(code -> code.head().getAddress(), identity());
	}

	public boolean isJumpTarget() {
		return !getJumpSources().isEmpty();
	}

	public boolean isStart() {
		return isDataBlockStart() || isMethodStart();
	}

	public boolean isDataBlockStart() {
		return dataBlock != null;
	}

	public boolean isMethodStart() {
		return methodInfo != null;
	}

	public boolean isCodeModificator() {
		return codeModificator;
	}

	void clearDataAccess() {
		this.dataBlock = null;
		this.variables = API.SortedMap();
		this.variableRefs = API.SortedSet();
		this.blockMap = API.Map();
	}

	public boolean isEmpty() {
		return this.jumpSources.isEmpty() && this.jumpTargets.isEmpty() && this.memoryAccesses.isEmpty()
			&& this.stringParameter.isEmpty() && (this.methodInfo == null || this.methodInfo.isEmpty());
	}

	void setCodeModificator(boolean codeModificator) {
		this.codeModificator = codeModificator;
	}

	void setDataBlock(DataBlock dataBlock) {
		this.dataBlock = dataBlock;
	}

	public void setMethodName(String name) {
		if (this.methodInfo == null)
			this.methodInfo = new MethodInfo();
		if (this.methodInfo.getName() == null)
			this.methodInfo.setName(name);
	}

	void removeMethod() {
		this.methodInfo = null;
	}

	void addJumpSource(Image image, Address16bit source) {
		this.jumpSources = this.jumpSources.add(new MemoryRef(image, source));
	}

	public void addJumpTarget(Image image, Address16bit target) {
		this.jumpTargets = this.jumpTargets.add(new MemoryRef(image, target));
	}

	public boolean addMemoryAccess(Image targetImage, Address16bit targetAddress, int base, int index) {
		MemoryRef memoryRef = new MemoryRef(targetImage, targetAddress);
		final Set<MemoryAccess> old = this.memoryAccesses;
		this.memoryAccesses = this.memoryAccesses.add(new MemoryAccess(this, memoryRef, base, index));
		return this.memoryAccesses != old;
	}

	public boolean addStringParams(Image srcImage, Address16bit strSource, Image dstImage, Address16bit strTarget,
		int count) {

		MemoryRef src = srcImage != null ? new MemoryRef(srcImage, strSource) : null;
		MemoryRef dst = dstImage != null ? new MemoryRef(dstImage, strTarget) : null;
		final Set<StringParameter> old = this.stringParameter;
		this.stringParameter = this.stringParameter.add(new StringParameter(this, src, dst, count));
		return this.stringParameter != old;
	}

	void addVariableRef(Variable v) {
		variableRefs = variableRefs.add(v);
	}

	Variable addByteAccess(MemoryRef src, boolean isRead, boolean isWrite, int count) {
		Variable v = appendVar(BYTE, count);
		appendAccess(v, new VariableAccess(src, VariableAccessType.from(isRead, isWrite)));
		return v;
	}

	Variable addWordAccess(MemoryRef src, boolean isRead, boolean isWrite, int count) {
		Variable v = appendVar(WORD, count);
		appendAccess(v, new VariableAccess(src, VariableAccessType.from(isRead, isWrite)));
		return v;
	}

	Variable addByteStringAccess(MemoryRef src, boolean isRead, boolean isWrite, int count) {
		Variable v = appendVar(BYTE_STR, count);
		appendAccess(v, new VariableAccess(src, VariableAccessType.from(isRead, isWrite)));
		return v;
	}

	Variable addWordStringAccess(MemoryRef src, boolean isRead, boolean isWrite, int count) {
		Variable v = appendVar(WORD_STR, count);
		appendAccess(v, new VariableAccess(src, VariableAccessType.from(isRead, isWrite)));
		return v;
	}

	public Variable appendVar(VariableSize size, int count) {
		final Variable v = new Variable(this, size, count);
		if (!blockMap.containsKey(v))
			blockMap = blockMap.put(v, new DataBlock(v));
		return v;
	}

	private void appendAccess(Variable v, VariableAccess access) {
		variables = variables.put(v, variables.getOrElse(v, API.SortedSet()).add(access));
	}

	public void setVariableName(String name, Variable variable) {
		variableNames = variableNames.put(variable, name);
	}

	@Override
	public int compareTo(AddressInfo o) {
		return getAddress().compareTo(o.getAddress());
	}

	@Override
	public int hashCode() {
		return ref.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if ((obj == null) || (getClass() != obj.getClass()))
			return false;
		AddressInfo other = (AddressInfo) obj;
		return Objects.equals(ref, other.ref);
	}

	@Override
	public String toString() {
		if (isMethodStart())
			return String.format("%s %s", ref.address, getMethodInfo().getName());
		if (isDataBlockStart()) {
			return getDataBlock().toString();
		}
		return "empty";
	}

	public record MemoryRef(Image image, Address16bit address) implements Comparable<MemoryRef> {
		public Segment getSegment() {
			return image().getSegment(address());
		}

		public AddressInfo getInfo() {
			return image().getInfo(address());
		}

		@Override
		public int compareTo(MemoryRef o) {
			if (image().equals(o.image())) {
				return address().compareTo(o.address());
			}
			return image().compareTo(o.image());
		}

		public Set<AddressInfo> asMethods() {
			return image().getMethodsFor(address());
		}
	}

	public record MemoryAccess(AddressInfo info, MemoryRef ref, int base, int index)
		implements Comparable<MemoryAccess> {

		public Segment getSegment() {
			return ref().getSegment();
		}

		public OffsetRange getRange() {
			int countSize = info().getiInstruction().getDerefernce().getType().getSize().getByteCount();
			int start = ref().address().getOffset();
			int end = ref().address().relativeAddress(countSize).getOffset();
			return new OffsetRange(start, end);
		}

		@Override
		public int compareTo(MemoryAccess o) {
			return ref().compareTo(o.ref());
		}
	}

	public record StringParameter(AddressInfo info, MemoryRef src, MemoryRef dst, int count)
		implements Comparable<StringParameter> {

		public Segment dstSegment() {
			if (dst() == null)
				return null;
			return dst().getSegment();
		}

		public OffsetRange dstRange() {
			if (dst() == null)
				return new OffsetRange(-1, -1);
			int countSize = info().getiInstruction().isStringByteOp() ? 1 : 2;
			int start = count() > 0 ? dst().address()
				.getOffset() : dst().address().relativeAddress(count() * countSize).getOffset();
			int end = count() < 0 ? dst().address()
				.getOffset() : dst().address().relativeAddress(count() * countSize).getOffset();
			return new OffsetRange(start, end);
		}

		@Override
		public int compareTo(StringParameter o) {
			if (src() != null && o.src() != null) {
				if (src().equals(o.src()))
					return count - o.count;
				return src().compareTo(o.src());
			}
			if (dst() != null && o.dst() != null) {
				if (dst().equals(o.dst()))
					return count - o.count;
				return dst().compareTo(o.dst());
			}
			return src() != null ? -1 : 1;
		}
	}

	public record Variable(AddressInfo info, VariableSize size, int count) implements Comparable<Variable> {

		public DataBlock getDataBlock() {
			return info.blockMap.apply(this);
		}

		public SortedSet<VariableAccess> getAccesses() {
			return info.variables.apply(this);
		}

		public Optional<VariableAccess> getAccess(@Nonnull MemoryRef source) {
			return getAccesses().find(a -> a.source().equals(source)).toJavaOptional();
		}

		public String getName() {
			return info.variableNames.getOrElse(this, getDefaultName());
		}

		public String getDefaultName() {
			return String.format("%s%s", prefix(size(), count()), info().getAddress().asNameSuffix());
		}

		public void setName(String newName) {
			info.setVariableName(newName, this);
		}

		public int byteLength() {
			return WORD.equals(size()) ? 2 * count() : count();
		}

		public int segmentOffset() {
			return info.getOffset();
		}

		public int segmentEnd() {
			return segmentOffset() + byteLength();
		}

		public boolean isArray() {
			return count > 1;
		}

		public boolean isConst() {
			return getAccesses().map(VariableAccess::accessType).forAll(READ::equals);
		}

		@Override
		public int compareTo(Variable o) {
			if (info() == o.info()) {
				return o.byteLength() - byteLength();
			}
			return info().compareTo(o.info());
		}

		@Override
		public String toString() {
			String suffix = "";
			if (isArray())
				suffix = String.format("[%d]", count());
			return String.format("%s%s", getName(), suffix);
		}
	}

	public record VariableAccess(MemoryRef source, VariableAccessType accessType)
		implements DataBlockContent<VariableAccess> {

		public AddressInfo getSourceMethod() {
			return source.image().getMethodsFor(source.address()).getOrNull();
		}

		public Segment getSegment() {
			return source.getSegment();
		}

		@Override
		public int compare(VariableAccess o) {
			if (accessType().equals(o.accessType())) {
				return source().compareTo(o.source());
			}
			return accessType().ordinal() - o.accessType().ordinal();
		}

		@Override
		public String toString() {
			final AddressInfo method = getSourceMethod();
			return String.format("%s (%S)%s", source.address(), accessType.shortName(),
				method != null ? method.getMethodInfo().getName() : "unknown Method");
		}
	}

	public enum VariableAccessType {
		READ, WRITE, UPDATE;

		public static VariableAccessType from(boolean isRead, boolean isWrite) {
			return isRead ? isWrite ? UPDATE : READ : WRITE;
		}

		public String shortName() {
			return name().substring(0, 1);
		}
	}

	public enum VariableSize {
		BYTE("byte_"), WORD("word_"), BYTE_STR("bstr_"), WORD_STR("wstr_");

		private final String prefix;

		private VariableSize(String prefix) {
			this.prefix = prefix;
		}

		public String getPrefix() {
			return prefix;
		}
	}

	private static String prefix(VariableSize size, int count) {
		if (count == 1 && (size == BYTE_STR || size == WORD_STR)) {
			return size == BYTE_STR ? BYTE.getPrefix() : WORD.getPrefix();
		}
		return size.getPrefix();
	}
}
