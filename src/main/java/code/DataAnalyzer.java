package code;

import static assembly.Dereference.DereferenceType.BYTE_PTR;

import java.util.Comparator;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.Function2;
import io.vavr.Tuple2;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;
import io.vavr.collection.SortedMap;
import io.vavr.collection.SortedSet;

import assembly.Address16bit;
import assembly.Dereference;
import assembly.Instruction;
import code.AddressInfo.MemoryAccess;
import code.AddressInfo.MemoryRef;
import code.AddressInfo.StringParameter;
import code.AddressInfo.Variable;
import code.Segment.OffsetRange;

public class DataAnalyzer {

	private final Project project;

	public DataAnalyzer(Project project) {
		this.project = project;
	}

	public void analyze(@Nonnull AnalyzerMonitor monitor) {
		final Seq<Segment> segments = project.getImages().flatMap(Image::getSegments);

		monitor.taskStarted(AnalyzerTask.DATA_ANALYZER_CODE_MODIFICATIONS);
		segments.toJavaParallelStream().forEach(this::analyzeCodeModifications);
		monitor.taskEnded(AnalyzerTask.DATA_ANALYZER_CODE_MODIFICATIONS);

		monitor.taskStarted(AnalyzerTask.DATA_ANALYZER_DATA_ACCESS);
		segments.toJavaParallelStream().forEach(this::analyzeDataAccess);
		monitor.taskEnded(AnalyzerTask.DATA_ANALYZER_DATA_ACCESS);

		monitor.taskStarted(AnalyzerTask.DATA_ANALYZER_DATA_BLOCKS);
		segments.toJavaParallelStream().forEach(this::createDataBlocks);
		monitor.taskEnded(AnalyzerTask.DATA_ANALYZER_DATA_BLOCKS);
	}

	public void analyzeCodeModifications(Segment segment) {
		segment.getInstructions().toJavaParallelStream().forEach(inst -> analyzeCodeModifications(segment, inst));
	}

	public void analyzeCodeModifications(Segment segment, Instruction inst) {
		final Map<Segment, Set<OffsetRange>> codeMods = toCodeModRanges(segment, inst);
		if (codeMods.isEmpty())
			return;
		final AddressInfo info = segment.getInfo(inst.getAddress());
		info.setCodeModificator(true);
		codeMods.forEach((seg, ranges) -> addCodeMods(info, seg, ranges));
	}

	private static Map<Segment, Set<OffsetRange>> toCodeModRanges(Segment segment, Instruction inst) {
		final Dereference deref = inst.getDerefernce();
		if (deref != null && inst.isDerefWrite()) {
			final AddressInfo info = segment.getInfo(inst.getAddress());
			return info.getMemoryAccesses()
				.filter(ma -> ma.getSegment().hasInstruction(ma.getRange()))
				.groupBy(MemoryAccess::getSegment)
				.bimap(Function.identity(), ma -> ma.map(MemoryAccess::getRange));
		}
		if (inst.isStringOp() && inst.isWritingStringDestination()) {
			final AddressInfo info = segment.getInfo(inst.getAddress());
			return info.getStringParameter()
				.filter(param -> param.dstSegment().hasInstruction(param.dstRange()))
				.groupBy(StringParameter::dstSegment)
				.bimap(Function.identity(), sp -> sp.map(StringParameter::dstRange));
		}
		return API.Map();
	}

	private static void addCodeMods(AddressInfo source, Segment segment, Set<OffsetRange> ranges) {
		segment.addCodeModificator(source.getRef(), ranges.toSortedSet().foldLeft(API.Seq(), DataAnalyzer::combine));
	}

	private static Seq<OffsetRange> combine(Seq<OffsetRange> ranges, OffsetRange range) {
		if (ranges.isEmpty())
			return ranges.append(range);
		OffsetRange last = ranges.last();
		if (last.to() >= range.from() && last.from() <= range.to())
			return ranges.dropRight(1).append(new OffsetRange(last.from(), range.to()));
		return ranges.append(range);
	}

	@SuppressWarnings("unchecked")
	private static Map<Integer, SortedSet<OffsetRange>> toRefOffsets(Set<MemoryRef> refs, int elementSize) {
		return toOffsetRanges(
			(Map<Integer, Set<Address16bit>>) refs.map(MemoryRef::address).groupBy(Address16bit::getSegment),
			elementSize);
	}

	private static Map<Integer, SortedSet<OffsetRange>> toOffsetRanges(Map<Integer, Set<Address16bit>> addresses,
		int elementSize) {

		return addresses.keySet().toMap(Function.identity(), segment -> {
			SortedSet<OffsetRange> ranges = API.SortedSet();
			final SortedSet<Integer> offsets = addresses.apply(segment).map(Address16bit::getOffset).toSortedSet();
			int start = offsets.head();
			int end = -1;
			for (Integer i : offsets) {
				if (end == -1 || (end < i && i <= end + elementSize)) {
					end = i;
				} else {
					ranges = ranges.add(new OffsetRange(start, end));
					start = i;
					end = i;
				}
			}
			return ranges.add(new OffsetRange(start, end));
		});
	}

	private void analyzeDataAccess(Segment segment) {
		segment.getInstructions().toJavaParallelStream().forEach(inst -> analyzeDataAccess(segment, inst));
	}

	private void analyzeDataAccess(Segment segment, Instruction inst) {
		final AddressInfo instInfo = segment.getInfo(inst.getAddress());
		if (instInfo.isCodeModificator() && (!inst.isStringOp() || !inst.isUsingStringSource()))
			return;
		final Dereference deref = inst.getDerefernce();
		if (deref != null) {
			boolean isByte = BYTE_PTR.equals(deref.getType());
			if (deref.isStatic()) {
				handleStaticDeref(inst, instInfo, isByte);
			} else {
				handleDynamicDeref(inst, instInfo, deref, isByte);
			}
		} else if (inst.isStringOp()) {
			instInfo.getStringParameter().forEach(param -> {
				if (inst.isUsingStringSource()) {
					registerString(instInfo, param.src(), inst, true, param.count());
				}
				if (inst.isUsingStringDestination()) {
					registerString(instInfo, param.dst(), inst, inst.isReadingStringDestination(), param.count());
				}
			});
		}
	}

	private static void handleStaticDeref(Instruction inst, AddressInfo instInfo, boolean isByte) {
		instInfo.getMemoryAccesses().forEach(access -> {
			MemoryRef ref = access.ref();
			if (isByte) {
				final Variable v = ref.image()
					.getInfo(ref.address())
					.addByteAccess(instInfo.getRef(), inst.isDerefRead(), inst.isDerefWrite(), 1);
				instInfo.addVariableRef(v);
			} else {
				final Variable v = ref.image()
					.getInfo(ref.address())
					.addWordAccess(instInfo.getRef(), inst.isDerefRead(), inst.isDerefWrite(), 1);
				instInfo.addVariableRef(v);
			}
		});
	}

	private static void handleDynamicDeref(Instruction inst, AddressInfo instInfo, Dereference deref, boolean isByte) {
		instInfo.getMemoryAccesses().map(MemoryAccess::ref).groupBy(MemoryRef::image).forEach(imageAndRefs -> {
			toRefOffsets(imageAndRefs._2(), deref.getType().getSize().getByteCount()).forEach(rangesPerSegment -> {
				rangesPerSegment._2().forEach(range -> {
					final int byteCount = range.to() - range.from() + deref.getType().getSize().getByteCount();
					final Address16bit addr = new Address16bit(rangesPerSegment._1(), range.from());
					if (isByte) {
						final Variable v = imageAndRefs._1()
							.getInfo(addr)
							.addByteAccess(instInfo.getRef(), inst.isDerefRead(), inst.isDerefWrite(), byteCount);
						instInfo.addVariableRef(v);
					} else {
						final Variable v = imageAndRefs._1()
							.getInfo(addr)
							.addWordAccess(instInfo.getRef(), inst.isDerefRead(), inst.isDerefWrite(), byteCount >> 1);
						instInfo.addVariableRef(v);
					}
				});
			});
		});
	}

	private static void registerString(AddressInfo instInfo, MemoryRef to, Instruction inst, boolean isRead,
		int count) {

		if (Math.abs(count) != 1) {
			final Address16bit start = count < 0 ? to.address().relativeAddress(count) : to.address();
			final Address16bit end = count < 0 ? to.address() : to.address().relativeAddress(count);
			if (end.linearAddress() <= start.linearAddress()) {
				int sCount = end.getOffset();
				int eCount = 0x10000 - start.getOffset();
				MemoryRef startRef = new MemoryRef(to.image(), new Address16bit(end.getSegment(), 0));
				MemoryRef endRef = new MemoryRef(to.image(), start);
				register(instInfo, startRef, inst.isStringByteOp(), isRead, !isRead, sCount);
				register(instInfo, endRef, inst.isStringByteOp(), isRead, !isRead, eCount);
			} else {
				register(instInfo, to, inst.isStringByteOp(), isRead, !isRead, count);
			}
		} else {
			register(instInfo, to, inst.isStringByteOp(), isRead, !isRead, count);
		}
	}

	private static void register(AddressInfo instInfo, MemoryRef accessTo, boolean isByte, boolean isRead,
		boolean isWrite, int count) {

		final MemoryRef src = instInfo.getRef();
		final Address16bit target = count < 0 ? accessTo.address().relativeAddress(count) : accessTo.address();
		count = Math.abs(count);
		if (isByte) {
			Variable v = accessTo.image().getInfo(target).addByteStringAccess(src, isRead, isWrite, count);
			instInfo.addVariableRef(v);
		} else {
			Variable v = accessTo.image().getInfo(target).addWordStringAccess(src, isRead, isWrite, count);
			instInfo.addVariableRef(v);
		}
	}

	private void createDataBlocks(Segment segment) {
		final Seq<DataBlock> dataBlocks = segment.getInfos()
			.flatMap(AddressInfo::getVariables)
			.map(Variable::getDataBlock);
		final SortedMap<Integer, Seq<DataBlock>> largestBlocks = dataBlocks.groupBy(DataBlock::getStartOffset)
			.flatMap(t2 -> t2._2().maxBy(DataBlock::byteLength).toArray())
			.groupBy(DataBlock::byteLength)
			.toSortedMap(Comparator.reverseOrder(), Function.identity());
		final SortedMap<Integer, Seq<DataBlock>> blocksByLength = dataBlocks.groupBy(DataBlock::byteLength)
			.toSortedMap(Comparator.reverseOrder(), Function.identity());
		final SortedMap<Integer, DataBlock> wordBlocks = blocksByLength.getOrElse(2, API.Seq())
			.toSortedMap(DataBlock::getStartOffset, Function.identity());

		SortedSet<DataBlock> parentBlocks = API.SortedSet();
		SortedSet<DataBlock> parentBlocksLarger = API.SortedSet();
		for (Tuple2<Integer, Seq<DataBlock>> lengthAndBlock : largestBlocks) {
			final int length = lengthAndBlock._1();
			if (length != 1) {
				parentBlocksLarger = parentBlocks;
				for (DataBlock dataBlock : lengthAndBlock._2()) {
					if (!parentBlocksLarger.exists(getSubRangePredicate(dataBlock))) {
						parentBlocks = parentBlocks.add(dataBlock);
						blocksByLength.keySet().filter(i -> i < length).forEach(len -> {
							blocksByLength.getOrElse(len, API.Seq()).forEach(dataBlock::addSubRange);
						});
					}
				}
			} else {
				for (DataBlock dataBlock : lengthAndBlock._2()) {
					final int offset = dataBlock.getStartOffset();
					if (!parentBlocksLarger.exists(getSubRangePredicate(dataBlock))
						&& !wordBlocks.containsKey(offset - 1) && !wordBlocks.containsKey(offset)) {

						parentBlocks = parentBlocks.add(dataBlock);
					} else {
						wordBlocks.get(offset - 1).peek(wordBlock -> wordBlock.addSubRange(dataBlock));
						wordBlocks.get(offset).peek(wordBlock -> wordBlock.addSubRange(dataBlock));
					}
				}
			}
		}
		dataBlocks.forEach(DataBlock::updateChildren);
		parentBlocks.forEach(block -> segment.addDataBlock(block));
	}

	private Predicate<DataBlock> getSubRangePredicate(DataBlock child) {
		final Function2<DataBlock, DataBlock, Boolean> isSubRange = this::isSubRange;
		return isSubRange.apply(child)::apply;
	}

	private boolean isSubRange(DataBlock child, DataBlock parent) {
		return parent.isSubRange(child);
	}
}
