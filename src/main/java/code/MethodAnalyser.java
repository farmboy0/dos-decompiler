package code;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.Tuple2;
import io.vavr.collection.Queue;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;
import io.vavr.collection.SortedSet;
import io.vavr.control.Option;

import assembly.Address16bit;
import assembly.Instruction;
import code.AddressInfo.MemoryRef;

public class MethodAnalyser {
	private final Project project;

	public MethodAnalyser(Project project) {
		this.project = project;
	}

	public void analyze(@Nonnull AnalyzerMonitor monitor) {
		project.getImages()
			.flatMap(Image::getMethodinfos)
			.map(AddressInfo::getMethodInfo)
			.forEach(MethodInfo::reset);

		monitor.taskStarted(AnalyzerTask.METHOD_ANALYZER_FIND_CALLS);
		project.getImages().forEach(MethodAnalyser::analyzeJumpTargets);
		monitor.taskEnded(AnalyzerTask.METHOD_ANALYZER_FIND_CALLS);

		monitor.taskStarted(AnalyzerTask.METHOD_ANALYZER_METHOD_CONTENT);
		project.getImages().forEach(MethodAnalyser::analyzeMethods);
		monitor.taskEnded(AnalyzerTask.METHOD_ANALYZER_METHOD_CONTENT);

		monitor.taskStarted(AnalyzerTask.METHOD_ANALYZER_FIND_RET_CALLS);
		findRETMethodCalls();
		monitor.taskEnded(AnalyzerTask.METHOD_ANALYZER_FIND_RET_CALLS);
	}

	private static void analyzeJumpTargets(Image image) {
		image.getSegments()
			.flatMap(Segment::getInstructions)
			.filter(Instruction::isJump)
			.forEach(inst -> analyzeJumpTargets(image, inst));
	}

	private static void analyzeJumpTargets(Image image, Instruction inst) {
		image.getJumpTargets(inst).forEach(jumpTarget -> analyzeJumpTarget(image, inst, jumpTarget));
	}

	private static void analyzeJumpTarget(Image srcImage, Instruction srcInst, MemoryRef jumpTarget) {
		final Image targetImage = jumpTarget.image();
		final Address16bit address = jumpTarget.address();
		targetImage.getInfo(address).addJumpSource(srcImage, srcInst.getAddress());
		if (srcInst.isMethodCall()) {
			final String name = String.format("func_%s", address.asNameSuffix());
			targetImage.addMethod(address, name);
		}
	}

	private static void analyzeMethods(Image image) {
		image.getSegments().forEach(segment -> {
			segment.update();
			findBlocks(segment);
			findCalledMethods(segment);
		});
		image.updateMethods();
	}

	private static void findBlocks(Segment segment) {
		segment.getMethodInfos().forEach(MethodAnalyser::findBlocks);
	}

	private static void findBlocks(AddressInfo method) {
		SortedSet<Address16bit> foundBlocks = API.SortedSet();

		SortedSet<Address16bit> blockRefs = API.SortedSet(method.getAddress());
		while (!blockRefs.isEmpty()) {
			final Address16bit blockRef = blockRefs.get();
			blockRefs = blockRefs.remove(blockRef);

			SortedSet<Instruction> block = method.getSegment().getCodeAt(blockRef);
			if (!block.isEmpty()) {
				foundBlocks = foundBlocks.add(blockRef);

				final SortedSet<Address16bit> localJumpTargets = block.filter(Instruction::isJump)
					.filter(inst -> !inst.isMethodCall())
					.map(inst -> method.getSegment().getInfo(inst.getAddress()))
					.flatMap(AddressInfo::getJumpTargets)
					.map(MemoryRef::address);
				blockRefs = blockRefs.addAll(localJumpTargets);

				final Instruction last = block.last();
				if (!last.isBlockFinisher()) {
					blockRefs = blockRefs.add(last.getNextAddress());
				}

				blockRefs = blockRefs.removeAll(foundBlocks);
			}
		}

		method.getMethodInfo().setBlocks(foundBlocks);
	}

	private static void findCalledMethods(Segment segment) {
		segment.getMethodInfos().forEach(MethodAnalyser::findCalledMethods);
	}

	private static void findCalledMethods(AddressInfo method) {
		method.getInstructions().filter(Instruction::isMethodCall).forEach(inst -> findCalledMethods(method, inst));
	}

	private static void findCalledMethods(AddressInfo method, Instruction inst) {
		method.getSegment()
			.getInfo(inst.getAddress())
			.getJumpTargets()
			.map(MemoryRef::getInfo)
			.filter(AddressInfo::isMethodStart)
			.forEach(calledMethod -> addCaller(method, calledMethod, inst));
	}

	private static void addCaller(AddressInfo caller, AddressInfo calledMethod, Instruction inst) {
		calledMethod.getMethodInfo().addCaller(caller, inst);
		caller.getMethodInfo().addCalledMethod(calledMethod);
	}

	private void findRETMethodCalls() {
		findRETMethodCalls(Queue.ofAll(project.getImages().flatMap(Image::getMethodinfos)));
	}

	private void findRETMethodCalls(Queue<AddressInfo> uncheckedMethods) {
		findRETMethodCalls(uncheckedMethods.dequeueOption());
	}

	private void findRETMethodCalls(Option<Tuple2<AddressInfo, Queue<AddressInfo>>> next) {
		next.peek(t2 -> findRETMethodCalls(t2._2().enqueueAll(findRETMethodCalls(t2._1()))));
	}

	private Seq<AddressInfo> findRETMethodCalls(AddressInfo method) {
		method.getInstructions()
			.filter(Instruction::isMethodFinisher)
			.forEach(inst -> findJumpsToUnknownMethods(method.getImage(), inst)
				.forEach(jt -> newMethod(method.getImage(), inst, jt)));
		return API.Seq();
	}

	private Set<MemoryRef> findJumpsToUnknownMethods(Image instImage, Instruction inst) {
		return instImage.getJumpTargets(inst).filter(jt -> jt.image().getMethodsFor(jt.address()).isEmpty());
	}

	private void newMethod(Image instImage, Instruction inst, MemoryRef jt) {
		final Address16bit targetAddress = jt.address();
		final Image targetImage = jt.image();

		final String name = String.format("func_%s", targetAddress.asNameSuffix());
		final AddressInfo newMethod = targetImage.addMethod(targetAddress, name);
		newMethod.addJumpSource(instImage, inst.getAddress());
		newMethod.getSegment().update();

		findBlocks(newMethod);
		findCalledMethods(newMethod);

		final Set<AddressInfo> callers = instImage.getMethodsFor(inst.getAddress());
		callers.forEach(caller -> addCaller(caller, newMethod, inst));

		targetImage.updateMethods();
	}
}
