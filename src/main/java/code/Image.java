package code;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Objects;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.Tuple2;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;

import assembly.Address16bit;
import assembly.Instruction;
import code.AddressInfo.MemoryRef;
import code.imageref.DataFileRef;
import code.imageref.ImageRef;

public class Image implements Comparable<Image> {
	private final ImageRef ref;
	private final Address16bit start;

	private Segment[][] segments;

	private Map<Address16bit, Set<AddressInfo>> methods = API.Map();
	private Seq<AddressInfo> content = API.Seq();

	public Image(@Nonnull ImageRef ref, @Nonnull Address16bit start) {
		this.ref = ref;
		this.start = start;
	}

	public boolean hasLoadModule() {
		return getRef().hasLoadModule();
	}

	public InitialStateMap getInitialState() {
		if (!getRef().hasLoadModule())
			return new InitialStateMap(API.Map());
		return new InitialStateMap(API.LinkedMap( //
			"AX", "AL: 1st FCB drive#, AH: 2nd FCB drive#", //
			"BX,CX,DX,BP,SI,DI", "Undefined", //
			"CS:IP", getRef().getCodeEntry().toString(), //
			"SS:SP", getRef().getInitialStack().toString(), //
			"DS,ES", String.format("%04X (PSP Segment)", start.getSegment() - 16) //
		));

	}

	public ImageRef getRef() {
		return ref;
	}

	public Address16bit getStart() {
		return start;
	}

	public int getLength() {
		return getRef().getBuffer().limit();
	}

	public int getEnd() {
		return start.linearAddress() + getLength();
	}

	public Address16bit getCodeEntry() {
		return getRef().getCodeEntry();
	}

	public Address16bit getInitialStack() {
		return getRef().getInitialStack();
	}

	public ByteBuffer getBuffer() {
		return getRef().getBuffer().duplicate().order(ByteOrder.LITTLE_ENDIAN);
	}

	public Seq<AddressInfo> getContent() {
		return content;
	}

	public Seq<Segment> getSegments() {
		if (segments == null) {
			return API.Seq();
		}
		return API.Seq(segments).filter(Objects::nonNull).flatMap(API::Seq).filter(Objects::nonNull);
	}

	public Segment getSegment(int index) {
		return getSegments().get(index);
	}

	public Segment getSegment(Address16bit addr) {
		return getSegmentBySegment(addr.getSegment());
	}

	public Segment getSegmentBySegment(int segment) {
		ensureNotEmpty();
		final int key1 = segment >> 8;
		if (segments[key1] == null) {
			segments[key1] = new Segment[256];
		}
		final int key2 = segment & 0xFF;
		if (segments[key1][key2] == null) {
			segments[key1][key2] = new Segment(this, segment);
		}
		return segments[key1][key2];
	}

	public Seq<AddressInfo> getMethodinfos() {
		return getSegments().flatMap(Segment::getMethodInfos);
	}

	public AddressInfo getInfo(Address16bit target) {
		return getSegment(target).getInfo(target);
	}

	public Set<MemoryRef> getJumpTargets(Instruction inst) {
		return getInfo(inst.getAddress()).getJumpTargets();
	}

	public Instruction getInstruction(Address16bit addr) {
		if (segments == null) {
			return null;
		}
		final int key1 = addr.getSegment() >> 8;
		if (segments[key1] == null) {
			return null;
		}
		final int key2 = addr.getSegment() & 0xFF;
		if (segments[key1][key2] == null) {
			return null;
		}
		return segments[key1][key2].getInstruction(addr.getOffset());
	}

	public Set<AddressInfo> getMethodsFor(Address16bit addr) {
		return methods.getOrElse(addr, API.Set());
	}

	public boolean isEmpty() {
		return segments == null;
	}

	public Image ensureNotEmpty() {
		if (segments == null) {
			segments = new Segment[256][];
		}
		return this;
	}

	public void update() {
		getSegments().forEach(Segment::update);
	}

	public void updateSegmentOffsets() {
		getSegments().forEach(Segment::updateSegmentOffsets);
		this.content = getSegments().flatMap(Segment::getDataAndMethodInfos).sorted();
	}

	public void updateMethods() {
		this.methods = getMethodinfos().flatMap(info -> {
			return info.getInstructions().map(inst -> new Tuple2<>(inst.getAddress(), info));
		}).groupBy(Tuple2::_1).toMap(Tuple2::_1, t2 -> t2._2().map(Tuple2::_2).toSet());
	}

	public AddressInfo addMethod(Address16bit address, String methodSuffix) {
		AddressInfo info = getInfo(address);
		if (!info.isMethodStart()) {
			info.setMethodName(
				String.format("%s_%s", getRef().getName().toLowerCase().replace('.', '_'), methodSuffix));
		}
		return info;
	}

	public void addInstruction(Instruction inst) {
		getSegment(inst.getAddress()).addInstruction(inst);
	}

	@Override
	public int compareTo(Image o) {
		if (getStart().equals(o.getStart())) {
			if (getRef().getName().equals(o.getRef().getName())) {
				if (getRef() instanceof DataFileRef dRef && o.getRef() instanceof DataFileRef oRef) {
					return dRef.getStart() - oRef.getStart();
				}
				return 0;
			}
			return getRef().getName().compareTo(o.getRef().getName());
		}
		return getStart().compareTo(o.getStart());
	}

	@Override
	public int hashCode() {
		return Objects.hash(ref);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Image)) {
			return false;
		}
		Image other = (Image) obj;
		return Objects.equals(ref, other.ref);
	}

	@Override
	public String toString() {
		return String.format("[%s]%s", getStart(), getRef());
	}

	public static class InitialStateMap {
		private final Map<String, String> state;

		public InitialStateMap(Map<String, String> state) {
			this.state = state;
		}

		public Object[] getColumnNames() {
			return new Object[] { "Register", "Content" };
		}

		public Object[][] getState() {
			return state.toArray().map(t2 -> new Object[] { t2._1, t2._2 }).toJavaArray(Object[][]::new);
		}

		@Override
		public String toString() {
			return "Initial register state";
		}
	}
}
