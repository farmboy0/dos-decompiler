package assembly;

public enum OpcodeType {
	SEGMENT_OVERRIDE_CS(0, Register.CS), //
	SEGMENT_OVERRIDE_DS(0, Register.DS), //
	SEGMENT_OVERRIDE_ES(0, Register.ES), //
	SEGMENT_OVERRIDE_FS(0, Register.FS), //
	SEGMENT_OVERRIDE_GS(0, Register.GS), //
	SEGMENT_OVERRIDE_SS(0, Register.SS), //
	STRING_REPETITION(1), //
	SIZE_OVERRIDE(2), //
	ADDRESS_OVERRIDE(3), //
	LOCK_ASSERTION(4), //
	NORMAL_INSTRUCTION(), //
	ANNOTATED_INSTRUCTION(), //
	;

	private static final int PREFIX_INDEX_NO_PREFIX = -1;

	private final int prefixIndex;
	private final Register segmentOverride;

	OpcodeType() {
		this(PREFIX_INDEX_NO_PREFIX, null);
	}

	OpcodeType(int prefixIndex) {
		this(prefixIndex, null);
	}

	OpcodeType(int prefixIndex, Register segmentOverride) {
		this.prefixIndex = prefixIndex;
		this.segmentOverride = segmentOverride;
	}

	public boolean isPrefix() {
		return prefixIndex != PREFIX_INDEX_NO_PREFIX;
	}

	public int getPrefixIndex() {
		return prefixIndex;
	}

	public Register getSegmentOverride() {
		return segmentOverride;
	}

	public boolean isStringRepetition() {
		return STRING_REPETITION.equals(this);
	}

	public boolean isSegmentOverride() {
		return segmentOverride != null;
	}
}
