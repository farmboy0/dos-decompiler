package assembly;

import java.util.Optional;

import io.vavr.collection.Array;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;

public class ModRM {
	private final AddressingMode mode;
	private final int modifier;
	private final RegisterMode rm;

	private ModRM(AddressingMode mode, int modifier, RegisterMode rm) {
		this.mode = mode;
		this.modifier = modifier;
		this.rm = rm;
	}

	public static ModRM from(byte value) {
		return new ModRM(AddressingMode.from(value), value >> 3 & 0x7, RegisterMode.from(value));
	}

	public AddressingMode getAdrMode() {
		return mode;
	}

	public OpcodeGroup getAsGroupId() {
		return OpcodeGroup.values()[modifier];
	}

	public int getAsRegisterModifier() {
		return modifier;
	}

	public RegisterMode getRegMode() {
		return rm;
	}

	enum AddressingMode {
		DISP_NONE(0x00, null), //
		DISP_8BIT(0x40, ArgumentSize._8bit), //
		DISP_16BIT(0x80, ArgumentSize._16bit), //
		REGISTER(0xC0, null), //
		;

		private final int code;
		private final ArgumentSize displacementSize;

		AddressingMode(int code, ArgumentSize displacementSize) {
			this.code = code;
			this.displacementSize = displacementSize;
		}

		public Optional<ArgumentSize> getDisplacementSize() {
			return Optional.ofNullable(displacementSize);
		}

		public int readDisplacement(ByteSource src) {
			return getDisplacementSize().map(size -> size.readSigned(src)).orElse(0);
		}

		public static AddressingMode from(byte value) {
			return Array.of(values()).find(am -> am.code == (value & 0xC0)).getOrNull();
		}
	}

	enum RegisterMode {
		EAX(0x0, Register.DS, Register.BX, Register.SI, Register.AL, Register.AX), //
		ECX(0x1, Register.DS, Register.BX, Register.DI, Register.CL, Register.CX), //
		EDX(0x2, Register.SS, Register.BP, Register.SI, Register.DL, Register.DX), //
		EBX(0x3, Register.SS, Register.BP, Register.DI, Register.BL, Register.BX), //
		ESP(0x4, Register.DS, null/*   */, Register.SI, Register.AH, Register.SP), //
		EBP(0x5, Register.DS, null/*   */, Register.DI, Register.CH, Register.BP), //
		ESI(0x6, Register.SS, Register.BP, null/*   */, Register.DH, Register.SI), //
		EDI(0x7, Register.DS, Register.BX, null/*   */, Register.BH, Register.DI), //
		;

		private final int code;
		private final Register segment;
		private final Register base;
		private final Register index;
		private final Map<ArgumentSize, Register> registers;

		RegisterMode(int code, Register segment, Register base, Register index, Register reg8bit, Register reg16bit) {
			this.code = code;
			this.segment = segment;
			this.base = base;
			this.index = index;
			this.registers = HashMap.of(ArgumentSize._8bit, reg8bit, ArgumentSize._16bit, reg16bit);
		}

		public Register getSegment() {
			return segment;
		}

		public Optional<Register> getBase() {
			return Optional.ofNullable(base);
		}

		public Optional<Register> getIndex() {
			return Optional.ofNullable(index);
		}

		public Register getRegister(ArgumentSize size) {
			return registers.getOrElse(size, null);
		}

		public static RegisterMode from(byte value) {
			return Array.of(values()).find(rm -> rm.code == (value & 0x7)).getOrNull();
		}
	}
}
