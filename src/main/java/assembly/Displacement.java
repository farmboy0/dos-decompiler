package assembly;

import java.util.Objects;

/**
 * A relative displacement to an instruction.
 *
 * @author eho
 */
public class Displacement implements Argument {

	private final int value;
	private final ArgumentSize size;

	public Displacement(int value, ArgumentSize size) {
		this.value = value;
		this.size = size;
	}

	public int getValue() {
		return value;
	}

	public ArgumentSize getSize() {
		return size;
	}

	@Override
	public int hashCode() {
		return Objects.hash(value, size);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Displacement)) {
			return false;
		}
		Displacement other = (Displacement) obj;
		return value == other.value && size == other.size;
	}

	@Override
	public String toString() {
		return getSize().format(getValue());
	}
}
