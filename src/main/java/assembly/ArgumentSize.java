package assembly;

import java.util.function.IntFunction;
import java.util.function.ToIntFunction;

import javax.annotation.Nonnull;

public enum ArgumentSize {
	_8bit(1, ByteSource::signedByte, ByteSource::unsignedByte, i -> String.format("0x%02X", i & 0xFF)), //
	_16bit(2, ByteSource::signedWord, ByteSource::unsignedWord, i -> String.format("0x%04X", i & 0xFFFF)), //
	;

	private final int byteCount;
	private final ToIntFunction<ByteSource> signedReader;
	private final ToIntFunction<ByteSource> unsignedReader;
	private final IntFunction<String> formatter;

	ArgumentSize(int byteCount, ToIntFunction<ByteSource> signedReader, ToIntFunction<ByteSource> unsignedReader,
		IntFunction<String> formatter) {

		this.byteCount = byteCount;
		this.signedReader = signedReader;
		this.unsignedReader = unsignedReader;
		this.formatter = formatter;
	}

	public int getByteCount() {
		return byteCount;
	}

	public int readSigned(@Nonnull ByteSource src) {
		return signedReader.applyAsInt(src);
	}

	public int readUnsigned(@Nonnull ByteSource src) {
		return unsignedReader.applyAsInt(src);
	}

	public String format(int value) {
		return formatter.apply(value);
	}
}
