package assembly;

public enum Mnemonics {
	AAA("AAA"), //
	AAD("AAD"), //
	AAM("AAM"), //
	AAS("AAS"), //
	ADC("ADC", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	ADD("ADD", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	ADDROVR(""), //
	AND("AND", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	ARPL("ARPL", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	BOUND("BOUND", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_BOTH), //
	BT("BT"), //
	BTC("BTC"), //
	BTR("BTR"), //
	BTS("BTS"), //
	CALL("CALL", MnemonicsType.METHOD_CALL, MnemonicsType.ARGUMENT_READ), //
	CALLBACK("DBCB"), //
	CBW("CBW"), //
	CLC("CLC"), //
	CLD("CLD"), //
	CLI("CLI"), //
	CMC("CMC"), //
	CMP("CMP", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_BOTH), //
	CMPSB("CMPSB", MnemonicsType.STRING_BYTE, MnemonicsType.ARGUMENTS_READ_BOTH), //
	CMPSW("CMPSW", MnemonicsType.STRING_WORD, MnemonicsType.ARGUMENTS_READ_BOTH), //
	CPUID("CPUID"), //
	CS_("CS:", MnemonicsType.PREFIX), //
	CWD("CWD"), //
	DAA("DAA"), //
	DAS("DAS"), //
	DEC("DEC", MnemonicsType.NONE, MnemonicsType.ARGUMENT_UPDATE), //
	DIV("DIV", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	DS_("DS:", MnemonicsType.PREFIX), //
	ENTER("ENTER"), //
	ES_("ES:", MnemonicsType.PREFIX), //
	FS_("FS:", MnemonicsType.PREFIX), //
	GS_("GS:", MnemonicsType.PREFIX), //
	HLT("HLT"), //
	IDIV("IDIV", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	IMUL("IMUL", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	IN("IN"), //
	INC("INC", MnemonicsType.NONE, MnemonicsType.ARGUMENT_UPDATE), //
	INSB("INSB", MnemonicsType.STRING_BYTE, MnemonicsType.ARGUMENTS_WRITE_DST), //
	INSW("INSW", MnemonicsType.STRING_WORD, MnemonicsType.ARGUMENTS_WRITE_DST), //
	INT("INT", MnemonicsType.INTERRUPT), //
	INTO("INTO", MnemonicsType.INTERRUPT), //
	IRET("IRET", MnemonicsType.INTERRUPT_RETURN), //
	JA("JA", MnemonicsType.COND_JUMP), //
	JB("JB", MnemonicsType.COND_JUMP), //
	JBE("JBE", MnemonicsType.COND_JUMP), //
	JCXZ("JCXZ", MnemonicsType.COND_JUMP), //
	JG("JG", MnemonicsType.COND_JUMP), //
	JGE("JGE", MnemonicsType.COND_JUMP), //
	JL("JL", MnemonicsType.COND_JUMP), //
	JLE("JLE", MnemonicsType.COND_JUMP), //
	JMP("JMP", MnemonicsType.UNCOND_JUMP, MnemonicsType.ARGUMENT_READ), //
	JNB("JNB", MnemonicsType.COND_JUMP), //
	JNO("JNO", MnemonicsType.COND_JUMP), //
	JNS("JNS", MnemonicsType.COND_JUMP), //
	JNZ("JNZ", MnemonicsType.COND_JUMP), //
	JO("JO", MnemonicsType.COND_JUMP), //
	JPE("JPE", MnemonicsType.COND_JUMP), //
	JPO("JPO", MnemonicsType.COND_JUMP), //
	JS("JS", MnemonicsType.COND_JUMP), //
	JZ("JZ", MnemonicsType.COND_JUMP), //
	LAHF("LAHF"), //
	LDS("LDS", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_WRITE), //
	LEA("LEA"), //
	LEAVE("LEAVE"), //
	LES("LES", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_WRITE), //
	LOCK("LOCK", MnemonicsType.PREFIX), //
	LODSB("LODSB", MnemonicsType.STRING_BYTE, MnemonicsType.ARGUMENTS_READ_SRC), //
	LODSW("LODSW", MnemonicsType.STRING_WORD, MnemonicsType.ARGUMENTS_READ_SRC), //
	LOOP("LOOP", MnemonicsType.COND_JUMP), //
	LOOPNZ("LOOPNZ", MnemonicsType.COND_JUMP), //
	LOOPZ("LOOPZ", MnemonicsType.COND_JUMP), //
	MOV("MOV", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_WRITE), //
	MOVSB("MOVSB", MnemonicsType.STRING_BYTE, MnemonicsType.ARGUMENTS_READ_WRITE), //
	MOVSW("MOVSW", MnemonicsType.STRING_WORD, MnemonicsType.ARGUMENTS_READ_WRITE), //
	MOVSX("MOVSX", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_WRITE), //
	MOVZX("MOVZX", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_WRITE), //
	MUL("MUL", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	NEG("NEG", MnemonicsType.NONE, MnemonicsType.ARGUMENT_UPDATE), //
	NOP("NOP"), //
	NOT("NOT", MnemonicsType.NONE, MnemonicsType.ARGUMENT_UPDATE), //
	OR("OR", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	OUT("OUT"), //
	OUTSB("OUTSB", MnemonicsType.STRING_BYTE, MnemonicsType.ARGUMENTS_READ_SRC), //
	OUTSW("OUTSW", MnemonicsType.STRING_WORD, MnemonicsType.ARGUMENTS_READ_SRC), //
	POP("POP", MnemonicsType.NONE, MnemonicsType.ARGUMENT_WRITE), //
	POPA("POPA"), //
	POPF("POPF"), //
	PUSH("PUSH", MnemonicsType.NONE, MnemonicsType.ARGUMENT_READ), //
	PUSHA("PUSHA"), //
	PUSHF("PUSHF"), //
	RCL("RCL", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	RCR("RCR", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	REP("REP", MnemonicsType.PREFIX), //
	REPNZ("REPNZ", MnemonicsType.PREFIX), //
	REPZ("REPZ", MnemonicsType.PREFIX), //
	RET("RET", MnemonicsType.METHOD_RETURN), //
	RETF("RETF", MnemonicsType.METHOD_RETURN), //
	ROL("ROL", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	ROR("ROR", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	RSM("RSM"), //
	SAHF("SAHF"), //
	SALC("SALC"), //
	SAR("SAR", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	SBB("SBB", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	SCASB("SCASB", MnemonicsType.STRING_BYTE, MnemonicsType.ARGUMENTS_READ_DST), //
	SCASW("SCASW", MnemonicsType.STRING_WORD, MnemonicsType.ARGUMENTS_READ_DST), //
	SETA("SETA"), //
	SETB("SETB"), //
	SETBE("SETBE"), //
	SETG("SETG"), //
	SETGE("SETGE"), //
	SETL("SETL"), //
	SETLE("SETLE"), //
	SETNB("SETNB"), //
	SETNO("SETNO"), //
	SETNS("SETNS"), //
	SETNZ("SETNZ"), //
	SETO("SETO"), //
	SETPE("SETPE"), //
	SETPO("SETPO"), //
	SETS("SETS"), //
	SETZ("SETZ"), //
	SHL("SHL", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	SHLD("SHLD", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	SHR("SHR", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	SHRD("SHRD", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	SIZEOVR("", MnemonicsType.PREFIX), //
	SS_("SS:", MnemonicsType.PREFIX), //
	STC("STC"), //
	STD("STD"), //
	STI("STI"), //
	STOSB("STOSB", MnemonicsType.STRING_BYTE, MnemonicsType.ARGUMENTS_WRITE_DST), //
	STOSW("STOSW", MnemonicsType.STRING_WORD, MnemonicsType.ARGUMENTS_WRITE_DST), //
	SUB("SUB", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	TEST("TEST", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_BOTH), //
	WAIT("WAIT"), //
	XCHG("XCHG", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_UPDATE_BOTH), //
	XLAT("XLAT"), //
	XOR("XOR", MnemonicsType.NONE, MnemonicsType.ARGUMENTS_READ_UPDATE), //
	;

	private final String name;
	private final MnemonicsType type;
	private final MnemonicsType argumentsType;

	Mnemonics(String name) {
		this(name, MnemonicsType.NONE);
	}

	Mnemonics(String name, MnemonicsType type) {
		this(name, type, MnemonicsType.NONE);
	}

	Mnemonics(String name, MnemonicsType type, MnemonicsType argumentsType) {
		this.name = name;
		this.type = type;
		this.argumentsType = argumentsType;
	}

	public String getName() {
		return name;
	}

	public MnemonicsType getType() {
		return type;
	}

	public MnemonicsType getArgumentsType() {
		return argumentsType;
	}

	@Override
	public String toString() {
		return getName();
	}
}
