package assembly;

import static assembly.ArgumentTypeEnum.ADR;
import static assembly.ArgumentTypeEnum.DISB;
import static assembly.ArgumentTypeEnum.DISV;
import static assembly.ArgumentTypeEnum.EAX;
import static assembly.ArgumentTypeEnum.EBP;
import static assembly.ArgumentTypeEnum.EBX;
import static assembly.ArgumentTypeEnum.ECX;
import static assembly.ArgumentTypeEnum.EDI;
import static assembly.ArgumentTypeEnum.EDX;
import static assembly.ArgumentTypeEnum.ESI;
import static assembly.ArgumentTypeEnum.ESP;
import static assembly.ArgumentTypeEnum.INTB;
import static assembly.ArgumentTypeEnum.INTV;
import static assembly.ArgumentTypeEnum.INTW;
import static assembly.ArgumentTypeEnum.MEMD;
import static assembly.ArgumentTypeEnum.MEMV;
import static assembly.ArgumentTypeEnum.ONE;
import static assembly.ArgumentTypeEnum.REFB;
import static assembly.ArgumentTypeEnum.REFV;
import static assembly.ArgumentTypeEnum.REGB;
import static assembly.ArgumentTypeEnum.REGD;
import static assembly.ArgumentTypeEnum.REGS;
import static assembly.ArgumentTypeEnum.REGV;
import static assembly.ArgumentTypeEnum.RMB;
import static assembly.ArgumentTypeEnum.RMV;
import static assembly.ArgumentTypeEnum.RMW;
import static assembly.ArgumentTypeEnum.THREE;
import static assembly.OpcodeGroup.GRP0;
import static assembly.OpcodeGroup.GRP1;
import static assembly.OpcodeGroup.GRP2;
import static assembly.OpcodeGroup.GRP3;
import static assembly.OpcodeGroup.GRP4;
import static assembly.OpcodeGroup.GRP5;
import static assembly.OpcodeGroup.GRP6;
import static assembly.OpcodeGroup.GRP7;
import static assembly.OpcodeType.ANNOTATED_INSTRUCTION;
import static assembly.OpcodeType.LOCK_ASSERTION;
import static assembly.OpcodeType.NORMAL_INSTRUCTION;
import static assembly.OpcodeType.SEGMENT_OVERRIDE_CS;
import static assembly.OpcodeType.SEGMENT_OVERRIDE_DS;
import static assembly.OpcodeType.SEGMENT_OVERRIDE_ES;
import static assembly.OpcodeType.SEGMENT_OVERRIDE_FS;
import static assembly.OpcodeType.SEGMENT_OVERRIDE_GS;
import static assembly.OpcodeType.SEGMENT_OVERRIDE_SS;
import static assembly.OpcodeType.STRING_REPETITION;

import io.vavr.collection.Array;
import io.vavr.collection.IndexedSeq;

public enum Opcode {
	ADD00(0x00, Mnemonics.ADD, RMB, REGB), //
	ADD01(0x01, Mnemonics.ADD, RMV, REGV), //
	ADD02(0x02, Mnemonics.ADD, REGB, RMB), //
	ADD03(0x03, Mnemonics.ADD, REGV, RMV), //
	ADD04(0x04, Mnemonics.ADD, Register.AL, INTB), //
	ADD05(0x05, Mnemonics.ADD, EAX, INTV), //
	PUSH06(0x06, Mnemonics.PUSH, Register.ES), //
	POP07(0x07, Mnemonics.POP, Register.ES), //
	OR08(0x08, Mnemonics.OR, RMB, REGB), //
	OR09(0x09, Mnemonics.OR, RMV, REGV), //
	OR0A(0x0A, Mnemonics.OR, REGB, RMB), //
	OR0B(0x0B, Mnemonics.OR, REGV, RMV), //
	OR0C(0x0C, Mnemonics.OR, Register.AL, INTB), //
	OR0D(0x0D, Mnemonics.OR, EAX, INTV), //
	PUSH0E(0x0E, Mnemonics.PUSH, Register.CS), //

	JO0F80(0x0F, 0x80, Mnemonics.JO, DISV), //
	JNO0F81(0x0F, 0x81, Mnemonics.JNO, DISV), //
	JB0F82(0x0F, 0x82, Mnemonics.JB, DISV), //
	JNB0F83(0x0F, 0x83, Mnemonics.JNB, DISV), //
	JZ0F84(0x0F, 0x84, Mnemonics.JZ, DISV), //
	JNZ0F85(0x0F, 0x85, Mnemonics.JNZ, DISV), //
	JBE0F86(0x0F, 0x86, Mnemonics.JBE, DISV), //
	JA0F87(0x0F, 0x87, Mnemonics.JA, DISV), //
	JS0F88(0x0F, 0x88, Mnemonics.JS, DISV), //
	JNS0F89(0x0F, 0x89, Mnemonics.JNS, DISV), //
	JPE0F8A(0x0F, 0x8A, Mnemonics.JPE, DISV), //
	JPO0F8B(0x0F, 0x8B, Mnemonics.JPO, DISV), //
	JL0F8C(0x0F, 0x8C, Mnemonics.JL, DISV), //
	JGE0F8D(0x0F, 0x8D, Mnemonics.JGE, DISV), //
	JLE0F8E(0x0F, 0x8E, Mnemonics.JLE, DISV), //
	JG0F8F(0x0F, 0x8F, Mnemonics.JG, DISV), //

	SETO0F90(0x0F, 0x90, Mnemonics.SETO, RMB), //
	SETNO0F91(0x0F, 0x91, Mnemonics.SETNO, RMB), //
	SETB0F92(0x0F, 0x92, Mnemonics.SETB, RMB), //
	SETNB0F93(0x0F, 0x93, Mnemonics.SETNB, RMB), //
	SETZ0F94(0x0F, 0x94, Mnemonics.SETZ, RMB), //
	SETNZ0F95(0x0F, 0x95, Mnemonics.SETNZ, RMB), //
	SETBE0F96(0x0F, 0x96, Mnemonics.SETBE, RMB), //
	SETA0F97(0x0F, 0x97, Mnemonics.SETA, RMB), //
	SETS0F98(0x0F, 0x98, Mnemonics.SETS, RMB), //
	SETNS0F99(0x0F, 0x99, Mnemonics.SETNS, RMB), //
	SETPE0F9A(0x0F, 0x9A, Mnemonics.SETPE, RMB), //
	SETPO0F9B(0x0F, 0x9B, Mnemonics.SETPO, RMB), //
	SETL0F9C(0x0F, 0x9C, Mnemonics.SETL, RMB), //
	SETGE0F9D(0x0F, 0x9D, Mnemonics.SETGE, RMB), //
	SETLE0F9E(0x0F, 0x9E, Mnemonics.SETLE, RMB), //
	SETG0F9F(0x0F, 0x9F, Mnemonics.SETG, RMB), //

	PUSH0FA0(0x0F, 0xA0, Mnemonics.PUSH, Register.FS), //
	POP0FA1(0x0F, 0xA1, Mnemonics.POP, Register.FS), //
	CPUID0FA2(0x0F, 0xA2, Mnemonics.CPUID), //
	BT0FA3(0x0F, 0xA3, Mnemonics.BT, RMV, REGV), //
	SHLD0FA4(0x0F, 0xA4, Mnemonics.SHLD, RMV, REGV, INTB), //
	SHLD0FA5(0x0F, 0xA5, Mnemonics.SHLD, RMV, REGV, Register.CL), //
	PUSH0FA8(0x0F, 0xA8, Mnemonics.PUSH, Register.GS), //
	POP0FA9(0x0F, 0xA9, Mnemonics.POP, Register.GS), //
	RSM0FAA(0x0F, 0xAA, Mnemonics.RSM), //
	BTS0FAB(0x0F, 0xAB, Mnemonics.BTS, RMV, REGV), //
	SHRD0FAC(0x0F, 0xAC, Mnemonics.SHRD, RMV, REGV, INTB), //
	SHRD0FAD(0x0F, 0xAD, Mnemonics.SHRD, RMV, REGV, Register.CL), //
	IMUL0FAF(0x0F, 0xAF, Mnemonics.IMUL, REGV, RMV), //

	BTR0FB3(0x0F, 0xB3, Mnemonics.BTR, RMV, REGV), //
	MOVZX0FB6(0x0F, 0xB6, Mnemonics.MOVZX, REGV, RMB), //
	MOVZX0FB7(0x0F, 0xB7, Mnemonics.MOVZX, REGD, RMW), //
	BT0FBA(0x0F, 0xBA, GRP4, Mnemonics.BT, RMV, INTB), //
	BTS0FBA(0x0F, 0xBA, GRP5, Mnemonics.BTS, RMV, INTB), //
	BTR0FBA(0x0F, 0xBA, GRP6, Mnemonics.BTR, RMV, INTB), //
	BTC0FBA(0x0F, 0xBA, GRP7, Mnemonics.BTC, RMV, INTB), //
	BTC0FBB(0x0F, 0xBB, Mnemonics.BTC, RMV, REGV), //
	MOVSX0FBE(0x0F, 0xBE, Mnemonics.MOVSX, REGV, RMB), //
	MOVSX0FBF(0x0F, 0xBF, Mnemonics.MOVSX, REGD, RMW), //

	ADC10(0x10, Mnemonics.ADC, RMB, REGB), //
	ADC11(0x11, Mnemonics.ADC, RMV, REGV), //
	ADC12(0x12, Mnemonics.ADC, REGB, RMB), //
	ADC13(0x13, Mnemonics.ADC, REGV, RMV), //
	ADC14(0x14, Mnemonics.ADC, Register.AL, INTB), //
	ADC15(0x15, Mnemonics.ADC, EAX, INTV), //
	PUSH16(0x16, Mnemonics.PUSH, Register.SS), //
	POP17(0x17, Mnemonics.POP, Register.SS), //
	SBB18(0x18, Mnemonics.SBB, RMB, REGB), //
	SBB19(0x19, Mnemonics.SBB, RMV, REGV), //
	SBB1A(0x1A, Mnemonics.SBB, REGB, RMB), //
	SBB1B(0x1B, Mnemonics.SBB, REGV, RMV), //
	SBB1C(0x1C, Mnemonics.SBB, Register.AL, INTB), //
	SBB1D(0x1D, Mnemonics.SBB, EAX, INTV), //
	PUSH1E(0x1E, Mnemonics.PUSH, Register.DS), //
	POP1F(0x1F, Mnemonics.POP, Register.DS), //
	AND20(0x20, Mnemonics.AND, RMB, REGB), //
	AND21(0x21, Mnemonics.AND, RMV, REGV), //
	AND22(0x22, Mnemonics.AND, REGB, RMB), //
	AND23(0x23, Mnemonics.AND, REGV, RMV), //
	AND24(0x24, Mnemonics.AND, Register.AL, INTB), //
	AND25(0x25, Mnemonics.AND, EAX, INTV), //

	ES_26(0x26, Mnemonics.ES_, SEGMENT_OVERRIDE_ES), //
	DAA27(0x27, Mnemonics.DAA), //

	SUB28(0x28, Mnemonics.SUB, RMB, REGB), //
	SUB29(0x29, Mnemonics.SUB, RMV, REGV), //
	SUB2A(0x2A, Mnemonics.SUB, REGB, RMB), //
	SUB2B(0x2B, Mnemonics.SUB, REGV, RMV), //
	SUB2C(0x2C, Mnemonics.SUB, Register.AL, INTB), //
	SUB2D(0x2D, Mnemonics.SUB, EAX, INTV), //

	CS_2E(0x2E, Mnemonics.CS_, SEGMENT_OVERRIDE_CS), //
	DAS2F(0x2F, Mnemonics.DAS), //

	XOR30(0x30, Mnemonics.XOR, RMB, REGB), //
	XOR31(0x31, Mnemonics.XOR, RMV, REGV), //
	XOR32(0x32, Mnemonics.XOR, REGB, RMB), //
	XOR33(0x33, Mnemonics.XOR, REGV, RMV), //
	XOR34(0x34, Mnemonics.XOR, Register.AL, INTB), //
	XOR35(0x35, Mnemonics.XOR, EAX, INTV), //

	SS_36(0x36, Mnemonics.SS_, SEGMENT_OVERRIDE_SS), //
	AAA37(0x37, Mnemonics.AAA), //

	CMP38(0x38, Mnemonics.CMP, RMB, REGB), //
	CMP39(0x39, Mnemonics.CMP, RMV, REGV), //
	CMP3A(0x3A, Mnemonics.CMP, REGB, RMB), //
	CMP3B(0x3B, Mnemonics.CMP, REGV, RMV), //
	CMP3C(0x3C, Mnemonics.CMP, Register.AL, INTB), //
	CMP3D(0x3D, Mnemonics.CMP, EAX, INTV), //

	DS_3E(0x3E, Mnemonics.DS_, SEGMENT_OVERRIDE_DS), //
	AAS3F(0x3F, Mnemonics.AAS), //

	INC40(0x40, Mnemonics.INC, EAX), //
	INC41(0x41, Mnemonics.INC, ECX), //
	INC42(0x42, Mnemonics.INC, EDX), //
	INC43(0x43, Mnemonics.INC, EBX), //
	INC44(0x44, Mnemonics.INC, ESP), //
	INC45(0x45, Mnemonics.INC, EBP), //
	INC46(0x46, Mnemonics.INC, ESI), //
	INC47(0x47, Mnemonics.INC, EDI), //

	DEC48(0x48, Mnemonics.DEC, EAX), //
	DEC49(0x49, Mnemonics.DEC, ECX), //
	DEC4A(0x4A, Mnemonics.DEC, EDX), //
	DEC4B(0x4B, Mnemonics.DEC, EBX), //
	DEC4C(0x4C, Mnemonics.DEC, ESP), //
	DEC4D(0x4D, Mnemonics.DEC, EBP), //
	DEC4E(0x4E, Mnemonics.DEC, ESI), //
	DEC4F(0x4F, Mnemonics.DEC, EDI), //

	PUSH50(0x50, Mnemonics.PUSH, EAX), //
	PUSH51(0x51, Mnemonics.PUSH, ECX), //
	PUSH52(0x52, Mnemonics.PUSH, EDX), //
	PUSH53(0x53, Mnemonics.PUSH, EBX), //
	PUSH54(0x54, Mnemonics.PUSH, ESP), //
	PUSH55(0x55, Mnemonics.PUSH, EBP), //
	PUSH56(0x56, Mnemonics.PUSH, ESI), //
	PUSH57(0x57, Mnemonics.PUSH, EDI), //

	POP58(0x58, Mnemonics.POP, EAX), //
	POP59(0x59, Mnemonics.POP, ECX), //
	POP5A(0x5A, Mnemonics.POP, EDX), //
	POP5B(0x5B, Mnemonics.POP, EBX), //
	POP5C(0x5C, Mnemonics.POP, ESP), //
	POP5D(0x5D, Mnemonics.POP, EBP), //
	POP5E(0x5E, Mnemonics.POP, ESI), //
	POP5F(0x5F, Mnemonics.POP, EDI), //

	PUSHA60(0x60, Mnemonics.PUSHA), //
	POPA61(0x61, Mnemonics.POPA), //
	BOUND62(0x62, Mnemonics.BOUND, REGV, RMV), //
	ARPL63(0x63, Mnemonics.ARPL, RMV, REGV), //

	FS_64(0x64, Mnemonics.FS_, SEGMENT_OVERRIDE_FS), //
	GS_65(0x65, Mnemonics.GS_, SEGMENT_OVERRIDE_GS), //

	SIZEOVR66(0x66, Mnemonics.SIZEOVR, OpcodeType.SIZE_OVERRIDE), //
	ADDROVR67(0x67, Mnemonics.ADDROVR, OpcodeType.ADDRESS_OVERRIDE), //

	PUSH68(0x68, Mnemonics.PUSH, INTW), //
	IMUL69(0x69, Mnemonics.IMUL, REGV, RMV, INTW), //
	PUSH6A(0x6A, Mnemonics.PUSH, INTB), //
	IMUL6B(0x6B, Mnemonics.IMUL, REGV, RMV, INTB), //

	INSB6C(0x6C, Mnemonics.INSB), //
	INSW6D(0x6D, Mnemonics.INSW), //
	OUTSB6E(0x6E, Mnemonics.OUTSB), //
	OUTSW6F(0x6F, Mnemonics.OUTSW), //

	JO70(0x70, Mnemonics.JO, DISB), //
	JNO71(0x71, Mnemonics.JNO, DISB), //
	JB72(0x72, Mnemonics.JB, DISB), //
	JNB73(0x73, Mnemonics.JNB, DISB), //
	JZ74(0x74, Mnemonics.JZ, DISB), //
	JNZ75(0x75, Mnemonics.JNZ, DISB), //
	JBE76(0x76, Mnemonics.JBE, DISB), //
	JA77(0x77, Mnemonics.JA, DISB), //
	JS78(0x78, Mnemonics.JS, DISB), //
	JNS79(0x79, Mnemonics.JNS, DISB), //
	JPE7A(0x7A, Mnemonics.JPE, DISB), //
	JPO7B(0x7B, Mnemonics.JPO, DISB), //
	JL7C(0x7C, Mnemonics.JL, DISB), //
	JGE7D(0x7D, Mnemonics.JGE, DISB), //
	JLE7E(0x7E, Mnemonics.JLE, DISB), //
	JG7F(0x7F, Mnemonics.JG, DISB), //

	ADD80(0x80, GRP0, Mnemonics.ADD, ANNOTATED_INSTRUCTION, RMB, INTB), //
	OR80(0x80, GRP1, Mnemonics.OR, ANNOTATED_INSTRUCTION, RMB, INTB), //
	ADC80(0x80, GRP2, Mnemonics.ADC, ANNOTATED_INSTRUCTION, RMB, INTB), //
	SBB80(0x80, GRP3, Mnemonics.SBB, ANNOTATED_INSTRUCTION, RMB, INTB), //
	AND80(0x80, GRP4, Mnemonics.AND, ANNOTATED_INSTRUCTION, RMB, INTB), //
	SUB80(0x80, GRP5, Mnemonics.SUB, ANNOTATED_INSTRUCTION, RMB, INTB), //
	XOR80(0x80, GRP6, Mnemonics.XOR, ANNOTATED_INSTRUCTION, RMB, INTB), //
	CMP80(0x80, GRP7, Mnemonics.CMP, ANNOTATED_INSTRUCTION, RMB, INTB), //

	ADD81(0x81, GRP0, Mnemonics.ADD, ANNOTATED_INSTRUCTION, RMV, INTV), //
	OR81(0x81, GRP1, Mnemonics.OR, ANNOTATED_INSTRUCTION, RMV, INTV), //
	ADC81(0x81, GRP2, Mnemonics.ADC, ANNOTATED_INSTRUCTION, RMV, INTV), //
	SBB81(0x81, GRP3, Mnemonics.SBB, ANNOTATED_INSTRUCTION, RMV, INTV), //
	AND81(0x81, GRP4, Mnemonics.AND, ANNOTATED_INSTRUCTION, RMV, INTV), //
	SUB81(0x81, GRP5, Mnemonics.SUB, ANNOTATED_INSTRUCTION, RMV, INTV), //
	XOR81(0x81, GRP6, Mnemonics.XOR, ANNOTATED_INSTRUCTION, RMV, INTV), //
	CMP81(0x81, GRP7, Mnemonics.CMP, ANNOTATED_INSTRUCTION, RMV, INTV), //

	ADD82(0x82, GRP0, Mnemonics.ADD, ANNOTATED_INSTRUCTION, RMB, INTB), //
	OR82(0x82, GRP1, Mnemonics.OR, ANNOTATED_INSTRUCTION, RMB, INTB), //
	ADC82(0x82, GRP2, Mnemonics.ADC, ANNOTATED_INSTRUCTION, RMB, INTB), //
	SBB82(0x82, GRP3, Mnemonics.SBB, ANNOTATED_INSTRUCTION, RMB, INTB), //
	AND82(0x82, GRP4, Mnemonics.AND, ANNOTATED_INSTRUCTION, RMB, INTB), //
	SUB82(0x82, GRP5, Mnemonics.SUB, ANNOTATED_INSTRUCTION, RMB, INTB), //
	XOR82(0x82, GRP6, Mnemonics.XOR, ANNOTATED_INSTRUCTION, RMB, INTB), //
	CMP82(0x82, GRP7, Mnemonics.CMP, ANNOTATED_INSTRUCTION, RMB, INTB), //

	ADD83(0x83, GRP0, Mnemonics.ADD, ANNOTATED_INSTRUCTION, RMV, INTB), //
	OR83(0x83, GRP1, Mnemonics.OR, ANNOTATED_INSTRUCTION, RMV, INTB), //
	ADC83(0x83, GRP2, Mnemonics.ADC, ANNOTATED_INSTRUCTION, RMV, INTB), //
	SBB83(0x83, GRP3, Mnemonics.SBB, ANNOTATED_INSTRUCTION, RMV, INTB), //
	AND83(0x83, GRP4, Mnemonics.AND, ANNOTATED_INSTRUCTION, RMV, INTB), //
	SUB83(0x83, GRP5, Mnemonics.SUB, ANNOTATED_INSTRUCTION, RMV, INTB), //
	XOR83(0x83, GRP6, Mnemonics.XOR, ANNOTATED_INSTRUCTION, RMV, INTB), //
	CMP83(0x83, GRP7, Mnemonics.CMP, ANNOTATED_INSTRUCTION, RMV, INTB), //

	TEST84(0x84, Mnemonics.TEST, REGB, RMB), //
	TEST85(0x85, Mnemonics.TEST, REGV, RMV), //
	XCHG86(0x86, Mnemonics.XCHG, REGB, RMB), //
	XCHG87(0x87, Mnemonics.XCHG, REGV, RMV), //
	MOV88(0x88, Mnemonics.MOV, RMB, REGB), //
	MOV89(0x89, Mnemonics.MOV, RMV, REGV), //
	MOV8A(0x8A, Mnemonics.MOV, REGB, RMB), //
	MOV8B(0x8B, Mnemonics.MOV, REGV, RMV), //
	MOV8C(0x8C, Mnemonics.MOV, RMW, REGS), //
	LEA8D(0x8D, Mnemonics.LEA, REGV, MEMV), //
	MOV8E(0x8E, Mnemonics.MOV, REGS, RMW), //
	POP8F(0x8F, Mnemonics.POP, RMV), //

	NOP90(0x90, Mnemonics.NOP), //

	XCHG91(0x91, Mnemonics.XCHG, ECX, EAX), //
	XCHG92(0x92, Mnemonics.XCHG, EDX, EAX), //
	XCHG93(0x93, Mnemonics.XCHG, EBX, EAX), //
	XCHG94(0x94, Mnemonics.XCHG, ESP, EAX), //
	XCHG95(0x95, Mnemonics.XCHG, EBP, EAX), //
	XCHG96(0x96, Mnemonics.XCHG, ESI, EAX), //
	XCHG97(0x97, Mnemonics.XCHG, EDI, EAX), //

	CBW98(0x98, Mnemonics.CBW), //
	CWD99(0x99, Mnemonics.CWD), //

	CALL9A(0x9A, Mnemonics.CALL, ANNOTATED_INSTRUCTION, ADR), //

	WAIT9B(0x9B, Mnemonics.WAIT), //
	PUSHF9C(0x9C, Mnemonics.PUSHF), //
	POPF9D(0x9D, Mnemonics.POPF), //
	SAHF9E(0x9E, Mnemonics.SAHF), //
	LAHF9F(0x9F, Mnemonics.LAHF), //

	MOVA0(0xA0, Mnemonics.MOV, Register.AL, REFB), //
	MOVA1(0xA1, Mnemonics.MOV, EAX, REFV), //
	MOVA2(0xA2, Mnemonics.MOV, REFB, Register.AL), //
	MOVA3(0xA3, Mnemonics.MOV, REFV, EAX), //

	MOVSBA4(0xA4, Mnemonics.MOVSB), //
	MOVSWA5(0xA5, Mnemonics.MOVSW), //
	CMPSBA6(0xA6, Mnemonics.CMPSB), //
	CMPSWA7(0xA7, Mnemonics.CMPSW), //

	TESTA8(0xA8, Mnemonics.TEST, Register.AL, INTB), //
	TESTA9(0xA9, Mnemonics.TEST, EAX, INTV), //

	STOSBAA(0xAA, Mnemonics.STOSB), //
	STOSWAB(0xAB, Mnemonics.STOSW), //
	LODSBAC(0xAC, Mnemonics.LODSB), //
	LODSWAD(0xAD, Mnemonics.LODSW), //
	SCASBAE(0xAE, Mnemonics.SCASB), //
	SCASWAF(0xAF, Mnemonics.SCASW), //

	MOVB0(0xB0, Mnemonics.MOV, Register.AL, INTB), //
	MOVB1(0xB1, Mnemonics.MOV, Register.CL, INTB), //
	MOVB2(0xB2, Mnemonics.MOV, Register.DL, INTB), //
	MOVB3(0xB3, Mnemonics.MOV, Register.BL, INTB), //
	MOVB4(0xB4, Mnemonics.MOV, Register.AH, INTB), //
	MOVB5(0xB5, Mnemonics.MOV, Register.CH, INTB), //
	MOVB6(0xB6, Mnemonics.MOV, Register.DH, INTB), //
	MOVB7(0xB7, Mnemonics.MOV, Register.BH, INTB), //
	MOVB8(0xB8, Mnemonics.MOV, EAX, INTV), //
	MOVB9(0xB9, Mnemonics.MOV, ECX, INTV), //
	MOVBA(0xBA, Mnemonics.MOV, EDX, INTV), //
	MOVBB(0xBB, Mnemonics.MOV, EBX, INTV), //
	MOVBC(0xBC, Mnemonics.MOV, ESP, INTV), //
	MOVBD(0xBD, Mnemonics.MOV, EBP, INTV), //
	MOVBE(0xBE, Mnemonics.MOV, ESI, INTV), //
	MOVBF(0xBF, Mnemonics.MOV, EDI, INTV), //

	ROLC0(0xC0, GRP0, Mnemonics.ROL, RMB, INTB), //
	RORC0(0xC0, GRP1, Mnemonics.ROR, RMB, INTB), //
	RCLC0(0xC0, GRP2, Mnemonics.RCL, RMB, INTB), //
	RCRC0(0xC0, GRP3, Mnemonics.RCR, RMB, INTB), //
	SHLC0(0xC0, GRP4, Mnemonics.SHL, RMB, INTB), //
	SHRC0(0xC0, GRP5, Mnemonics.SHR, RMB, INTB), //
	SARC0(0xC0, GRP7, Mnemonics.SAR, RMB, INTB), //

	ROLC1(0xC1, GRP0, Mnemonics.ROL, RMW, INTB), //
	RORC1(0xC1, GRP1, Mnemonics.ROR, RMW, INTB), //
	RCLC1(0xC1, GRP2, Mnemonics.RCL, RMW, INTB), //
	RCRC1(0xC1, GRP3, Mnemonics.RCR, RMW, INTB), //
	SHLC1(0xC1, GRP4, Mnemonics.SHL, RMW, INTB), //
	SHRC1(0xC1, GRP5, Mnemonics.SHR, RMW, INTB), //
	SARC1(0xC1, GRP7, Mnemonics.SAR, RMW, INTB), //

	RETC2(0xC2, Mnemonics.RET, INTW), //
	RETC3(0xC3, Mnemonics.RET), //
	LESC4(0xC4, Mnemonics.LES, REGV, MEMD), //
	LDSC5(0xC5, Mnemonics.LDS, REGV, MEMD), //
	MOVC6(0xC6, Mnemonics.MOV, ANNOTATED_INSTRUCTION, RMB, INTB), //
	MOVC7(0xC7, Mnemonics.MOV, ANNOTATED_INSTRUCTION, RMV, INTV), //

	ENTERC8(0xC8, Mnemonics.ENTER, INTW, INTB), //
	LEAVEC9(0xC9, Mnemonics.LEAVE), //
	RETFCA(0xCA, Mnemonics.RETF, INTW), //
	RETFCB(0xCB, Mnemonics.RETF), //
	INT3CC(0xCC, Mnemonics.INT, THREE), //
	INTCD(0xCD, Mnemonics.INT, INTB), //
	INTOCE(0xCE, Mnemonics.INTO), //
	IRETCF(0xCF, Mnemonics.IRET), //

	ROLD0(0xD0, GRP0, Mnemonics.ROL, ANNOTATED_INSTRUCTION, RMB, ONE), //
	RORD0(0xD0, GRP1, Mnemonics.ROR, ANNOTATED_INSTRUCTION, RMB, ONE), //
	RCLD0(0xD0, GRP2, Mnemonics.RCL, ANNOTATED_INSTRUCTION, RMB, ONE), //
	RCRD0(0xD0, GRP3, Mnemonics.RCR, ANNOTATED_INSTRUCTION, RMB, ONE), //
	SHLD0(0xD0, GRP4, Mnemonics.SHL, ANNOTATED_INSTRUCTION, RMB, ONE), //
	SHRD0(0xD0, GRP5, Mnemonics.SHR, ANNOTATED_INSTRUCTION, RMB, ONE), //
	SARD0(0xD0, GRP7, Mnemonics.SAR, ANNOTATED_INSTRUCTION, RMB, ONE), //

	ROLD1(0xD1, GRP0, Mnemonics.ROL, ANNOTATED_INSTRUCTION, RMV, ONE), //
	RORD1(0xD1, GRP1, Mnemonics.ROR, ANNOTATED_INSTRUCTION, RMV, ONE), //
	RCLD1(0xD1, GRP2, Mnemonics.RCL, ANNOTATED_INSTRUCTION, RMV, ONE), //
	RCRD1(0xD1, GRP3, Mnemonics.RCR, ANNOTATED_INSTRUCTION, RMV, ONE), //
	SHLD1(0xD1, GRP4, Mnemonics.SHL, ANNOTATED_INSTRUCTION, RMV, ONE), //
	SHRD1(0xD1, GRP5, Mnemonics.SHR, ANNOTATED_INSTRUCTION, RMV, ONE), //
	SARD1(0xD1, GRP7, Mnemonics.SAR, ANNOTATED_INSTRUCTION, RMV, ONE), //

	ROLD2(0xD2, GRP0, Mnemonics.ROL, ANNOTATED_INSTRUCTION, RMB, Register.CL), //
	RORD2(0xD2, GRP1, Mnemonics.ROR, ANNOTATED_INSTRUCTION, RMB, Register.CL), //
	RCLD2(0xD2, GRP2, Mnemonics.RCL, ANNOTATED_INSTRUCTION, RMB, Register.CL), //
	RCRD2(0xD2, GRP3, Mnemonics.RCR, ANNOTATED_INSTRUCTION, RMB, Register.CL), //
	SHLD2(0xD2, GRP4, Mnemonics.SHL, ANNOTATED_INSTRUCTION, RMB, Register.CL), //
	SHRD2(0xD2, GRP5, Mnemonics.SHR, ANNOTATED_INSTRUCTION, RMB, Register.CL), //
	SARD2(0xD2, GRP7, Mnemonics.SAR, ANNOTATED_INSTRUCTION, RMB, Register.CL), //

	ROLD3(0xD3, GRP0, Mnemonics.ROL, ANNOTATED_INSTRUCTION, RMV, Register.CL), //
	RORD3(0xD3, GRP1, Mnemonics.ROR, ANNOTATED_INSTRUCTION, RMV, Register.CL), //
	RCLD3(0xD3, GRP2, Mnemonics.RCL, ANNOTATED_INSTRUCTION, RMV, Register.CL), //
	RCRD3(0xD3, GRP3, Mnemonics.RCR, ANNOTATED_INSTRUCTION, RMV, Register.CL), //
	SHLD3(0xD3, GRP4, Mnemonics.SHL, ANNOTATED_INSTRUCTION, RMV, Register.CL), //
	SHRD3(0xD3, GRP5, Mnemonics.SHR, ANNOTATED_INSTRUCTION, RMV, Register.CL), //
	SARD3(0xD3, GRP7, Mnemonics.SAR, ANNOTATED_INSTRUCTION, RMV, Register.CL), //

	AAMD4(0xD4, 0x0A, Mnemonics.AAM), //
	AADD5(0xD5, 0x0A, Mnemonics.AAD), //

	SALCD6(0xD6, Mnemonics.SALC), //

	XLATD7(0xD7, Mnemonics.XLAT), //

	LOOPNZE0(0xE0, Mnemonics.LOOPNZ, DISB), //
	LOOPZE1(0xE1, Mnemonics.LOOPZ, DISB), //
	LOOPE2(0xE2, Mnemonics.LOOP, DISB), //
	JCXZE3(0xE3, Mnemonics.JCXZ, DISB), //
	INE4(0xE4, Mnemonics.IN, Register.AL, INTB), //
	INE5(0xE5, Mnemonics.IN, EAX, INTB), //
	OUTE6(0xE6, Mnemonics.OUT, INTB, Register.AL), //
	OUTE7(0xE7, Mnemonics.OUT, INTB, EAX), //
	CALLE8(0xE8, Mnemonics.CALL, DISV), //
	JMPE9(0xE9, Mnemonics.JMP, DISV), //
	JMPEA(0xEA, Mnemonics.JMP, ANNOTATED_INSTRUCTION, ADR), //
	JMPEB(0xEB, Mnemonics.JMP, DISB), //
	INEC(0xEC, Mnemonics.IN, Register.AL, Register.DX), //
	INED(0xED, Mnemonics.IN, EAX, Register.DX), //
	OUTEE(0xEE, Mnemonics.OUT, Register.DX, Register.AL), //
	OUTEF(0xEF, Mnemonics.OUT, Register.DX, EAX), //

	LOCKF0(0xF0, Mnemonics.LOCK, LOCK_ASSERTION), //
	INT1F1(0xF1, Mnemonics.INT, ONE), //
	REPNZF2(0xF2, Mnemonics.REPNZ, STRING_REPETITION), //
	REPZF3(0xF3, Mnemonics.REP, STRING_REPETITION), //
	HLTF4(0xF4, Mnemonics.HLT), //
	CMCF5(0xF5, Mnemonics.CMC), //

	TESTF6_GRP0(0xF6, GRP0, Mnemonics.TEST, ANNOTATED_INSTRUCTION, RMB, INTB), //
	TESTF6_GRP1(0xF6, GRP1, Mnemonics.TEST, ANNOTATED_INSTRUCTION, RMB, INTB), //
	NOTF6(0xF6, GRP2, Mnemonics.NOT, ANNOTATED_INSTRUCTION, RMB), //
	NEGF6(0xF6, GRP3, Mnemonics.NEG, ANNOTATED_INSTRUCTION, RMB), //
	MULF6(0xF6, GRP4, Mnemonics.MUL, ANNOTATED_INSTRUCTION, Register.AL, RMB), //
	IMULF6(0xF6, GRP5, Mnemonics.IMUL, ANNOTATED_INSTRUCTION, Register.AL, RMB), //
	DIVF6(0xF6, GRP6, Mnemonics.DIV, ANNOTATED_INSTRUCTION, RMB), //
	IDIVF6(0xF6, GRP7, Mnemonics.IDIV, ANNOTATED_INSTRUCTION, RMB), //

	TESTF7_GRP0(0xF7, GRP0, Mnemonics.TEST, ANNOTATED_INSTRUCTION, RMV, INTV), //
	TESTF7_GRP1(0xF7, GRP1, Mnemonics.TEST, ANNOTATED_INSTRUCTION, RMV, INTV), //
	NOTF7(0xF7, GRP2, Mnemonics.NOT, ANNOTATED_INSTRUCTION, RMV), //
	NEGF7(0xF7, GRP3, Mnemonics.NEG, ANNOTATED_INSTRUCTION, RMV), //
	MULF7(0xF7, GRP4, Mnemonics.MUL, ANNOTATED_INSTRUCTION, Register.AX, RMV), //
	IMULF7(0xF7, GRP5, Mnemonics.IMUL, ANNOTATED_INSTRUCTION, Register.AX, RMV), //
	DIVF7(0xF7, GRP6, Mnemonics.DIV, ANNOTATED_INSTRUCTION, RMV), //
	IDIVF7(0xF7, GRP7, Mnemonics.IDIV, ANNOTATED_INSTRUCTION, RMV), //

	CLCF8(0xF8, Mnemonics.CLC), //
	STCF9(0xF9, Mnemonics.STC), //
	CLIFA(0xFA, Mnemonics.CLI), //
	STIFB(0xFB, Mnemonics.STI), //
	CLDFC(0xFC, Mnemonics.CLD), //
	STDFD(0xFD, Mnemonics.STD), //

	INCFE(0xFE, GRP0, Mnemonics.INC, ANNOTATED_INSTRUCTION, RMB), //
	DECFE(0xFE, GRP1, Mnemonics.DEC, ANNOTATED_INSTRUCTION, RMB), //
	CBFE(0xFE, GRP7, Mnemonics.CALLBACK, INTW), // callback Dosbox instruction

	INCFF(0xFF, GRP0, Mnemonics.INC, ANNOTATED_INSTRUCTION, RMV), //
	DECFF(0xFF, GRP1, Mnemonics.DEC, ANNOTATED_INSTRUCTION, RMV), //
	CALLFF_2(0xFF, GRP2, Mnemonics.CALL, RMV), //
	CALLFF_3(0xFF, GRP3, Mnemonics.CALL, ANNOTATED_INSTRUCTION, MEMD), //
	JMPFF_4(0xFF, GRP4, Mnemonics.JMP, RMV), //
	JMPFF_5(0xFF, GRP5, Mnemonics.JMP, ANNOTATED_INSTRUCTION, MEMD), //
	PUSHFF(0xFF, GRP6, Mnemonics.PUSH, RMV), //
	;

	public static final int NO_SECONDARY_OPCODE = Integer.MIN_VALUE;

	private final int primaryOpcode;
	private final int secondaryOpcode;
	private final OpcodeGroup group;
	private final Mnemonics mnemonic;
	private final OpcodeType type;
	private final IndexedSeq<ArgumentType> argTypes;

	Opcode(int primaryOpcode, Mnemonics mnemonic, ArgumentType... argTypes) {
		this(primaryOpcode, OpcodeGroup.NONE, mnemonic, argTypes);
	}

	Opcode(int primaryOpcode, Mnemonics mnemonic, OpcodeType type, ArgumentType... argTypes) {
		this(primaryOpcode, OpcodeGroup.NONE, mnemonic, type, argTypes);
	}

	Opcode(int primaryOpcode, OpcodeGroup group, Mnemonics mnemonic, ArgumentType... argTypes) {
		this(primaryOpcode, group, mnemonic, NORMAL_INSTRUCTION, argTypes);
	}

	Opcode(int primaryOpcode, OpcodeGroup group, Mnemonics mnemonic, OpcodeType type, ArgumentType... argTypes) {
		this(primaryOpcode, NO_SECONDARY_OPCODE, group, mnemonic, type, argTypes);
	}

	Opcode(int primaryOpcode, int secondaryOpcode, Mnemonics mnemonic, ArgumentType... argTypes) {
		this(primaryOpcode, secondaryOpcode, OpcodeGroup.NONE, mnemonic, NORMAL_INSTRUCTION, argTypes);
	}

	Opcode(int primaryOpcode, int secondaryOpcode, OpcodeGroup group, Mnemonics mnemonic, ArgumentType... argTypes) {
		this(primaryOpcode, secondaryOpcode, OpcodeGroup.NONE, mnemonic, NORMAL_INSTRUCTION, argTypes);
	}

	Opcode(int primaryOpcode, int secondaryOpcode, OpcodeGroup group, Mnemonics mnemonic, OpcodeType type,
		ArgumentType... argTypes) {
		this.primaryOpcode = primaryOpcode;
		this.secondaryOpcode = secondaryOpcode;
		this.group = group;
		this.mnemonic = mnemonic;
		this.type = type;
		this.argTypes = Array.of(argTypes);
	}

	public int getPrimaryOpcode() {
		return primaryOpcode;
	}

	public int getSecondaryOpcode() {
		return secondaryOpcode;
	}

	public OpcodeGroup getGroup() {
		return group;
	}

	public Mnemonics getMnemonic() {
		return mnemonic;
	}

	public OpcodeType getType() {
		return type;
	}

	public IndexedSeq<ArgumentType> getArgTypes() {
		return argTypes;
	}

	public boolean hasSecondary() {
		return getSecondaryOpcode() != NO_SECONDARY_OPCODE;
	}

	public boolean hasGroup() {
		return getGroup() != OpcodeGroup.NONE;
	}

	public boolean hasArgs() {
		return !getArgTypes().isEmpty();
	}

	public boolean isPrefix() {
		return getType().isPrefix();
	}

	public boolean isJump() {
		if (getArgTypes().size() != 1) {
			return false;
		}
		final ArgumentType argType = getArgTypes().get(0);
		return DISB.equals(argType) || DISV.equals(argType);
	}

	public boolean needsAnnotation() {
		return OpcodeType.ANNOTATED_INSTRUCTION.equals(getType());
	}

	public boolean needsModRM() {
		return hasGroup() || getArgTypes().exists(ArgumentType::needsModRM);
	}

	public static IndexedSeq<Opcode> fromPrimary(int code) {
		return Array.of(values()).filter(op -> op.getPrimaryOpcode() == code);
	}
}
