package assembly;

public interface ByteSource {
	byte signedByte();

	short signedWord();

	int unsignedByte();

	int unsignedWord();

	void markStart();

	byte[] fromMark();
}
