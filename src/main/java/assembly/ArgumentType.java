package assembly;

public interface ArgumentType {
	boolean needsModRM();

	Argument createArgument(ByteSource src, ModRM modrm);
}
