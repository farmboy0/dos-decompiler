package assembly;

import static java.util.Optional.empty;

import java.util.Objects;
import java.util.Optional;

import io.vavr.collection.Array;

/**
 * A reference to a value in a memory address. The memory address is determined
 * by base reg + index reg + displacement.
 *
 * @author eho
 */
public class Dereference implements Argument {
	private final DereferenceType type;
	private final Register segment;
	private final Optional<Register> base;
	private final Optional<Register> index;
	private final int displacement;
	private final ArgumentSize displacementSize;

	public Dereference(DereferenceType type, Register segment, int displacement, ArgumentSize displacementSize) {
		this(type, segment, empty(), empty(), displacement, displacementSize);
	}

	public Dereference(DereferenceType type, Register segment, Optional<Register> base, Optional<Register> index,
		int displacement, ArgumentSize displacementSize) {
		this.type = type;
		this.segment = segment;
		this.base = base;
		this.index = index;
		this.displacement = displacement;
		this.displacementSize = displacementSize;
	}

	public DereferenceType getType() {
		return type;
	}

	public Register getSegment() {
		return segment;
	}

	public Optional<Register> getBase() {
		return base;
	}

	public Optional<Register> getIndex() {
		return index;
	}

	public int getDisplacement() {
		return displacement;
	}

	public ArgumentSize getDisplacementSize() {
		return displacementSize;
	}

	public boolean isStatic() {
		return base.isEmpty() && index.isEmpty();
	}

	@Override
	public int hashCode() {
		return Objects.hash(segment, base, displacement, index, displacementSize, type);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Dereference)) {
			return false;
		}
		Dereference other = (Dereference) obj;
		return segment == other.segment && Objects.equals(base, other.base) && displacement == other.displacement
			&& Objects.equals(index, other.index) && displacementSize == other.displacementSize && type == other.type;
	}

	@Override
	public String toString() {
		final String args = isStatic() ? staticArg() : dynamicArg();
		return String.format("[%s]", args);
	}

	private String staticArg() {
		return getDisplacementSize().format(getDisplacement());
	}

	private String dynamicArg() {
		final String dispStr = displacement == 0 ? "" : (displacement > 0 ? " + " : " - ") + staticArg();
		return Array.of(base.map(Register::getName).orElse(null), index.map(Register::getName).orElse(null))
			.filter(Objects::nonNull)
			.mkString(" + ") + dispStr;
	}

	public enum DereferenceType {
		BYTE_PTR("BYTE PTR ", ArgumentSize._8bit), //
		WORD_PTR("WORD PTR ", ArgumentSize._16bit), //
		;

		private final String annotation;
		private final ArgumentSize size;

		DereferenceType(String annotation, ArgumentSize size) {
			this.annotation = annotation;
			this.size = size;
		}

		public String getAnnotation() {
			return annotation;
		}

		public ArgumentSize getSize() {
			return size;
		}

		@Override
		public String toString() {
			return getAnnotation();
		}
	}
}
