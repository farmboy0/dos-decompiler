package assembly;

import static assembly.ArgumentTypeEnum.MEMD;

import java.util.Arrays;
import java.util.Objects;

import io.vavr.API;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;

public class Instruction implements Comparable<Instruction> {
	private final Address16bit address;
	private final byte[] bytes;
	private final Opcode[] prefixes;
	private final Register segmentOverride;
	private final Opcode opcode;
	private final Seq<Argument> arguments;
	private final int dereferenceIndex;

	private Instruction(Address16bit address, byte[] bytes, Opcode[] prefixes, Opcode opcode, Seq<Argument> arguments) {

		this.address = address;
		this.bytes = bytes;
		this.prefixes = prefixes;
		this.segmentOverride = prefixes[0] != null ? prefixes[0].getType().getSegmentOverride() : null;
		this.opcode = opcode;
		this.arguments = arguments;
		this.dereferenceIndex = arguments.find(Dereference.class::isInstance).map(arguments::indexOf).getOrElse(-1);
	}

	public static Instruction parseNext(ByteSource src, Address16bit address) {
		src.markStart();
		return parseNext(src, address, new Opcode[5]);
	}

	private static Instruction parseNext(ByteSource src, Address16bit address, Opcode[] prefixes) {
		final int primary = src.unsignedByte();
		final boolean hasSecondary = primary == 0xF;
		final int secondary = hasSecondary ? src.unsignedByte() : 0;

		Seq<Opcode> opcodes = Opcode.fromPrimary(primary);

		if (hasSecondary) {
			assert opcodes.forAll(Opcode::hasSecondary) : "Not all found opcodes have a secondary code";

			opcodes = opcodes.filter(op -> op.getSecondaryOpcode() == secondary);
		}

		final ModRM modrm = opcodes.exists(Opcode::needsModRM) ? ModRM.from(src.signedByte()) : null;
		if (opcodes.exists(Opcode::hasGroup)) {
			assert opcodes.forAll(Opcode::hasGroup) : "Not all found opcodes have a group";

			@SuppressWarnings("java:S2259")
			final OpcodeGroup group = modrm.getAsGroupId();
			opcodes = opcodes.filter(op -> op.getGroup() == group);
		}

		if (opcodes.isEmpty()) {
			final String scndStr = hasSecondary ? String.format("(%02X)", secondary) : "";
			System.err.printf("Unknown opcode: %02X%s at %s%n", primary, scndStr, address);
			return null;
		}

		assert opcodes.length() == 1 : "More than one opcode found";

		final Opcode opcode = opcodes.get();

		if (opcode.isPrefix()) {
			prefixes[opcode.getType().getPrefixIndex()] = opcode;
			return parseNext(src, address, prefixes);
		}

		final Seq<Argument> args = opcode.getArgTypes().map(type -> type.createArgument(src, modrm));
		final byte[] bytes = src.fromMark();
		return new Instruction(address, bytes, prefixes, opcode, args);
	}

	public Address16bit getAddress() {
		return address;
	}

	public Address16bit getNextAddress() {
		return address.relativeAddress(getSize());
	}

	public int getSegment() {
		return address.getSegment();
	}

	public int getOffset() {
		return address.getOffset();
	}

	public byte[] getBytes() {
		return bytes;
	}

	public String getBytesAsString() {
		return Array.ofAll(bytes).map(b -> String.format("%02X", b)).mkString(" ");
	}

	public Opcode getOpcode() {
		return opcode;
	}

	public Mnemonics getMnemonic() {
		return opcode.getMnemonic();
	}

	public Register getSegmentOverride() {
		return segmentOverride;
	}

	public Opcode getStringRepetition() {
		return prefixes[1];
	}

	private static final Seq<Opcode> USE_REPZ = API.Seq(Opcode.CMPSBA6, Opcode.CMPSWA7, Opcode.SCASBAE, Opcode.SCASWAF);

	public String getOpcodeAsString() {
		if (getStringRepetition() != null) {
			Mnemonics mnemonic = getStringRepetition().getMnemonic();
			if (USE_REPZ.contains(getOpcode()) && Mnemonics.REP.equals(mnemonic)) {
				mnemonic = Mnemonics.REPZ;
			}
			return String.format("%s %s", mnemonic.getName(), getOpcode().getMnemonic().getName());
		}
		return getOpcode().getMnemonic().getName();
	}

	public Seq<Argument> getArguments() {
		return arguments;
	}

	public Argument getArgument(int index) {
		return arguments.get(index);
	}

	public String getArgumentsAsString() {
		return arguments.map(this::formatArgument).mkString(", ");
	}

	public int getDereferenceIndex() {
		return dereferenceIndex;
	}

	public Dereference getDerefernce() {
		return dereferenceIndex != -1 ? (Dereference) getArgument(dereferenceIndex) : null;
	}

	public boolean isDerefRead() {
		if (getDereferenceIndex() == 0) {
			return MnemonicsType.ARGUMENT_READ.equals(getMnemonic().getArgumentsType())
				|| MnemonicsType.ARGUMENT_UPDATE.equals(getMnemonic().getArgumentsType())
				|| MnemonicsType.ARGUMENTS_READ_BOTH.equals(getMnemonic().getArgumentsType())
				|| MnemonicsType.ARGUMENTS_READ_UPDATE.equals(getMnemonic().getArgumentsType())
				|| MnemonicsType.ARGUMENTS_UPDATE_BOTH.equals(getMnemonic().getArgumentsType());
		} else if (getDereferenceIndex() == 1) {
			return MnemonicsType.ARGUMENTS_READ_BOTH.equals(getMnemonic().getArgumentsType())
				|| MnemonicsType.ARGUMENTS_READ_WRITE.equals(getMnemonic().getArgumentsType())
				|| MnemonicsType.ARGUMENTS_READ_UPDATE.equals(getMnemonic().getArgumentsType())
				|| MnemonicsType.ARGUMENTS_UPDATE_BOTH.equals(getMnemonic().getArgumentsType());
		}
		return false;
	}

	public boolean isDerefWrite() {
		if (getDereferenceIndex() == 0) {
			return MnemonicsType.ARGUMENT_WRITE.equals(getMnemonic().getArgumentsType())
				|| MnemonicsType.ARGUMENT_UPDATE.equals(getMnemonic().getArgumentsType())
				|| MnemonicsType.ARGUMENTS_READ_WRITE.equals(getMnemonic().getArgumentsType())
				|| MnemonicsType.ARGUMENTS_READ_UPDATE.equals(getMnemonic().getArgumentsType())
				|| MnemonicsType.ARGUMENTS_UPDATE_BOTH.equals(getMnemonic().getArgumentsType());
		} else if (getDereferenceIndex() == 1) {
			return MnemonicsType.ARGUMENTS_UPDATE_BOTH.equals(getMnemonic().getArgumentsType());
		}
		return false;
	}

	public int getSize() {
		return bytes.length;
	}

	public boolean isBlockFinisher() {
		return MnemonicsType.UNCOND_JUMP.equals(getMnemonic().getType())
			|| MnemonicsType.METHOD_RETURN.equals(getMnemonic().getType())
			|| MnemonicsType.INTERRUPT_RETURN.equals(getMnemonic().getType());
	}

	public boolean isMethodCall() {
		return MnemonicsType.METHOD_CALL.equals(getMnemonic().getType())
			|| (MnemonicsType.UNCOND_JUMP.equals(getMnemonic().getType())
				&& (getArgument(0) instanceof Address16bit || MEMD.equals(getOpcode().getArgTypes().get(0))));
	}

	public boolean isMethodFinisher() {
		return MnemonicsType.METHOD_RETURN.equals(getMnemonic().getType());
	}

	public boolean isJump() {
		return MnemonicsType.METHOD_CALL.equals(getMnemonic().getType())
			|| MnemonicsType.COND_JUMP.equals(getMnemonic().getType())
			|| MnemonicsType.UNCOND_JUMP.equals(getMnemonic().getType());
	}

	public boolean isBranch() {
		return MnemonicsType.METHOD_CALL.equals(getMnemonic().getType())
			|| MnemonicsType.METHOD_RETURN.equals(getMnemonic().getType())
			|| MnemonicsType.COND_JUMP.equals(getMnemonic().getType())
			|| MnemonicsType.UNCOND_JUMP.equals(getMnemonic().getType());
	}

	public boolean isStringOp() {
		return isStringByteOp() || isStringWordOp();
	}

	public boolean isStringByteOp() {
		return MnemonicsType.STRING_BYTE.equals(getMnemonic().getType());
	}

	public boolean isStringWordOp() {
		return MnemonicsType.STRING_WORD.equals(getMnemonic().getType());
	}

	public boolean isUsingStringSource() {
		return MnemonicsType.ARGUMENTS_READ_SRC.equals(getMnemonic().getArgumentsType())
			|| MnemonicsType.ARGUMENTS_READ_BOTH.equals(getMnemonic().getArgumentsType())
			|| MnemonicsType.ARGUMENTS_READ_WRITE.equals(getMnemonic().getArgumentsType());
	}

	public boolean isUsingStringDestination() {
		return isReadingStringDestination() || isWritingStringDestination();
	}

	public boolean isReadingStringDestination() {
		return MnemonicsType.ARGUMENTS_READ_DST.equals(getMnemonic().getArgumentsType())
			|| MnemonicsType.ARGUMENTS_READ_BOTH.equals(getMnemonic().getArgumentsType());
	}

	public boolean isWritingStringDestination() {
		return MnemonicsType.ARGUMENTS_READ_WRITE.equals(getMnemonic().getArgumentsType())
			|| MnemonicsType.ARGUMENTS_WRITE_DST.equals(getMnemonic().getArgumentsType());
	}

	public boolean isMemoryAccess() {
		return isStringOp() || getDereferenceIndex() >= 0;
	}

	@Override
	public int compareTo(Instruction o) {
		if (o == null) {
			return 1;
		}
		return getAddress().linearAddress() - o.getAddress().linearAddress();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(bytes);
		result = prime * result + Objects.hash(address);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Instruction other = (Instruction) obj;
		return Objects.equals(address, other.address) && Arrays.equals(bytes, other.bytes);
	}

	@Override
	public String toString() {
		if (arguments.isEmpty()) {
			return String.format("%s %-20s %s", address, getBytesAsString(), getOpcodeAsString());
		}
		return String.format("%s %-20s %-12s %s", address, getBytesAsString(), getOpcodeAsString(),
			getArgumentsAsString());
	}

	private String formatArgument(Argument arg) {
		if (arg instanceof Address16bit) {
			return "FAR PTR " + arg.toString();
		} else if (arg instanceof Displacement disp) {
			final Address16bit target = getNextAddress().relativeAddress(disp.getValue());
			return String.format(":%04X", target.getOffset());
		} else if (arg instanceof Dereference deref) {
			final String prefix = segmentOverride != null ? segmentOverride.getName() + ":" : "";
			return String.format("%s%s%s", getOpcode().needsAnnotation() ? deref.getType() : "", prefix,
				arg.toString());
		}
		return arg.toString();
	}
}
