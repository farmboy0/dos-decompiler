package assembly;

import io.vavr.collection.Array;

public enum Register implements Argument, ArgumentType {
	AL(0x00, "AL"), //
	CL(0x01, "CL"), //
	DL(0x02, "DL"), //
	BL(0x03, "BL"), //

	AH(0x04, "AH"), //
	CH(0x05, "CH"), //
	DH(0x06, "DH"), //
	BH(0x07, "BH"), //

	AX(0x10, "AX"), //
	CX(0x11, "CX"), //
	DX(0x12, "DX"), //
	BX(0x13, "BX"), //

	SP(0x14, "SP"), //
	BP(0x15, "BP"), //

	SI(0x16, "SI"), //
	DI(0x17, "DI"), //

	EAX(0x20, "EAX"), //
	ECX(0x21, "ECX"), //
	EDX(0x22, "EDX"), //
	EBX(0x23, "EBX"), //

	ESP(0x24, "ESP"), //
	EBP(0x25, "EBP"), //

	ESI(0x26, "ESI"), //
	EDI(0x27, "EDI"), //

	ES(0x30, "ES"), //
	CS(0x31, "CS"), //
	SS(0x32, "SS"), //
	DS(0x33, "DS"), //
	FS(0x34, "FS"), //
	GS(0x35, "GS"), //
	;

	private final int code;
	private final String name;

	Register(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	@Override
	public boolean needsModRM() {
		return false;
	}

	@Override
	public Argument createArgument(ByteSource src, ModRM modrm) {
		return this;
	}

	public static Register byCode(int code) {
		return Array.of(values()).find(r -> r.code == code).getOrNull();
	}

	public static Register byName(String name) {
		return Array.of(values()).find(r -> r.name.equals(name)).getOrNull();
	}
}
