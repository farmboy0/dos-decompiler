package assembly;

import static assembly.ArgumentSize._16bit;
import static assembly.ArgumentSize._8bit;
import static assembly.Dereference.DereferenceType.BYTE_PTR;
import static assembly.Dereference.DereferenceType.WORD_PTR;

import io.vavr.Function2;

import assembly.Dereference.DereferenceType;
import assembly.ModRM.AddressingMode;
import assembly.ModRM.RegisterMode;

public enum ArgumentTypeEnum implements ArgumentType {
	ONE(false, (src, modrm) -> new Immediate(1, _8bit)), //
	THREE(false, (src, modrm) -> new Immediate(3, _8bit)), //

	EAX(false, (src, modrm) -> Register.AX), //
	ECX(false, (src, modrm) -> Register.CX), //
	EDX(false, (src, modrm) -> Register.DX), //
	EBX(false, (src, modrm) -> Register.BX), //
	ESP(false, (src, modrm) -> Register.SP), //
	EBP(false, (src, modrm) -> Register.BP), //
	ESI(false, (src, modrm) -> Register.SI), //
	EDI(false, (src, modrm) -> Register.DI), //

	ADR(false, (src, modrm) -> Address16bit.readFrom(src)), //

	INTB(false, (src, modrm) -> new Immediate(src.unsignedByte(), _8bit)), //
	INTW(false, (src, modrm) -> new Immediate(src.unsignedWord(), _16bit)), //
	INTV(false, (src, modrm) -> new Immediate(src.unsignedWord(), _16bit)), //

	DISB(false, (src, modrm) -> new Displacement(src.signedByte(), _8bit)), //
	DISV(false, (src, modrm) -> new Displacement(src.signedWord(), _16bit)), //

	REFB(false, (src, modrm) -> new Dereference(BYTE_PTR, Register.DS, src.unsignedWord(), _16bit)), //
	REFV(false, (src, modrm) -> new Dereference(WORD_PTR, Register.DS, src.unsignedWord(), _16bit)), //

	RMB(true, (src, modrm) -> createDeRef(src, modrm, BYTE_PTR)), //
	RMW(true, (src, modrm) -> createDeRef(src, modrm, WORD_PTR)), //
	RMV(true, (src, modrm) -> createDeRef(src, modrm, WORD_PTR)), //

	REGB(true, (src, modrm) -> Register.byCode(0x00 + modrm.getAsRegisterModifier())), //
	REGV(true, (src, modrm) -> Register.byCode(0x10 + modrm.getAsRegisterModifier())), //
	REGD(true, (src, modrm) -> Register.byCode(0x20 + modrm.getAsRegisterModifier())), //
	REGS(true, (src, modrm) -> Register.byCode(0x30 + modrm.getAsRegisterModifier())), //

	MEMV(true, (src, modrm) -> createDeRef(src, modrm, WORD_PTR)), //
	MEMD(true, (src, modrm) -> createDeRef(src, modrm, WORD_PTR)), //
	;

	private final boolean needsModRM;
	private final Function2<ByteSource, ModRM, Argument> argumentCreator;

	ArgumentTypeEnum(boolean needsModRM, Function2<ByteSource, ModRM, Argument> argumentCreator) {
		this.needsModRM = needsModRM;
		this.argumentCreator = argumentCreator;
	}

	@Override
	public boolean needsModRM() {
		return needsModRM;
	}

	@Override
	public Argument createArgument(ByteSource src, ModRM modrm) {
		return argumentCreator.apply(src, modrm);
	}

	public static Argument createDeRef(ByteSource src, ModRM modrm, DereferenceType type) {
		if (AddressingMode.REGISTER.equals(modrm.getAdrMode())) {
			return modrm.getRegMode().getRegister(type.getSize());
		}
		// special case in 16-bit
		if (AddressingMode.DISP_NONE.equals(modrm.getAdrMode()) && RegisterMode.ESI.equals(modrm.getRegMode())) {
			return new Dereference(type, Register.DS, src.unsignedWord(), _16bit);
		}
		final int displacement = modrm.getAdrMode().readDisplacement(src);
		return new Dereference(type, modrm.getRegMode().getSegment(), modrm.getRegMode().getBase(),
			modrm.getRegMode().getIndex(), displacement,
			modrm.getAdrMode().getDisplacementSize().orElse(type.getSize()));
	}
}
