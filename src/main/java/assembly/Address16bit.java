package assembly;

import java.nio.ByteBuffer;
import java.util.Objects;

/**
 * An 16 bit address consisting of a segment:offset pair.
 *
 * @author eho
 */
public class Address16bit implements Argument, Comparable<Address16bit> {
	private final int segment;
	private final int offset;
	private final int linear;

	private String nameSuffix;

	public Address16bit(int segment, int offset) {
		this.segment = segment;
		this.offset = offset;
		this.linear = (segment << 4) + offset;
	}

	public Address16bit(String address) {
		String[] token = address.split(":");
		this.segment = Integer.parseInt(token[0], 16);
		this.offset = Integer.parseInt(token[1], 16);
		this.linear = (segment << 4) + offset;
	}

	public static Address16bit readFrom(ByteBuffer data) {
		int offset = data.getShort() & 0xFFFF;
		int segment = data.getShort() & 0xFFFF;
		return new Address16bit(segment, offset);
	}

	public static Address16bit readFrom(ByteSource data) {
		int offset = data.unsignedWord();
		int segment = data.unsignedWord();
		return new Address16bit(segment, offset);
	}

	public int linearAddress() {
		return linear;
	}

	public Address16bit plus(Address16bit address) {
		return new Address16bit(segment + address.segment, offset + address.offset);
	}

	public Address16bit minus(Address16bit address) {
		return new Address16bit(segment - address.segment, offset - address.offset);
	}

	public Address16bit relativeAddress(int displacement) {
		return new Address16bit(segment, offset + displacement & 0xFFFF);
	}

	public boolean isBetween(Address16bit start, Address16bit end) {
		return start.linearAddress() <= linearAddress() && linearAddress() < end.linearAddress();
	}

	public int getSegment() {
		return segment;
	}

	public int getOffset() {
		return offset;
	}

	@Override
	public int compareTo(Address16bit o) {
		if (o == null) {
			return 1;
		}
		return linearAddress() - o.linearAddress();
	}

	@Override
	public int hashCode() {
		return Objects.hash(offset, segment);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Address16bit)) {
			return false;
		}
		Address16bit other = (Address16bit) obj;
		return linearAddress() == other.linearAddress();
	}

	public String asNameSuffix() {
		if (nameSuffix == null)
			nameSuffix = String.format("%04X_%04X", segment, offset);
		return nameSuffix;
	}

	@Override
	public String toString() {
		return String.format("%04X:%04X", segment, offset);
	}
}
