package jdosbox;

import java.io.File;
import java.io.IOException;

import io.vavr.API;
import io.vavr.collection.Map;

import assembly.Address16bit;
import assembly.ByteSource;
import assembly.Dereference;
import assembly.Instruction;
import assembly.Opcode;
import assembly.Register;
import code.Project;
import data.DOSProgramFile;
import data.OverlayFile;
import jdos.cpu.CPU;
import jdos.cpu.Core;
import jdos.debug.DebugListener;
import jdos.hardware.Memory;

public final class DebugListenerImpl implements DebugListener {
	private final Project project;
	private final DosboxByteSource src = new DosboxByteSource();

	private boolean disassemble = false;

	private int interrupt = -1;
	private boolean interruptRecorded = false;

	private Instruction[] inst = new Instruction[10];
	private int depth = 0;

	public DebugListenerImpl(Project project) {
		this.project = project;
	}

	@Override
	public void execution_start(int eax, int ebx, int ecx, int edx, int esi, int edi, int ebp, int eip, int esp, int cs,
		int ds, int es, int fs, int gs, int ss) {

		if (!disassemble || src.inLastParse() || CPU.cpu.code.big) {
			return;
		}
		final Address16bit addr = new Address16bit(cs, eip);
		if (depth > 0 && !interruptRecorded) {
			project.getImage(addr).addMethod(addr, String.format("int_%02X_handler", interrupt));
			interruptRecorded = true;
		}
		if (inst[depth] != null && inst[depth].isBranch()) {
			project.getImage(inst[depth].getAddress())
				.getInfo(inst[depth].getAddress())
				.addJumpTarget(project.getImage(addr), addr);
		}
		inst[depth] = project.getInstruction(addr);
		if (inst[depth] == null) {
			src.reset();
			inst[depth] = Instruction.parseNext(src, addr);
			if (inst[depth] == null)
				return;
			project.addInstruction(inst[depth]);
		}

		final Dereference deref = inst[depth].getDerefernce();
		if (deref != null && !Opcode.LEA8D.equals(inst[depth].getOpcode())) {
			int targetSegment = segmentFrom(deref.getSegment(), inst[depth].getSegmentOverride(), cs, ds, es, fs, gs,
				ss);
			int base = deref.getBase().map(baseReg -> targetBaseFrom(baseReg, ebx, ebp)).orElse(0);
			int index = deref.getIndex().map(indexReg -> targetIndexFrom(indexReg, esi, edi)).orElse(0);
			int displacement = deref.getDisplacement();
			int targetOffset = base + index + displacement;
			Address16bit target = new Address16bit(targetSegment, targetOffset & 0xFFFF);
			project.addMemoryAccess(addr, target, base, index);
		} else if (inst[depth].isStringOp()) {
			int count = inst[depth].getStringRepetition() != null ? ecx * CPU.cpu.direction : 1;
			int srcSegment = segmentFrom(Register.DS, inst[depth].getSegmentOverride(), cs, ds, es, fs, gs, ss);
			Address16bit strSource = inst[depth].isUsingStringSource() ? new Address16bit(srcSegment, esi) : null;
			Address16bit strTarget = inst[depth].isUsingStringDestination() ? new Address16bit(es, edi) : null;
			project.addStringParams(addr, strSource, strTarget, count);
		}
	}

	private static int segmentFrom(Register base, Register override, int cs, int ds, int es, int fs, int gs, int ss) {
		final Register targetSegment = override != null ? override : base;
		return switch (targetSegment) {
			case CS -> cs;
			case DS -> ds;
			case ES -> es;
			case FS -> fs;
			case GS -> gs;
			case SS -> ss;
			default -> throw new IllegalArgumentException("Not a segment register: " + targetSegment);
		};
	}

	private static int targetBaseFrom(Register base, int bx, int bp) {
		return switch (base) {
			case BX -> bx;
			case BP -> bp;
			default -> throw new IllegalArgumentException("Not a base register: " + base);
		};
	}

	private static int targetIndexFrom(Register index, int si, int di) {
		return switch (index) {
			case SI -> si;
			case DI -> di;
			default -> throw new IllegalArgumentException("Not an index register: " + index);
		};
	}

	@Override
	public void interrupt_start(int num, int type) {
		interrupt = num;
		interruptRecorded = false;
		inst[++depth] = null;
	}

	@Override
	public void interrupt_exit() {
		depth--;
		if (depth < 0)
			depth = 0;
	}

	@Override
	public void com_loaded(String file, int loadsegment, boolean execute, int initCS, int initEIP) {
		if (file == null) {
			return;
		}
		Address16bit addr = new Address16bit(loadsegment, 0);
		try {
//			System.out.printf("Loading com binary %s to %s%n", file, addr);
			project.addBinary(DOSProgramFile.create(new File(file)), addr);
			if (execute)
				disassemble = true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void exe_loaded(String file, int loadsegment, boolean execute) {
		if (file == null) {
			return;
		}
		Address16bit addr = new Address16bit(loadsegment, 0);
		try {
//			System.out.printf("Loading exe binary %s to %s%n", file, addr);
			project.addBinary(DOSProgramFile.create(new File(file)), addr);
			if (execute)
				disassemble = true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void overlay_loaded(String file, int loadsegment) {
		if (file == null) {
			return;
		}
		Address16bit addr = new Address16bit(loadsegment, 0);
		try {
//			System.out.printf("Loading overlay %s to %s%n", file, addr);
			project.addOverlay(new OverlayFile(new File(file)), addr);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Map<String, Long> offsets = API.Map();

	@Override
	public void file_seek(String file, long fileOffset, SeekType type) {
		offsets = switch (type) {
			case START -> offsets.put(file, fileOffset);
			case CURRENT -> offsets.put(file, offsets.getOrElse(file, 0L) + fileOffset);
			case END -> offsets.put(file, new File(file).length() + fileOffset);
		};
	}

	@Override
	public void file_read(String file, int length, int targetSegment, int targetOffset) {
		long fileOffset = offsets.getOrElse(file, 0L);
		file_read(file, fileOffset, length, targetSegment, targetOffset);
	}

	@Override
	public void file_read(String file, long fileOffset, int length, int targetSegment, int targetOffset) {
		Address16bit addr = new Address16bit(targetSegment, targetOffset);
		offsets = offsets.put(file, fileOffset + length);
		try {
//			System.out.printf("Loading file data %s(%d,%d) to %s%n", file, fileOffset, length, addr);
			project.addData(file, addr, (int) fileOffset, length);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	final class DosboxByteSource implements ByteSource {
		private int current;
		private int mark;

		public DosboxByteSource() {
			reset();
		}

		public void reset() {
			current = Core.cseip;
			mark = -1;
		}

		public boolean inLastParse() {
			if (mark == -1)
				return false;
			return mark < Core.cseip && Core.cseip < current;
		}

		@Override
		public int unsignedWord() {
			int temp = Memory.mem_readw(current);
			current += 2;
			return temp;
		}

		@Override
		public int unsignedByte() {
			return Memory.mem_readb(current++);
		}

		@Override
		public short signedWord() {
			short temp = (short) Memory.mem_readw(current);
			current += 2;
			return temp;
		}

		@Override
		public byte signedByte() {
			return (byte) Memory.mem_readb(current++);
		}

		@Override
		public void markStart() {
			mark = current;
		}

		@Override
		public byte[] fromMark() {
			if (mark == -1) {
				return new byte[0];
			}
			byte[] result = new byte[current - mark];
			current = mark;
			for (int i = 0; i < result.length; i++) {
				result[i] = (byte) unsignedByte();
			}
			return result;
		}
	}
}
