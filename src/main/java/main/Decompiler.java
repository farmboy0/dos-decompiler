package main;

import java.awt.EventQueue;

import javax.swing.UIManager;

import ui.DesktopFrame;

public class Decompiler {
	public static final void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}

		EventQueue.invokeLater(() -> {
			DesktopFrame ui = new DesktopFrame();
			ui.show();
		});
	}
}
