package assembly;

import static assembly.ArgumentSize._16bit;
import static assembly.ArgumentSize._8bit;
import static assembly.Dereference.DereferenceType.BYTE_PTR;
import static assembly.Dereference.DereferenceType.WORD_PTR;
import static assembly.Instruction.parseNext;
import static java.nio.ByteBuffer.wrap;
import static java.nio.ByteOrder.LITTLE_ENDIAN;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.stream.Stream;

import io.vavr.collection.Array;
import io.vavr.collection.IndexedSeq;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import code.ByteBufferSource;

class InstructionTest {

	private static Stream<Arguments> arguments() {
		return Stream.of( //
			Arguments.of(b(0x80, 0xBF, 0x84, 0x69, 0x3C), Opcode.CMP80,
				args(new Dereference(BYTE_PTR, Register.DS, of(Register.BX), empty(), 0x6984, _16bit),
					new Immediate(0x3C, _8bit))), //
			Arguments.of(b(0x83, 0xC4, 0x04), Opcode.ADD83, args(Register.SP, new Immediate(4, _8bit))), //
			Arguments.of(b(0x2E, 0x8B, 0x36, 0xCD, 0x01), Opcode.MOV8B,
				args(Register.SI, new Dereference(WORD_PTR, Register.DS, 0x1CD, _16bit))), //
			Arguments.of(b(0x8C, 0xC8), Opcode.MOV8C, args(Register.AX, Register.CS)), //
			Arguments.of(b(0x8D, 0x06, 0x79, 0x05), Opcode.LEA8D,
				args(Register.AX, new Dereference(WORD_PTR, Register.DS, 0x579, _16bit))), //
			Arguments.of(b(0x9A, 0x6E, 0x33, 0xDB, 0x13), Opcode.CALL9A, args(new Address16bit(0x13DB, 0x336E))), //
			Arguments.of(b(0xB1, 0x03), Opcode.MOVB1, args(Register.CL, new Immediate(3, _8bit))), //
			Arguments.of(b(0xD4, 0x0A), Opcode.AAMD4, args()), //
			Arguments.of(b(0xE9, 0x15, 0x0E), Opcode.JMPE9, args(new Displacement(0xE15, ArgumentSize._16bit))), //
			Arguments.of(b(0xEA, 0x12, 0x34, 0x56, 0x78), Opcode.JMPEA, args(new Address16bit(0x7856, 0x3412))), //
			Arguments.of(b(0xFF, 0x46, 0xFC), Opcode.INCFF,
				args(new Dereference(WORD_PTR, Register.SS, of(Register.BP), empty(), -4, _8bit))) //
		);
	}

	@ParameterizedTest
	@MethodSource("arguments")
	void testInstructionParsing(byte[] data, Opcode resultOpcode, IndexedSeq<Argument> resultArguments) {
		Instruction inst = parseNext(new ByteBufferSource(wrap(data).order(LITTLE_ENDIAN)), new Address16bit(0, 0));
		assertNotNull(inst);
		assertNotNull(inst.toString());
		assertEquals(resultOpcode, inst.getOpcode());
		resultArguments.forEach(arg -> assertEquals(arg, inst.getArguments().get(resultArguments.indexOf(arg))));
	}

	private static byte[] b(int... ints) {
		byte[] result = new byte[ints.length];
		for (int i = 0; i < ints.length; i++) {
			result[i] = (byte) ints[i];
		}
		return result;
	}

	private static IndexedSeq<Argument> args(Argument... args) {
		return Array.of(args);
	}
}
