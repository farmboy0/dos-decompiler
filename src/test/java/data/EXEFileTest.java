package data;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;

class EXEFileTest {

	@Test
	void test() throws IOException {
		File f = new File("/mnt/daten/Dosbox/dosroot/DARKLAND/DARKLAND.EXE");
		assumeTrue(f.exists());

		EXEFile e = new EXEFile(f);
		assertNotNull(e);
	}

}
