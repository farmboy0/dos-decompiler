About
=====

Disassembler/Decompiler of DOS binaries.
Supports running the binary in DOSBox and
retrieving executed instructions from it.
Currently only 16-bit real-mode binaries are supported.

Building
========

Requirements:

* Java Development Kit 17+


To build you currently need to build the following project first:
https://github.com/farmboy0/JDosBoxRemastered/tree/rework
Checkout the rework branch and run
`./mvnw clean install`


Run
`./mvnw clean package -Dmaven.test.skip=true`
in the project directory to build dos-decompiler.

Running
=======

From the project directory run the following command:
`java -jar ./target/dos-decompiler-0.0.1-SNAPSHOT.jar [<dos exe or com file>]`

if you want run a binary with dosbox, start the jar without any further argument.
Create a new project and set up the dosbox config to run your binary in the autoexec section.
